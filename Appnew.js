// import React from 'react';
// import { Provider } from 'react-redux';
// import { store } from 'src/redux/reduxStore';
// import { StyleSheet, TouchableOpacity, Text } from 'react-native'
// import { Title} from 'native-base';
// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
// import NavigationService from 'src/components/NavigationService';
// import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
// import {Transition} from 'react-native-reanimated';
// import FontAwesome from 'src/components/FontAwesome5';
// import Splash from './src/screens/SplashInitialScreen';
// import CompleteRegister from './src/screens/CompleteRegistration';
// import CreatePetProfile from './src/screens/CreatePetProfile';
// import CustomerContainer from './src/components/CustomerContainer';

// const Stack = createStackNavigator();

// function App() {
//     return (
//         <NavigationContainer>
//             <Stack.Navigator
//                 initialRouteName="Splash"
//             >
//                 <Stack.Screen 
//                     name="" 
//                     component={Splash}
//                     options={{ header: null, gestureEnabled: false }}/>
//                 <Stack.Screen 
//                     name="" 
//                     component={CompleteRegister}
//                     options={{ header: null, gestureEnabled: false }}/>
//                 <Stack.Screen 
//                     name="Pet Profile" 
//                     component={CreatePetProfile}
//                     options={({ navigation }) => ({  headerTintColor: '#fff', 
//                                                 title: 'Pet Profile',
//                                                 headerTitle: () => <Title style={styles.headerTitle}>Pet Profile</Title>,
//                                                 headerStyle: { backgroundColor: '#6c63ff' },
//                                                 headerRight: <TouchableOpacity style={{marginRight: 15}} hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
//                                                                         onPress={() =>{
//                                                                             const saveProfile = navigation.getParam('createSaveProfile');
//                                                                             saveProfile();
//                                                                         }}>
//                                                                 <FontAwesome name={'check'} size={24} style={{color: '#fff'}}/>
//                                                             </TouchableOpacity>,
//                                                 headerLeft: <TouchableOpacity style={{marginLeft: 15}} hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
//                                                                     onPress={() =>{navigation.goBack()}}>
//                                                                 <FontAwesome name={'times'} size={24} style={{color: '#fff'}}/>
//                                                             </TouchableOpacity>
//                                             })}/>
//             </Stack.Navigator>
//         </NavigationContainer>
//     )
// }

// const styles = StyleSheet.create({
//     headerTitle:{
//       color: 'white', 
//       fontFamily: 'sans', 
//       fontSize: 18, 
//       paddingLeft: (Platform.OS === 'android')?15:0
//     }
//   });