import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store } from 'src/redux/reduxStore';
import { StyleSheet, TouchableOpacity, Text, Button } from 'react-native'
import { Title} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import NavigationService from 'src/components/NavigationService';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import {Transition} from 'react-native-reanimated';
import FontAwesome from 'src/components/FontAwesome5';
import Splash from './src/screens/SplashInitialScreen';
import CompleteRegister from './src/screens/CompleteRegistration';
import CreatePetProfile from './src/screens/CreatePetProfile';
import CustomerContainer from './src/components/CustomerContainer';
import WalkerContainer from './src/components/WalkerContainer'
import WalkerApply from 'src/screens/WalkerApply';
import WalkerApplication from 'src/screens/WalkerApplication';

const Main = createStackNavigator(
  {
    Splash: {
      screen: Splash,
      navigationOptions: {header: null}
    },
    CompleteRegister: {
      screen: CompleteRegister,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
        title: null
      }
    },
    CreatePetProfile: {
      screen: CreatePetProfile,
      navigationOptions: ({ navigation }) => ({
        headerTintColor: '#fff',        
        title: 'Pet Profile',
        headerTitle: () => <Title style={styles.headerTitle}>Pet Profile</Title>,
        headerStyle:{                    
          backgroundColor: '#6c63ff', 
        },
        headerRight: <TouchableOpacity style={{marginRight: 15}} hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                                onPress={() =>{
                                    const saveProfile = navigation.getParam('createSaveProfile');
                                    saveProfile();
                                }}>
                            <Text style={{ fontFamily: 'sans', fontSize: 18, color: 'white'}}>Save</Text>
                            {/* <FontAwesome name={'check'} size={24} style={{color: '#fff'}}/> */}
                        </TouchableOpacity>,
        // headerLeft:<TouchableOpacity style={{marginLeft: 15}} hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
        //                     onPress={() =>{navigation.goBack()}}>
        //                 <FontAwesome name={'times'} size={24} style={{color: '#fff'}}/>
        //             </TouchableOpacity>
      })
    },    
    WalkerApply: {
      screen: WalkerApply,
      navigationOptions: ({ navigation }) => ({
        headerTintColor: '#fff',        
        title: null,
        headerTitle: () => <Title style={styles.headerTitle}>Dog Walker</Title>,
        headerStyle:{                    
          backgroundColor: '#6c63ff', 
        },
        // headerLeft:<TouchableOpacity style={{marginLeft: 15}} hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
        //                     onPress={() =>{navigation.goBack()}}>
        //                 <FontAwesome name={'times'} size={24} style={{color: '#fff'}}/>
        //             </TouchableOpacity>
      })
    },    
    WalkerApplication: {
      screen: WalkerApplication,
      navigationOptions: ({ navigation }) => {          
          const applied = store.getState().userWalkerApplicationReducer.applicationData.walkerapplied || false; 
          const appliedParam = navigation.getParam('walkerapplied');
          if(applied || appliedParam){
            return {
              headerTintColor: '#fff',        
              title: 'Application',
              headerTitle: () => <Title style={styles.headerTitle}>Application</Title>,
              headerStyle:{                    
                backgroundColor: '#6c63ff', 
              },
              headerLeft: null,
              gesturesEnabled: false,
            }
          }else{
            return {
              headerTintColor: '#fff',        
              title: 'Application',
              headerTitle: () => <Title style={styles.headerTitle}>Application</Title>,
              headerStyle:{                    
                backgroundColor: '#6c63ff', 
              }
            }
          }
      }
    }
  },
  {
      initialRouteName: 'Splash',
  }
);

const appNavigator = createAnimatedSwitchNavigator(
  {
    Main:  {
      screen: Main
    },
    CustomerContainer: {
      screen: CustomerContainer 
    },
    WalkerContainer: {
      screen: WalkerContainer
    }
  },
  {
    initialRouteName: 'Main',
    transition: (
      <Transition.Together>
        <Transition.Out
          type="fade"
          durationMs={200}
          interpolation="easeIn"
        />
        <Transition.In type="fade" durationMs={500} />
      </Transition.Together>
    )
  }
);

const AppNavigator = createAppContainer(appNavigator);

const styles = StyleSheet.create({
  headerTitle:{
    color: 'white', 
    fontFamily: 'sans', 
    fontSize: 18, 
    paddingLeft: (Platform.OS === 'android')?15:0
  }
});

function App(){
  return(
    <Provider store={store}>
        <AppNavigator ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                      }}/>
    </Provider>
  );
}

export default App;

// export default () => <Provider store={store}>
//             <AppNavigator ref={navigatorRef => {
//                             NavigationService.setTopLevelNavigator(navigatorRef);
//                           }}/>
//         </Provider>;
