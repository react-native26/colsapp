## App Scaffolding

This app is created using `expo init`. For more info on Expo with React Native visit [this page](https://docs.expo.io/). 

To start the app use

```sh
$ npm start
```

This app is already preconfigured with [Metro Bundler](https://facebook.github.io/metro/docs/en/getting-started). Starting the app will launch Metro Bundler in your local machine browser which can be used to test the app in physcial iOS or Android devices. 

## Default linking

The app can be default deep linked via `collrsapp://`. This is defined in `app.json` in the `scheme` key. DO NOT modify the scheme since this will be used in the Callback URLS for Authentication with AWS Cognito and Google federation. More info about Linking and app URL scheme found [here](https://docs.expo.io/versions/latest/workflow/linking/#in-a-standalone-app).

URLs in various environments
* Published app in Expo client: `exp://exp.host/@community/with-webbrowser-redirect`
* Published app in standalone: `collrsapp://`
* Development: `exp://wg-qka.community.app.exp.direct:80`

## Expo Client

To test apps using the Metro Bundler please download the Expo Client app from the [Google Playstore](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en_US) or [Apple App Store](https://apps.apple.com/us/app/expo-client/id982107779). Once the app has started using the `npm start` command, scan the QR Code from the Metro bundler screen on your phone which will provide a link to launch the app using the Expo Client App.