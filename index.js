import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { AppearanceProvider } from 'react-native-appearance';

AppRegistry.registerComponent(appName, () =>                                              
                                    <AppearanceProvider>                                         
                                        <App />                                                                           
                                    </AppearanceProvider>                                  
                            );
