const customerProfileForm ={
    cognitoUserName:{
        fieldID: 'cognitoUserName',
        errorID: 'cognitoUserNameError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },
    image: {
        fieldID: 'image',
        errorID: 'imageError',            
        required: false,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },
    memberSince: {
        fieldID: 'memberSince',
        errorID: 'memberSinceError',            
        required: false,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },
    petName: {
        fieldID: 'petName',
        errorID: 'petNameError',            
        required: true,
        minlength: 2,
        maxlength: 20,
        customValidations: [{
            validate : (value) => {                
                if(!(/^[A-Za-z]+$/i.test(value))){
                    return 'Enter a valid name';
                }
            }
        }]
    },        
    petWeight: {
        fieldID: 'petWeight',
        errorID: 'petWeightError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },        
    petAge: {
        fieldID: 'petAge',
        errorID: 'petAgeError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },        
    petBreed: {
        fieldID: 'petBreed',
        errorID: 'petBreedError',            
        required: true,
        minlength: 1,
        maxlength: 100,
        customValidations: []
    },                      
    petGender: {
        fieldID: 'petGender',
        errorID: 'petGenderError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },        
    houseTrained: {
        fieldID: 'houseTrained',
        errorID: 'houseTrainedError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },        
    friendlyDog: {
        fieldID: 'friendlyDog',
        errorID: 'friendlyDogError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },        
    aboutPet: {
        fieldID: 'aboutPet',
        errorID: null,            
        required: false,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },
    careInfo: {
        fieldID: 'careInfo',
        errorID: null,            
        required: false,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },
    vetInfo: {
        fieldID: 'vetInfo',
        errorID: null,            
        required: false,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },
    customerFirstName: {
        fieldID: 'customerFirstName',
        errorID: 'customerFirstNameError',            
        required: true,
        minlength: 2,
        maxlength: 100,
        customValidations: [{
            validate : (value) => {                
                if(!(/[A-Za-z]+$/i.test(value))){
                    return 'Enter a valid first name';
                }
            }
        }]
    }, 
    customerLastName: {
        fieldID: 'customerLastName',
        errorID: 'customerLastNameError',            
        required: true,
        minlength: 2,
        maxlength: 100,
        customValidations: [{
            validate : (value) => {                
                if(!(/[A-Za-z]+$/i.test(value))){
                    return 'Enter a valid last name';
                }
            }
        }]
    },       
    customerEmail: {
        fieldID: 'customerEmail',
        errorID: 'customerEmailError',            
        required: true,
        minlength: 2,
        maxlength: 100,
        customValidations: []
    },                
    customerPhone: {
        fieldID: 'customerPhone',
        errorID: 'customerPhoneError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: [{
            validate : (value) => {                
                if(value.length !== 10){
                    return 'Enter a 10-digit phone number';
                }
            }
        },{
            validate: (value) => {
                if(!(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(value))){
                    return 'Enter a valid 10-digit phone number';
                }
            }
        }]
    },        
    customerAddress: {
        fieldID: 'customerAddress',
        errorID: 'customerAddressError',            
        required: true,
        minlength: 5,
        maxlength: 100,
        customValidations: []
    },        
    customerCity: {
        fieldID: 'customerCity',
        errorID: 'customerCityError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },        
    customerPinCode: {
        fieldID: 'customerPinCode',
        errorID: 'customerPinCodeError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: [{
            validate : (value) => {                
                if(value.length !== 6){
                    return 'Enter a valid 6-digit pin code';
                }
            }
        },{
            validate: (value) => {
                if(!(/^(\d{6})$/.test(value))){
                    return 'Enter a valid 6-digit pin code';
                }
            }
        }]
    }        
};

export default customerProfileForm;