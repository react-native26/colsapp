const walkerProfileForm ={
    cognitoUserName:{
        fieldID: 'cognitoUserName',
        errorID: 'cognitoUserNameError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },
    image: {
        fieldID: 'image',
        errorID: 'imageError',            
        required: false,
        minlength: 0,
        maxlength: 0,
        customValidations: []
    },    
    walkerEmail: {
        fieldID: 'walkerEmail',
        errorID: 'walkerEmailError',            
        required: true,
        minlength: 2,
        maxlength: 100,
        customValidations: []
    },                
    walkerPhone: {
        fieldID: 'walkerPhone',
        errorID: 'walkerPhoneError',            
        required: true,
        minlength: 0,
        maxlength: 0,
        customValidations: [{
            validate : (value) => {                
                if(value.length !== 10){
                    return 'Enter a 10-digit phone number';
                }
            }
        },{
            validate: (value) => {
                if(!(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(value))){
                    return 'Enter a valid 10-digit phone number';
                }
            }
        }]
    },        
    walkerAddress: {
        fieldID: 'walkerAddress',
        errorID: 'walkerAddressError',            
        required: true,
        minlength: 5,
        maxlength: 100,
        customValidations: []
    },              
};

export default walkerProfileForm;