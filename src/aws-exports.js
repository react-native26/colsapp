
import { Platform } from 'react-native';
import { Linking, AuthSession } from 'expo';
import * as WebBrowser from 'expo-web-browser';
import { Hub } from '@aws-amplify/core';
// import { Cache } from 'aws-amplify';

// const federatedInfo = Cache.getItem('federatedInfo');
// const { token } = federatedInfo;

// console.log(token);

console.log("Link: ", Linking.makeUrl());

// Maybe for Future implementation with Google, Facebook and Twitter federation
const urlOpener = async (url, redirectUrl) => {    
    console.log("URL = ",url, " Redirect URL = " ,redirectUrl);
    const { type, url: newUrl } = await WebBrowser.openAuthSessionAsync(url, redirectUrl);    

    if (type === 'success') {
        console.log(Platform.OS);
        
        if (Platform.OS === 'ios') {
            await WebBrowser.dismissBrowser();
            return Linking.openURL(newUrl);
        }
    }
    if(type === 'cancel' || type === 'dismiss'){        
        Hub.dispatch('authChannel', 
            { 
                event: 'loginCancelled',
                data: 'Login Cancelled'
            });
    }
};

const amplifyconfig = {
    Auth:
    {
        region: "ap-south-1",
        identityPoolId: "ap-south-1:2ebc7a6d-d0fe-48f1-874d-d370dccfea0c",
        userPoolId: "ap-south-1_YjjmA8qcC",
        userPoolWebClientId: "61palncbp2v3eumhagappfsar5",    
        oauth: {
            domain : "collrsnonprod.auth.ap-south-1.amazoncognito.com", 
            scope : ["email", "profile", "openid","aws.cognito.signin.user.admin"],         
            redirectSignIn : Linking.makeUrl(), 
            redirectSignOut : Linking.makeUrl(),
            responseType: "code",
            options: {
                // Indicates if the data collection is enabled to support Cognito advanced security features. By default, this flag is set to true.
                AdvancedSecurityDataCollectionFlag : true
            },
            urlOpener: urlOpener
        }
    },
    Storage:{
        AWSS3:
        {
            bucket: 'collrs-user-files',
            region: 'ap-south-1'
        }        
    }
}

export default amplifyconfig;