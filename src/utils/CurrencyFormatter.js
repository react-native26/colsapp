import 'intl';
import 'intl/locale-data/jsonp/en-IN';

export function currencyFormatter(value) {
    // For now the app is India only thus, we will hard code the locale
   return new Intl.NumberFormat('en-IN', {
                                    style: 'currency',
                                    currency: 'INR',
                                    minimumFractionDigits: 0,
                                    maximumFractionDigits: 20,}).format(value);
}