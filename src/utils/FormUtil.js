
class FormUtil{
    /**
     * Takes a JSON Form Template
     * @param {*} form 
     */
    constructor(form){
        this._form = form || {};
    }

    /**
     * Returns the Form State as a JSON Objet when the Component state is passed
     * Argument state is optional.
     * Call this function to get the keys required for form fields in components state
     * USAGE: 
     * 1) To initalize the Component state
     * const form = new Form(template);
     * const formFields = form.GetFormState();
     * this.state = {...formFields.formFields, ...formFields.errorFields}
     * 
     * 2) To get hydrated state back
     * const formData = form.GetFormState(this.state);
     * @param {*} state 
     */
    GetFormState = (state={}) => {
        // console.log("*****RECEIVED STATE*****", state);
        const formFields = {}, errorFields = {};
        for (const property in this._form) {
            formFields[this._form[property].fieldID] = state[this._form[property].fieldID] || '';
            errorFields[this._form[property].errorID] = ''; 
        }
        return {formFields, errorFields};
    }


    /**
     * Takes data and runs the validations as defined in the form template.
     * Generally the entire component state (this.state) can be passed to this method.
     * EFC returns error boolean flag, and error data (if any)
     * @param {*} data 
     * @param {*} callback 
     */
    ValidateForm = (data, callback) => {
        let errorCount = 0, errorFields = {}, formFields={};
    
        for (const property in this._form) {
            const {fieldID, errorID, required, minlength, maxlength,customValidations} = this._form[property];
            const value = data[fieldID]; 
            
            if(required && !value){
                errorFields[errorID] = 'This field is required'; 
                errorCount += 1;
            }else{
                if(minlength > 0 && maxlength > 0){
                    if(value.length < minlength){
                        errorFields[errorID] = `Value must be atleast ${minlength} characters long`; 
                        errorCount += 1;
                    }else if (value.length > maxlength){
                        errorFields[errorID] = `Max ${maxlength} characters allowed`; 
                        errorCount += 1;
                    }                
                }
    
                if( customValidations.length > 0 ){
                    for(const validation of customValidations){
                        
                        const err = validation.validate(value);
                        
                        errorFields[errorID] = err || '';
                        if(err){
                            errorCount += 1;
                        }
                    }
                }
            }                 
        }    

        if(errorCount === 0){
            formFields = this.GetFormState(data).formFields;
        }
        callback((errorCount>0), errorFields, formFields);
    }
}

export default FormUtil;