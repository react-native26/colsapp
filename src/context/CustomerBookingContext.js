import React from 'react';

const CustomerBookingContext = React.createContext();

export const CustomerBookingProvider = ({ children, value }) => {
    return <CustomerBookingContext.Provider value={value}>
        {children}
    </CustomerBookingContext.Provider>
}

export default CustomerBookingContext;