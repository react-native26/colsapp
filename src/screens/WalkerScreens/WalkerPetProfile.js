import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, Keyboard, TextInput, ImageBackground, TouchableOpacity, Switch } from 'react-native';
import { Container, Content, Text, List, ListItem, Separator, Thumbnail } from 'native-base';
import { Col, Grid, Row } from "react-native-easy-grid";
import { LinearGradient } from 'expo-linear-gradient';
import { format } from "date-fns";
import FontAwesome from '../../components/FontAwesome5';
import Placeholder from '../../components/PlaceHolder';
import AlertModal from '../../components/AlertModal';
const learnMoreText = 'If you deactivate your profile you will no longer be able to view or select new tasks. You can activate your profile back at any time. However, you wont be able to deactivate your profile if you have any active tasks.';
const switchAlertText = 'If you deactivate your profile you will no longer be able to view or select tasks. Do you wish to proceed?';


const userData = {
    pet_name: "Rollie",
    breed: "Rottweiler",
    age: 3.5,
    weight: 9.2,
    gender: 'M',
    house_trained: 'Y',
    friendly_dog: 'N',
    about_pet: 'Rollie is a good dog but he can get crranky some times. He has brown fur and black eyes and is usually playful.',
    care_info: 'Please do not feed Rollie outside water or food. He is allergic to gluten as well.',
    vet_info: '',
    profile_image: '',
    member_since: '13-DEC-2019',
    contact_info: {
        name: 'John Doe',
        email: 'john@email.com',
        phone: '+919208438890',
        address: '442 Blg K, Sector 24, Near Ashok Vihar',
        city: 'Noida',
        pin: '100101',
        walker_id:''
    }

};


const Label = ({ title }) => {
    return (<Row size={1} style={{ left: 4 }}>
        <Text style={styles.labelText}>
            {title}
        </Text>
    </Row>);
};

const Value = ({ title }) => {
    return (<Row size={2}>
        <Text style={styles.valueText}>
            {title}
        </Text>
    </Row>);
};

const ItemWithVal = (props) => {
    const { label, value, icon } = props;
    return (<ListItem {...props} style={styles.formLine}>
        <Grid>
            <Col size={2} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                {
                    (icon) && <FontAwesome name={icon} size={14} />
                }
                <Label title={label} />
            </Col>
            <Col size={1} style={{ paddingRight: 5 }}>
                <Value title={value} />
            </Col>
        </Grid>
    </ListItem>);
}

const NoInfo = () => {
    return (
        <View style={{ marginTop: 20, marginBottom: 20 }}>
            <FontAwesome name={'question-square'} size={26} style={[styles.profileIcons, { color: '#E0E0E0' }]} />
            <Text style={[styles.valueText, { color: '#E0E0E0' }]}>
                No information provided
            </Text>
        </View>
    );
}

class WalkerPetProfile extends Component {
    constructor(props) {
        super(props);
        this.inputRefs = {
            favSport5: null

        };

        // console.log("Date is *******",format(Date.parse(userData.member_since), 'LLL d, yyyy'))

        this.state = {
            image: null,
            userData: null,
            loading: true,
            isSwitchEnabled: false
        };
    }

    componentDidMount() {
        this.setState({ image: userData.profile_image, userData, loading: false });
    }

    render() {
        // const { contact_info } = this.state.userData;
        return (
            <Container showsVerticalScrollIndicator={false}>
                <StatusBar barStyle="light-content" />
                <Content showsVerticalScrollIndicator={false}>
                    <List style={styles.bodyContainer} >
                        <ListItem noIndent style={{ marginLeft: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0, paddingTop: 0 }}>
                            <ImageBackground style={styles.headercontainer} source={(this.state.image) ? { uri: this.state.image } : undefined}>
                                {/* <BlurView tint="light" intensity={90} style={styles.blurred}/>   */}
                                <LinearGradient locations={[1.0, 0]} colors={['rgba(0,0,0,0.50)', 'rgba(0,0,0,0.50)']} start={[1.0, 0.0]} start={[1.0, 0.0]}
                                    style={styles.linearGradient} />
                                <View style={{ alignContent: 'center', justifyContent: 'center', marginTop: 40, marginBottom: 40 }}>
                                    <View style={styles.avatarStyle}>
                                        <Thumbnail large source={(this.state.image) ? { uri: this.state.image } : require('../../assets/images/paw.png')} style={styles.avatarImg} />
                                    </View>
                                </View>
                            </ImageBackground>
                        </ListItem>
                        {
                            (this.state.loading === true) ?
                                <Placeholder />
                                : <>
                                    <ListItem noIndent noBorder>
                                        <Grid>
                                            <Col>
                                                <Text style={styles.petName}>{this.state.userData.pet_name}</Text>
                                                <Text style={styles.petSpecifics}>{this.state.userData.breed}</Text>
                                                <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>
                                                    <Text style={styles.petSpecifics}>{(this.state.userData.gender === 'M') ? 'Male, ' : 'Female, '}</Text>
                                                    <Text style={styles.petSpecifics}>{this.state.userData.age} yrs</Text>
                                                </View>
                                                {
                                                    (this.state.userData.member_since !== '' || this.state.userData.member_since !== null) &&
                                                    <Text style={styles.petSpecifics}>Member since {format(Date.parse(userData.member_since), 'LLL d, yyyy')}</Text>
                                                }
                                            </Col>
                                        </Grid>

                                    </ListItem>
                                    <Separator style={{ height: 20 }} />
                                    <Separator style={styles.separatorStyle}>
                                        <View style={{ flexDirection: 'row' }}>
                                            {/* <FontAwesome name={'paw'} size={26} style={styles.profileIcons} /> */}
                                            <Text style={styles.blockHeader}>Profile Info</Text>
                                        </View>
                                        {/* <Text style={styles.blockSubHeader}>Basic information about your pet</Text> */}
                                    </Separator>
                                    <View style={styles.formBlock}>
                                        <ItemWithVal noIndent label={"Eamil"} value={`${this.state.userData.contact_info.email}`} />
                                        <ItemWithVal noIndent label="Phone" value={this.state.userData.contact_info.phone} />
                                        <ItemWithVal noIndent label="Address" value={this.state.userData.contact_info.address} />
                                        <ListItem noIndent style={{...styles.bookingListItem, height: 70}} noBorder>
                                            <Grid>
                                                <Col size={2}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        {/* <FontAwesome name={'walking'} size={14} style={styles.InfoIcon}/> */}
                                                        <Text style={styles.labelText}>Deactivate</Text>
                                                    </View> 
                                                    <View style={{ flexDirection: 'row', paddingTop: 5}}>
                                                        <FontAwesome name={'question-circle'} size={11} style={{...styles.InfoIcon,color: '#6c63ff'}}/>
                                                        <TouchableOpacity onPress={() => this.setState({ learnmoreAlert: true })}>
                                                            <Text style={{...styles.infoText, color: '#6c63ff'}}>Learn more</Text>
                                                            <AlertModal 
                                                                visible={this.state.learnmoreAlert}             // show hide modal
                                                                // scrollable={true}                            // if the modal content needs to be scrollable (defaults to false)
                                                                header={'Deactivating Profile'}                            // header text for the modal popup
                                                                // headerStyle = {{...}}                        // header text style override object
                                                                content={learnMoreText}                              // modal popup body content text
                                                                // contentstyle = {{...}}                       // body content text style object override
                                                                headerIcon={'lightbulb-exclamation'}                          // (optional) Icon to be set on the modal
                                                                headerIconStyle={{color: '#6c63ff'}}            // (optional) override Icon Style         
                                                                okText={'Got it!'}                           // (optional) OK button text 
                                                                // cancelText={'Cancel'}                        // (optional) Cancel Button text
                                                                // onOk={() => console.log('Ok Pressed')}       // (optional) call back function when OK button is tapped
                                                                // onCancel={() => console.log('Cancel Pressed')}   //(optional) call back function when Cancel button is tapped
                                                                // okButtonStyle = {{...}}                      // (optional) OK Button custom text style object
                                                                // cancelButtonStyle = {{...}}                  // (optional) Cancel button custom style object
                                                                onDismissed={() => this.setState({ learnmoreAlert: false })}  // (optional) Callback function when modal is dismissed
                                                                // animationIn={''}                             // (optional) animation in for the modal (default slideInUp)
                                                                // animationOut={''}                            // (optional) animation out for the modal (default slideOutDown)
                                                                />
                                                        </TouchableOpacity>
                                                    </View>
                                                </Col>
                                                <Col size={2}>  
                                                <Switch
                                                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                                                    thumbColor={this.state.isSwitchEnabled ? "#81b0ff" : "#81b0ff"}
                                                    ios_backgroundColor="#3e3e3e"
                                                    onValueChange={() => this.setState({ isSwitchEnabled: !this.state.isSwitchEnabled})}
                                                    value={this.state.isSwitchEnabled}
                                                    />
                                                </Col>
                                            </Grid>
                                        </ListItem>
                                    </View>

                                </>
                        }

                    </List>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    bodyContainer: {
        width: '100%',
        backgroundColor: 'white'
    },
    headercontainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#41484e'
    },
    avatarImg: {
        width: 100,
        height: 100,
        borderRadius: 240 / 2,
        resizeMode: "contain",
        alignSelf: 'center'
    },
    avatarStyle: {
        width: 110,
        height: 110,
        borderRadius: 260 / 2,
        backgroundColor: 'white',
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 5,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    linearGradient: {
        position: 'absolute',
        flex: 1,
        width: '100%',
        height: '100%'
    },
    profileIcons: {
        color: '#37474F',
        paddingRight: 5,
        alignSelf: 'center'
    },
    formLine: {
        height: 60
    },
    largeFormLine: {
        paddingBottom: 40
    },
    InfoIcon:{        
        color: '#616161',
        paddingRight: 5,
        alignSelf: 'center'
    },
    infoText:{
        fontFamily: 'sans',        
        color: '#37474F',
        opacity: 0.75,
        fontSize: 13,                
        alignSelf: 'center'
    },
    labelText: {
        fontFamily: 'sans',
        color: '#616161',
        fontSize: 14,
        textAlign: 'left',
        paddingTop: 5,
        paddingBottom: 5
    },
    valueText: {
        fontFamily: 'sans',
        fontSize: 14,
        textAlign: 'right',
        flex: 1
    },
    formBlock: {
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 5,
    },
    blockHeader: {
        fontFamily: 'sansmedium',
        fontSize: 24,
        color: '#37474F'
    },
    blockSubHeader: {
        fontFamily: 'sans',
        fontSize: 12
    },
    separatorStyle: {
        height: 70,
        backgroundColor: '#fff'
    },
    petName: {
        fontFamily: 'sansmedium',
        fontSize: 22,
        color: '#37474F',
    },
    petSpecifics: {
        fontFamily: 'sans',
        fontSize: 16,
        color: '#616161'
    }
});

export default WalkerPetProfile;