import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Animated, Easing, ActivityIndicator, Dimensions } from 'react-native';
import { Container, Content, Thumbnail, List, ListItem, Separator, Button, Right, Left, DatePicker } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid"
import { format } from "date-fns";
import DateTimePickerModal from "react-native-modal-datetime-picker";

import FontAwesome from '../../../components/FontAwesome5';
import AlertModal from '../../../components/AlertModal';



import Constants from 'expo-constants';
import { Header } from 'react-navigation';

export default class MyTaskDetails extends Component {
    constructor(props) {
        super(props);
        let date = new Date();
        date.setHours(0, 0, 0, 0);
        this.state = {
            isMeetGreetDateScheduled: false,
            isMeetGreetAlert: false,
            showDatePicker: false,
            showTimePicker: false,
            meetGreetDate: '',
            isMeeeGreetConfirmed: false,
            initialDate: date,
            meetGreetAlertText: 'This is recomended that you contact the customer prior to scheduling the meet & greet in order to confirm their availability',
            meetGreetScheduledAlertText: 'Your meet and greet with the customer hasbeen scheduled and is awaiting customer confirmation.'
        };
    }

    taskDetails = () => {
        return (
            <ListItem noIndent noBorder touchableHighlightStyle={{ borderRadius: 20 }}>
                <View style={styles.upcomingItem}>
                    <Grid>
                        <Col style={{ marginTop: 10, marginRight: 10, flex: 1, height: 200, justifyContent: 'space-evenly' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Thumbnail large source={require('../../../assets/images/walker.png')} />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={styles.infoLabel}>Vijay C</Text>
                                {/* <Text style={styles.infoLabelValue}>: Vijay chavre</Text> */}
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={styles.infoLabel}>Pet Name</Text>
                                <Text style={styles.infoLabelValue}>: Romieo</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={styles.infoLabel}>Breed</Text>
                                <Text ellipsizeMode='middle' style={styles.infoLabelValue}>: Collie</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={styles.infoLabel}>Customer Since </Text>
                                <Text style={styles.infoLabelValue}>{format(Date.parse('13-DEC-2019'), 'LLL d, yyyy')}</Text>
                            </View>
                        </Col>
                    </Grid>
                </View>
            </ListItem>
        )
    }


    //***** DatePicker helpers */
    setDate = (date) => {

        const meetGreetDate = date || this.state.meetGreetDate;
        if (this.state.showTimePicker) {
            const { timeSlots } = this.state;
            timeSlots[this.state.timeSlotPosition] = date;
            this.setState({
                // showTimePicker: Platform.OS === 'ios' ? true : false,
                showTimePicker: false,
                timeSlots
            });
        }

        if (this.state.showDatePicker) {
            meetGreetDate.setHours(0, 0, 0, 0);
            this.setState({
                // showDatePicker: Platform.OS === 'ios' ? true : false,
                showDatePicker: false,
                meetGreetDate: format(meetGreetDate, 'LLL d, yyyy'),
                initialDate: meetGreetDate,
                isMeetGreetDateScheduled: true
            });
        }
    }

    show = mode => {
        this.setState({
            showDatePicker: (mode === 'date') ? true : false,
            showTimePicker: (mode === 'time') ? true : false,
            mode,
        });
    }

    datePicker = (mode) => {
        this.show(mode);
    }

    showMeetGreetSection = () => {
        if (this.state.isMeetGreetDateScheduled) { // show when scheduled meet and greet date
            return (
                <>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={styles.infoLabel}>Meet & Greet Date </Text>
                        <Text style={styles.infoLabelValue}>: {this.state.meetGreetDate}</Text>
                    </View>
                    {
                        this.state.isMeeeGreetConfirmed ? (
                        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center', marginTop: 10}}><Text> Pending customer confirmations</Text></View> 
                        ) : <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                        <View style={styles.btn2}>
                            <TouchableOpacity
                                onPress={() => this.datePicker('date')}
                                style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
                            >
                                {/* <FontAwesome name={'phone-alt'} size={14} style={{color: '#4CAF50', marginRight: 5, alignSelf: 'center'}}/> */}
                                <Text style={{ color: '#4CAF50', fontFamily: 'sansmedium', fontSize: 14 }}>Change Date</Text>

                            </TouchableOpacity>
                        </View>
                        <View style={styles.btn2}>
                            <TouchableOpacity
                                onPress={() => this.setState({isMeetScheduleGreetAlert: true})}
                                style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
                            >
                                <AlertModal
                                    visible={this.state.isMeetScheduleGreetAlert}             // show hide modal
                                    // scrollable={true}                            // if the modal content needs to be scrollable (defaults to false)
                                    header={'Meet & Greet scheduled'}                            // header text for the modal popup
                                    // headerStyle = {{...}}                        // header text style override object
                                    content={this.state.meetGreetScheduledAlertText}                              // modal popup body content text
                                    // contentstyle = {{...}}                       // body content text style object override
                                    headerIcon={'handshake'}                          // (optional) Icon to be set on the modal
                                    headerIconStyle={{ color: '#6c63ff' }}            // (optional) override Icon Style         
                                    okText={'OK'}                           // (optional) OK button text 
                                    // cancelText={'Cancel'}                        // (optional) Cancel Button text
                                    // onOk={() => console.log('Ok Pressed')}       // (optional) call back function when OK button is tapped
                                    // onCancel={() => console.log('Cancel Pressed')}   //(optional) call back function when Cancel button is tapped
                                    // okButtonStyle = {{...}}                      // (optional) OK Button custom text style object
                                    // cancelButtonStyle = {{...}}                  // (optional) Cancel button custom style object
                                    onDismissed={() => 
                                        this.setState({
                                            isMeetScheduleGreetAlert: false,
                                            isMeeeGreetConfirmed: true
                                        })
                                    } // (optional) Callback function when modal is dismissed
                                // animationIn={''}                             // (optional) animation in for the modal (default slideInUp)
                                // animationOut={''}                            // (optional) animation out for the modal (default slideOutDown)
                                />
                                <Text style={{ fontSize: 14, fontFamily: 'sansmedium', color: '#90A4AE', }}>Confirm</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                    
                }
                </>
            )
        }

        return (
            <>
                <View style={styles.btn}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isMeetGreetAlert: true })}
                        style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
                    >
                        <AlertModal
                            visible={this.state.isMeetGreetAlert}             // show hide modal
                            // scrollable={true}                            // if the modal content needs to be scrollable (defaults to false)
                            header={'Meet & Greet'}                            // header text for the modal popup
                            // headerStyle = {{...}}                        // header text style override object
                            content={this.state.meetGreetAlertText}                              // modal popup body content text
                            // contentstyle = {{...}}                       // body content text style object override
                            headerIcon={'handshake'}                          // (optional) Icon to be set on the modal
                            headerIconStyle={{ color: '#6c63ff' }}            // (optional) override Icon Style         
                            okText={'OK'}                           // (optional) OK button text 
                            // cancelText={'Cancel'}                        // (optional) Cancel Button text
                            // onOk={() => console.log('Ok Pressed')}       // (optional) call back function when OK button is tapped
                            // onCancel={() => console.log('Cancel Pressed')}   //(optional) call back function when Cancel button is tapped
                            // okButtonStyle = {{...}}                      // (optional) OK Button custom text style object
                            // cancelButtonStyle = {{...}}                  // (optional) Cancel button custom style object
                            onDismissed={() => 
                                this.setState({
                                    isMeetGreetAlert: false,
                                    showDatePicker: true,
                                    // showTimePicker: true,
                                    mode: 'date'
                                })
                            } // (optional) Callback function when modal is dismissed
                        // animationIn={''}                             // (optional) animation in for the modal (default slideInUp)
                        // animationOut={''}                            // (optional) animation out for the modal (default slideOutDown)
                        />
                        <FontAwesome name={'handshake'} size={18} style={{ color: '#4CAF50', marginRight: 5, alignSelf: 'center' }} />
                        <Text style={{ color: '#4CAF50', fontFamily: 'sansmedium', fontSize: 14 }}>Meet Greet</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                    <Text style={{ ...styles.infoLabelValue, width: '80%', textAlign: 'center' }}>We will charge an amount of Rs. 50 on your card if you accept this task. This amount will be refunded to you. <Text style={{ ...styles.infoText, color: '#6c63ff' }}>Learn more</Text></Text>
                </View>
            </>
        )
    }

    taskInfo = () => {
        return (
            <ListItem noIndent noBorder touchableHighlightStyle={{ borderRadius: 20 }}>
                <View style={styles.upcomingItem}>
                    <Grid>
                        <Col style={{ marginTop: 10, marginRight: 10, flex: 1, height: 200, justifyContent: 'space-evenly' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={styles.infoLabel}>Start Date </Text>
                                <Text style={styles.infoLabelValue}>: 15th Sept 2020</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{
                                    fontSize: 16,
                                    // fontFamily: 'sansmedium',
                                    color: '#37474F',
                                }}>Mon, Tue, 6AM 5PM WALKS</Text>
                                {/* <Text style={styles.infoLabelValue}>: Vijay chavre</Text> */}
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={styles.infoLabel}>Address </Text>
                                <Text style={styles.infoLabelValue}>: 1234, Building 5, 4th floor, Borivali</Text>
                            </View>
                            <View style={styles.btn}>
                                <TouchableOpacity
                                    onPress={() => Linking.openURL(`tel:${this.state.bookingData.walker_contact}`)}
                                    style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
                                >
                                    <FontAwesome name={'phone-alt'} size={14} style={{ color: '#4CAF50', marginRight: 5, alignSelf: 'center' }} />
                                    <Text style={{ color: '#4CAF50', fontFamily: 'sansmedium', fontSize: 14 }}>Call Customer</Text>
                                </TouchableOpacity>

                            </View>
                            {
                                this.showMeetGreetSection()
                            }
                            <DateTimePickerModal
                                minimumDate={(this.state.showDatePicker) ? new Date() : undefined}
                                isVisible={this.state.showDatePicker || this.state.showTimePicker}
                                mode={this.state.mode}
                                onConfirm={this.setDate}
                                onCancel={() => this.setState({ showDatePicker: false, showTimePicker: false })}
                                isDarkModeEnabled={(this.state.colorscheme === 'light') ? false : true}
                                confirmTextIOS={'Done'}
                            />
                        </Col>
                    </Grid>
                </View>

            </ListItem>
        )
    }
    render() {

        return (
            <Container showsVerticalScrollIndicator={false}>
                <Content showsVerticalScrollIndicator={false}>
                    <List>

                        <React.Fragment>
                            <Separator style={styles.separatorStyle}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 16 }}>Task Details</Text>
                                </View>
                            </Separator>
                            <View
                                style={styles.upcomingBlock}
                            >
                                {
                                    this.taskDetails()
                                }
                            </View>
                            <Separator style={styles.separatorStyle}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 16 }}>Task Info</Text>
                                </View>
                            </Separator>
                            <View
                                style={styles.upcomingBlock}
                            >
                                {
                                    this.taskInfo()
                                }
                            </View>
                            {
                                this.state.isMeetGreetDateScheduled &&
                                <View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                        <Text style={{ ...styles.infoLabelValue, width: '80%', textAlign: 'center' }}>We will charge an amount of Rs. 50 on your card if you accept this task. This amount will be refunded to you. <Text style={{ ...styles.infoText, color: '#6c63ff' }}>Learn more</Text></Text>
                                    </View>
                                </View>
                            }
                        </React.Fragment>
                    </List>
                </Content>
            </Container >
        );
    }
}


const styles = StyleSheet.create({
    infoLabel: {
        fontSize: 14,
        fontFamily: 'sansmedium',
        color: '#616161',
    },
    infoLabelValue: {
        fontSize: 12,
        color: '#37474F',
        flexWrap: 'wrap',
        marginLeft: 10,
    },
    upcomingBlock: {
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 3,
    },
    upcomingItem: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flex: 1,
    },
    upcomingHeader: {
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 16,
        alignSelf: 'flex-start'
    },
    separatorStyle: {
        height: 60,
        backgroundColor: '#fff'
    },
    btn: {
        flexDirection: 'row',
        // backgroundColor: 'white',
        height: 35,
        // marginHorizontal: 40,
        marginLeft: '20%',
        marginRight: '20%',
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#90A4AE',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },
    btn2: {
        flexDirection: 'row',
        width: '48%',
        height: 35,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#90A4AE',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    }
});