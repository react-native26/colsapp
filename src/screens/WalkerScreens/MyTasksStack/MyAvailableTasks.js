import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Animated, Easing, ActivityIndicator, Dimensions } from 'react-native';
import { Content, Thumbnail, List, ListItem, Separator, Button, Right, Left } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid"

import FontAwesome from '../../../components/FontAwesome5';



import Constants from 'expo-constants';
import { Header } from 'react-navigation';

export default class MyAvailableTasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    taskItem = () => {
        return (
            <ListItem  noIndent noBorder touchableHighlightStyle={{ borderRadius: 20 }}>
                <View style={styles.upcomingItem}>
                    <Grid >
                        <Col style={{height: 100, width:'25%' }}>
                        <Thumbnail style={{marginTop: 10, marginLeft: 5}} source={require('../../../assets/images/walker.png')} />                                

                        </Col>

                        <Col style={{ marginTop: 10, marginRight: 10,width:'75%'  }}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.infoLabel}>Customer Name</Text>
                                <Text style={styles.infoLabelValue}>: Vijay chavre</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.infoLabel}>Pet Name</Text>
                                <Text style={styles.infoLabelValue}>: Romieo</Text>
                            </View>
                            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                <Text style={styles.infoLabel}>Breed</Text>
                                <Text  ellipsizeMode = 'middle' style={styles.infoLabelValue}>: Collie</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.infoLabel}>Task Info </Text>
                                <Text style={styles.infoLabelValue}>: Mon, tue, 6 AM</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.infoLabel}>Location </Text>
                                <Text style={styles.infoLabelValue}>: Borivali</Text>
                            </View>

                       </Col>
                    </Grid>
                    <Grid>
                        <Col>
                        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around', alignItems: 'center'}}>
                           
                           <View style={styles.btn}>
                           <TouchableOpacity
                            onPress={() => Linking.openURL(`tel:${this.state.bookingData.walker_contact}`)}
                            style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
                        >
                            <FontAwesome name={'phone-alt'} size={14} style={{color: '#4CAF50', marginRight: 5, alignSelf: 'center'}}/>
                            <Text style={{color: '#4CAF50', fontFamily: 'sansmedium', fontSize: 14}}>Call Customer</Text>  
                           </TouchableOpacity>

                           </View>
                           <View style={styles.btn}>
                               <TouchableOpacity
                                   onPress={() => this.props.navigation.navigate('MyTaskDetails')}
                                   style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
                               >
                                   
                                   <Text style={{ fontSize: 14, fontFamily: 'sansmedium', color: '#90A4AE', }}>View</Text>
                                   <FontAwesome name={'chevron-right'} size={18} style={{color: '#90A4AE', marginLeft: 10, alignSelf: 'center'}}/>
                               </TouchableOpacity>

                               </View>
                               </View>
                       
                        </Col>
                    </Grid>
                </View>
            </ListItem>
        )
    }
    render() {

        return (
            <Content showsVerticalScrollIndicator={false}>
                <List style={styles.bodyContainer}>

                    <React.Fragment>
                        <Separator style={styles.separatorStyle}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16 }}>Available nearby tasks</Text>
                            </View>
                        </Separator>
                        <View 
                        style={styles.upcomingBlock}
                        >
                            {
                                this.taskItem()
                            }
                        </View>
                        <View style={styles.upcomingBlock}>
                            {
                                this.taskItem()
                            }
                        </View>
                        <View style={styles.upcomingBlock}>
                            {
                                this.taskItem()
                            }
                        </View>
                    </React.Fragment>
                </List>
            </Content>
        );
    }
}


const styles = StyleSheet.create({
    infoLabel: {
        fontSize: 14,
        fontFamily: 'sansmedium',
        color: '#616161',
        width:'50%'
    },
    infoLabelValue: {
        fontSize: 12,
        color:'#37474F',
        flex: 1, 
        flexWrap: 'wrap',
        marginLeft: 10,
    },
    upcomingBlock: {
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
    },
    upcomingItem: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flex: 1,
    },
    upcomingHeader: {
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 16,
        alignSelf: 'flex-start'
    },
    separatorStyle: {
        height: 70,
        backgroundColor: '#fff'
    },
    btn: {
        flexDirection: 'row',
        width:'48%',
        height: 35,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#90A4AE',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    }
});

