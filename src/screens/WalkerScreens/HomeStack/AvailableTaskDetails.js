import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Animated, Easing, ActivityIndicator, Dimensions } from 'react-native';
import { Container, Content, Thumbnail, List, ListItem, Separator, Button, Right, Left } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid"
import { format } from "date-fns";

import FontAwesome from '../../../components/FontAwesome5';



import Constants from 'expo-constants';
import { Header } from 'react-navigation';

export default class AvailableTasksDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    taskDetails = () => {
        return (
            <ListItem  noIndent noBorder touchableHighlightStyle={{ borderRadius: 20 }}>
                <View style={styles.upcomingItem}>
                    <Grid>
                        <Col style={{ marginTop: 10, marginRight: 10, flex: 1, height: 200, justifyContent: 'space-evenly' }}>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                            <Thumbnail large  source={require('../../../assets/images/walker.png')} />    
                            </View>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                                <Text style={styles.infoLabel}>Vijay C</Text>
                                {/* <Text style={styles.infoLabelValue}>: Vijay chavre</Text> */}
                            </View>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                                <Text style={styles.infoLabel}>Pet Name</Text>
                                <Text style={styles.infoLabelValue}>: Romieo</Text>
                            </View>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                                <Text style={styles.infoLabel}>Breed</Text>
                                <Text  ellipsizeMode = 'middle' style={styles.infoLabelValue}>: Collie</Text>
                            </View>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                                <Text style={styles.infoLabel}>Customer Since </Text>
                                <Text style={styles.infoLabelValue}>{format(Date.parse('13-DEC-2019'), 'LLL d, yyyy')}</Text>
                            </View>
                        </Col>
                    </Grid>
                </View>
            </ListItem>
        )
    }

    taskInfo = () => {
        return (
            <ListItem  noIndent noBorder touchableHighlightStyle={{ borderRadius: 20 }}>
                <View style={styles.upcomingItem}>
                    <Grid>
                        <Col style={{ marginTop: 10, marginRight: 10, flex: 1, height: 200, justifyContent: 'space-evenly'  }}>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                                <Text style={{
                                    fontSize: 16,
                                    // fontFamily: 'sansmedium',
                                    color:'#37474F',
                                }}>Mon, Tue, 6AM 5PM WALKS</Text>
                                {/* <Text style={styles.infoLabelValue}>: Vijay chavre</Text> */}
                            </View>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                                <Text style={styles.infoLabel}>Location </Text>
                                <Text style={styles.infoLabelValue}>: Borivali</Text>
                            </View>
                            <View style={styles.btn}>
                            <TouchableOpacity
                                //onPress={() => this.props.navigation.navigate('TaskDetails')}
                                //style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
                            >
                                    
                                <Text style={{ fontSize: 14, fontFamily: 'sans', color: '#90A4AE', }}>Accept task</Text>
                            </TouchableOpacity>

                            </View>
                            <View style={{flexDirection:'row', justifyContent:'center',}}>
                                <Text style={{...styles.infoLabelValue, width: '80%', textAlign: 'center'}}>We will charge an amount of Rs. 50 on your card if you accept this task. This amount will be refunded to you. <Text style={{...styles.infoText, color: '#6c63ff'}}>Learn more</Text></Text>
                            </View>
                        </Col>
                    </Grid>
                </View>
            </ListItem>
        )
    }
    render() {

        return (
            <Container showsVerticalScrollIndicator={false}>
            <Content showsVerticalScrollIndicator={false}>
                <List>

                    <React.Fragment>
                        <Separator style={styles.separatorStyle}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16 }}>Task Details</Text>
                            </View>
                        </Separator>
                        <View 
                        style={styles.upcomingBlock}
                        >
                            {
                                this.taskDetails()
                            }
                        </View>
                        <Separator style={styles.separatorStyle}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16 }}>Task Info</Text>
                            </View>
                        </Separator>
                        <View 
                        style={styles.upcomingBlock}
                        >
                            {
                                this.taskInfo()
                            }
                        </View>
                    </React.Fragment>
                </List>
            </Content>
            </Container >
        );
    }
}


const styles = StyleSheet.create({
    infoLabel: {
        fontSize: 14,
        fontFamily: 'sansmedium',
        color: '#616161',
    },
    infoLabelValue: {
        fontSize: 12,
        color:'#37474F',
        flexWrap: 'wrap',
        marginLeft: 10,
    },
    upcomingBlock: {
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 3,
    },
    upcomingItem: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flex: 1,
    },
    upcomingHeader: {
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 16,
        alignSelf: 'flex-start'
    },
    separatorStyle: {
        height: 60,
        backgroundColor: '#fff'
    },
    btn: {
        flexDirection: 'row',
        // backgroundColor: 'white',
        height: 35,
        // marginHorizontal: 40,
        marginLeft: '20%',
        marginRight: '20%',
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#90A4AE',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    }
});