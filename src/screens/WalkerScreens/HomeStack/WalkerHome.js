import React, { Component } from 'react';
import Amplify, { Auth, Hub } from 'aws-amplify';
import { connect } from 'react-redux';
import { getUserData, setWalkerApplicationData } from '../../../redux/reduxReducer';
import { bindActionCreators } from 'redux';


import { View, Text, StyleSheet, TouchableOpacity, Animated, Easing, ActivityIndicator } from 'react-native';
import { Icon, Button, Spinner } from 'native-base';
import FontAwesome from '../../../components/FontAwesome5';



import Constants from 'expo-constants';
import { Header } from 'react-navigation';
import AvailableTasks from './AvailableTasks';

class WalkerHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: 'BLOCKED',
    };
  }

  blockedStatus = () => {
    return(
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', justifyContent: 'center' }} >
        <FontAwesome name={'exlamation-triangle'} size={80} style={{ color: 'black', marginRight: 10 }} />
      </View>
      <View >
        <Text style={styles.blockedSubtext}>Sorry, we are unable to display availabe list of tasks right now</Text>
      </View>
    </View>
    )
  }


  checkStatus = () => {
    Auth.currentAuthenticatedUser()
    .then(async(user) => {

      const status = user.signInUserSession.idToken.payload['custom:walker_status'] || this.props.applicationData.status;
      const isWalker = user.signInUserSession.idToken.payload['custom:is_walker'];  
      const userName = user.signInUserSession.idToken.payload['cognito:username'];  
      this.props.setWalkerApplicationData({ walkerapplied: true, status: 'APPROVED' });   

    })
    .catch((err) => {
      console.log("Not signed in",err)
    });
  }
  render() {
    if (this.state.status === 'BLOCKED' && false) {
      return(
        this.blockedStatus()
      )
    }
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }} >
          <Text style={styles.heaederText}>Hi, Vijay !</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }} >
          <Text style={styles.subText}>
            We are still processing your application. You will be notified once we have verified the
            information you provided.
          </Text>
        </View>
        <View style={styles.btn}>
          <TouchableOpacity
           // onPress={() => this.props.navigation.navigate('AvailableNearbyTasks')}
              onPress={this.checkStatus}
            style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center' }}
          >
            <FontAwesome name={'sync'} size={20} style={{ color: '#90A4AE', marginRight: 10 }} />
            <Text style={{ fontSize: 16, fontFamily: 'sans', color: '#90A4AE', }}>Check Status</Text>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    //marginTop: Constants.statusBarHeight + Header.HEIGHT
    flex: 1,
    justifyContent: 'center'
  },
  heaederText: {
    fontSize: 21,
    fontFamily: 'sansmedium',
    color: '#37474f'
  },
  blockedSubtext: {
    fontSize: 16,
    // marginLeft: 50,
    // marginRight: 50,
    textAlign: 'center',
    top: 20,
    fontFamily: 'sans',
    color: '#37474f'
  },
  subText: {
    fontSize: 16,
    marginLeft: 60,
    marginRight: 60,
    textAlign: 'center',
    top: 20,
    fontFamily: 'sans',
    color: '#37474f'
  },
  btn: {
    flexDirection: 'row',
    // backgroundColor: 'white',
    height: 40,
    marginHorizontal: 70,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#90A4AE',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 50,
  }
})


function mapStateToProps(state){
  // console.log(state.userWalkerApplicationReducer.applicationData)
  return{
      applicationData: state.userWalkerApplicationReducer.applicationData
    };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getUserData: (username) => getUserData(username),
    setWalkerApplicationData: (applicationData) => setWalkerApplicationData(applicationData)
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WalkerHome);