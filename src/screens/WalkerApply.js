import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Dimensions, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { Container, Content, List, ListItem, Text} from 'native-base';
import AwesomeButton from "react-native-really-awesome-button";
import ButtonGradient from 'src/components/ButtonGradient';
import FontAwesome from 'src/components/FontAwesome5';
import { Col, Row, Grid } from "react-native-easy-grid";
import { LinearGradient } from 'expo-linear-gradient';
import * as WebBrowser from 'expo-web-browser';

const walkerHeaderImg = 'https://collrs-assets.s3.ap-south-1.amazonaws.com/images/products/walker101.png';

//https://www.collrs.com/become-a-dog-walker#comp-jlhmfrph

class WalkerApply extends Component {

    openWeb = async (url) =>{
        await WebBrowser.openBrowserAsync(url, {enableBarCollapsing: true });
    }

    render() {
        return (
            <>
            <Container style={styles.attribution}>
                <StatusBar barStyle="light-content"  />  
                <Content showsVerticalScrollIndicator={false} style={{ backgroundColor: '#fff'}} contentContainerStyle={{paddingBottom: 60}} alwaysBounceVertical={false}>                                        
                    <ImageBackground resizeMode="cover" resizeMethod="auto" source={{uri: walkerHeaderImg}} style={{ flex: 1, padding: 0, justifyContent: 'flex-end', overflow: 'hidden', height: 170}}>
                        <LinearGradient locations={[0, 1.0]}  colors= {['rgba(0,0,0,0.00)', 'rgba(0,0,0,0.80)']} 
                            style={styles.linearGradient}/>
                        <View style={{padding: 10}}>
                            <Text style={styles.headerText}>Become a <Text style={{fontFamily: 'pacifico', fontSize: 20, color: '#fff'}}>Collrs</Text> Dog-Walker</Text>
                        </View>
                    </ImageBackground>
                    <List style={styles.bodyContainer} >
                        <ListItem noBorder noIndent style={styles.listView}>                            
                            <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>                                                                        
                                <Text style={styles.headerSubText}>We're glad that you've decided to become a dog-walker with Collrs. We'd love to get to know more about you.</Text>
                            </View>
                        </ListItem>
                        <ListItem noBorder noIndent style={styles.listView}>      
                             <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>
                                <AwesomeButton 
                                            raiseLevel={0} 
                                            height={50} 
                                            stretch={true}
                                            textFontFamily="sansmedium"       
                                            textSize={16}          
                                            borderRadius={50}                                                        
                                            ExtraContent={
                                                <ButtonGradient/>
                                                }
                                            onPress={() => this.props.navigation.navigate('WalkerApplication')}                                            
                                    >
                                    <FontAwesome name={'rocket-launch'} size={18} style={{color: '#fff', marginLeft: 5, marginRight: 5}}/>
                                    <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 16}}>Get Started</Text>
                                </AwesomeButton>                            
                            </View>
                        </ListItem>
                        <ListItem noIndent style={{marginBottom: 10}}>
                            <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>  
                                <Text style={[styles.headerSubText, {color: '#37474F', fontFamily: 'sansmedium', fontSize: 18}]}>Here's How It Works</Text>
                            </View>
                        </ListItem>                                                
                    </List> 
                    <List  style={styles.stepsContainer}>
                        <ListItem noIndent noBorder>
                            <Grid>
                                <Col size={0.5}>
                                    <Text style={styles.stepsText}>1</Text>
                                </Col>
                                <Col size={2}>
                                    <View style={{justifyContent: 'flex-start', alignContent: 'flex-start', flex: 1}}>                                                                        
                                        <Text style={styles.stepMainText}>Fill Out The Application</Text>
                                        <Text style={styles.stepSubText}>Our application is a set of questions which will help us know you better. Get cracking on your typing skills! 💪</Text>
                                    </View>
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem noIndent noBorder>
                            <Grid>
                                <Col size={0.5}>
                                    <Text style={styles.stepsText}>2</Text>
                                </Col>
                                <Col size={2}>
                                    <View style={{justifyContent: 'flex-start', alignContent: 'flex-start', flex: 1}}>                                                                        
                                        <Text style={styles.stepMainText}>Provide Proof of Identity</Text>
                                        <Text style={styles.stepSubText}>Provide us with a Government issued ID proof such as PAN, Driver's License, Passport, Aadhar card etc. You know? Just to make sure you're a real human 🤠</Text>
                                    </View>
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem noIndent noBorder>
                            <Grid>
                                <Col size={0.5}>
                                    <Text style={styles.stepsText}>3</Text>
                                </Col>
                                <Col size={2}>
                                    <View style={{justifyContent: 'flex-start', alignContent: 'flex-start', flex: 1}}>                                                                        
                                        <Text style={styles.stepMainText}>Application Review</Text>
                                        <Text style={styles.stepSubText}>We will review your application 🤓 and may reach out to you as well if we have any questions 📞</Text>
                                    </View>
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem noIndent noBorder>
                            <Grid>
                                <Col size={0.5}>
                                    <Text style={styles.stepsText}>4</Text>
                                </Col>
                                <Col size={2}>
                                    <View style={{justifyContent: 'flex-start', alignContent: 'flex-start', flex: 1}}>                                                                        
                                        <Text style={styles.stepMainText}>You're Approved</Text>
                                        <Text style={styles.stepSubText}>We notify you of approval* 👍 Welcome! you can now start walking Furbabies 🐶</Text>
                                    </View>
                                </Col>
                            </Grid>
                        </ListItem>          
                        <ListItem noIndent style={{marginTop: 15}}>
                            <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>
                                <Text style={{ fontFamily: 'sansmedium', fontSize: 14, alignSelf: 'center', textAlign: 'center', color: '#37474F'}}>Still not sure?</Text>
                                <TouchableOpacity onPress={ () => this.openWeb('https://www.collrs.com/become-a-dog-walker#comp-jlhmfrph') }>
                                    <Text style={{ fontFamily: 'sans', fontSize: 12, alignSelf: 'center', textAlign: 'center', color: '#6c63ff'}}>Checkout what it takes to become a dog-walker</Text>
                                </TouchableOpacity>
                            </View>
                        </ListItem> 
                        <ListItem noBorder noIndent>
                            <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>
                                <Text style={{  marginTop: 20, fontSize: 12, alignSelf: 'center', textAlign: 'center', color: '#B0BEC5'}}>
                                    By applying to Collrs you confirm that you are 18 years of age or older and have read, understood, & agree to Collrs <Text style={{fontSize: 12, color: '#B0BEC5', textDecorationLine: 'underline'}} onPress={ () => this.openWeb('https://www.collrs.com/walker-tos') }>Terms of Service</Text> &
                                    <Text style={{fontSize: 12, color: '#B0BEC5', textDecorationLine: 'underline', alignSelf: 'center', textAlign: 'center'}} onPress={ () => this.openWeb('https://www.collrs.com/privacy-policy') }> Privacy Policy</Text>
                                </Text>
                                

                                <Text style={{fontSize: 10, color: '#B0BEC5', alignSelf: 'center', textAlign: 'center', fontFamily: 'sans', marginTop: 20}}>
                                    * Approval depends on our review of your application and identity verification. Applications may be reviewed on a case-by-case basis.
                                </Text>
                            </View>
                        </ListItem>               
                    </List>                                                             
                </Content>
            </Container>
            </>
        )
    }
}

const styles = StyleSheet.create({
    attribution: {
        position: 'absolute',        
        left: 0,
        right: 0                           
      },
    bodyContainer:{
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10
    },
    stepsContainer:{
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 30
    },
    listView:{
        width: '100%', 
        marginLeft: 0, 
        paddingLeft: 0, 
        paddingRight: 0, 
        marginRight: 0,
    },
    headerView:{
        flex: 1,
        width: Math.round(Dimensions.get('window').width) - 40,
        borderRadius: 20,
        borderColor: '#CFD8DC',        
        backgroundColor: 'white',
        shadowOpacity: 0.55,
        shadowRadius: 3,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 3,
        // overflow: 'hidden'
    },
    linearGradient:{
        position:'absolute',
        flex: 1,
        width:'100%',
        height:'70%'
    },
    headerText:{
        fontSize: 22,
        fontFamily: 'sansmedium',
        color: 'white',
        alignSelf: 'center',
        textAlign: 'center'
    },
    headerSubText:{
        fontSize: 16,
        marginTop: 10,
        fontFamily: 'sans',
        color: '#616161',
        alignSelf: 'center',
        textAlign: 'center'
    },
    marketingHeader:{
        width: '100%',
        height: 150,
        backgroundColor: '#ECEFF1',
        borderRadius: 20
    },
    marketingDetail:{
        padding: 12,        
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    stepsText:{ 
        fontFamily: 'sansmedium', 
        fontSize: 40, 
        color: '#90A4AE'
    },
    stepMainText:{
        fontSize: 18,        
        fontFamily: 'sansmedium',
        color: '#37474F',
        alignSelf: 'flex-start'
    },
    stepSubText:{
        fontSize: 12,
        // marginTop: 10,
        fontFamily: 'sans',
        color: '#616161',
        alignSelf: 'flex-start'
    }
  });

export default WalkerApply;