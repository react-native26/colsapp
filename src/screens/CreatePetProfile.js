import React, { Component } from 'react';
import  {StyleSheet, StatusBar, View, Keyboard, TextInput, ImageBackground, TouchableOpacity, Animated, Alert, Platform} from 'react-native';
import { Container, Thumbnail, Content, List, ListItem, Text, Separator} from 'native-base';
import { connect } from 'react-redux';
import { setUserData } from 'src/redux/reduxReducer';
import { Auth } from 'aws-amplify';
import { Col, Grid } from "react-native-easy-grid";
import { format } from "date-fns";
import AwesomeButton from "react-native-really-awesome-button";
import { LinearGradient } from 'expo-linear-gradient';
import Loading from 'src/components/ActivityHud';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import ActionSheet from 'react-native-actionsheet';
import Constants from 'expo-constants';
import FontAwesome from 'src/components/FontAwesome5';
import * as WebBrowser from 'expo-web-browser';
import CustomerForm from 'src/forms/CustomerProfileForm';
import Forms from 'src/utils/FormUtil';
import ModalPicker from 'src/components/ModalPicker';
import API from 'src/services/ServiceProvider';
import ImageServices from 'src/services/ImageServices';
import DogBreeds from 'src/components/DogBreeds';

class CreateEditPetProfile extends Component {

    constructor(props){
        super(props);

        const mode = this.props.navigation.getParam('mode') || 'new';
        this._saveType = (mode === 'new')? 'CUSTOMERCRE':'CUSTOMERMOD';
        
        this.inputRefs = {
            favSport5: null,
            favSport6: null,
            favSport7: null,
            favSport8: null
          };

        this.form = new Forms(CustomerForm);        
        const formState = this.form.GetFormState((mode === 'edit')? this.props.userdata.user:undefined);  //this.props.userdata.user
        
        //set member since date only in 'new' mode
        if (mode === 'new') formState.formFields.memberSince = format(new Date(),'dd-MMM-yyy');
        
        this.state = {
                // image: null,
                genderPopup: false,                
                mode,
                saving: false,
                animation : new Animated.Value(10),
                newImage: false,
                ...formState.formFields,
                ...formState.errorFields
            };
    }

    startAnimation=()=>{
        Animated.timing(this.state.animation,{
          toValue : 0,
          duration : 800
        }).start();
    }

    openWeb = async (url) =>{
        await WebBrowser.openBrowserAsync(url, {enableBarCollapsing: true });
    }

    getDetailsFromIDToken = () => {
        Auth.currentAuthenticatedUser()
          .then(user => {
                       
            // const isCustomer = user.signInUserSession.idToken.payload['custom:is_customer'];
            // const isWalker = user.signInUserSession.idToken.payload['custom:is_walker'];   
            // const userID = user.signInUserSession.idToken.payload.sub;

            const customerFirstName = user.signInUserSession.idToken.payload['custom:first_name'];
            const customerLastName = user.signInUserSession.idToken.payload['custom:last_name'];
            const customerEmail = user.signInUserSession.idToken.payload['email'];
            const customerProfileImage = user.signInUserSession.idToken.payload['custom:profile_image'];
            const cognitoUserName = user.signInUserSession.idToken.payload['cognito:username'];
            
            this.setState({ customerFirstName, customerLastName, customerEmail, customerProfileImage, cognitoUserName})
            
          })
          .catch((err) => {
            this.setState({ checkingAuth: false })
            console.log("Not signed in",err)
          });
    }
    
    componentDidMount(){        
        this.getDetailsFromIDToken();        
        this.props.navigation.setParams({ createSaveProfile: this.saveProfile });                                 
    }

    _buildPayload = () => {        
        const {customerEmail, customerFirstName,customerLastName,
              customerPhone, customerAddress, customerCity,
              customerPinCode, petName , petWeight, petAge, petBreed,
              petGender,houseTrained,friendlyDog,aboutPet,careInfo,vetInfo} = this.state;

        const payload = {
            cognitoUserName: this.state.cognitoUserName,
            operation: 'WRITE',
            action: this._saveType,
            data:
            {
                user:{
                    user_id: this.state.cognitoUserName,
                    customerEmail, 
                    customerFirstName,
                    customerLastName,
                    customerPhone, 
                    customerAddress, 
                    customerCity,
                    customerPinCode,
                    pet:[
                        {
                            petName , 
                            petWeight, 
                            petAge, 
                            petBreed,
                            petGender,
                            houseTrained,
                            friendlyDog,
                            aboutPet,
                            careInfo,
                            vetInfo
                        }
                    ]
                }
            }
        };
        return payload;
    }

    callAPI = async(payloadData, rawData) =>{
        let imageResult = {};
        try{            
            if(this.state.image){            
                if(this.state.newImage){
                    imageResult = await ImageServices.UploadImage({
                        imageURI: rawData.image,
                        filename: `images/users/profile/img/${rawData.cognitoUserName}_profile1`,                     
                        bucket: 'collrs-assets',
                        level: 'public'
                    });              
                }else{                    
                    imageResult['webURL'] = (this.props.userdata.user)? this.props.userdata.user.webImage : '';
                    imageResult['localURL'] = (this.props.userdata.user)? this.props.userdata.user.image : '';
                }                
            }

            // add image = web url new from image.webURL or existing from redux webImage
            const { image, ...remaining } = rawData;
            const cacheData = {
                image: imageResult.webURL,
                ...remaining,
            };
            payloadData['rawdata'] = cacheData;
            const userApiResponse = await API({lambdaName: 'collrs-user-profile', 
                                                data: payloadData});
            
            if (userApiResponse.responseCode === 200){
                //update Redux store                
                this.props.setUserData({ user: {
                    webImage: imageResult.webURL,
                    image: imageResult.localURL,
                    ...remaining,
                } });                      
                
                // logic based on mode
                if(this.state.mode === 'new'){          
                    const payload = {
                        params: {
                            UserAttributes: [
                                {
                                    Name: 'custom:is_customer',
                                    Value: 'Y'
                                }
                            ],            
                            Username: this.state.cognitoUserName
                        }
                    };
        
                    //call User Update API
                    await API({ method: 'POST', 
                                lambdaName: 'collrs-cognito-user-update', 
                                data: payload});  

                    this.props.navigation.navigate('CustomerContainer');
                    this.setState({ saving: false });
                }else{
                    this.setState({ saving: true});
                    this.props.navigation.pop();
                }
            }else{
                this._showAlert('Error','Cannot save profile at this time. Please try again later.')
            }
            
        }catch(err){  
            console.log(err);                  
            this._showAlert('Error','Cannot save profile at this time. Please try again later.')
        }  
    }

    saveProfile = () => {
        Keyboard.dismiss();
        this.setState({ saving: true });
        this.form.ValidateForm(this.state, async(err, response, data) => {                        
            if(err){                
                this.setState({ saving: false, ...response });
                // this._showAlert('Oops!','Some required fields were left blank. Please review and try again.');
                this.startAnimation();                
            }else{                                
                const payloadData = this._buildPayload();   
                this.callAPI(payloadData, data)              
            }
        })
    }    

    _showAlert = (header, error) => {
        Alert.alert(
            header,
            error,
          [
            { text: 'OK', onPress: () => this.setState({ saving: false }) },
          ],
          { cancelable: false }
        )
      }

    showActionSheet = () => {
        this.ActionSheet.show();
    }

    closeCamera = () => this.setState({ showCamera: false });

    showCamera = () => this.getPermissionsCameraAsync();

    showCameraRoll = () => {
        this.getPermissionAsync();
    }

    showGenderPopup = () => {
        Keyboard.dismiss(); //if the keyboard is on screen it will permanently dismiss it. Keyboard will show up again when the user needs to type
        this.setState({ genderPopup: true })
    }

    getPermissionsCameraAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA);
            if (status !== 'granted') {
                alert('Sorry, we need camera permissions to make this work. You can go to settings and grant Collrs App permission to access the camera.');
            }else{
                // this.setState({ showCamera: true });
                this._takePhoto();
            }
        }else{
            this._takePhoto();
        }
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work. You can go to settings and grant Collrs App Photo access.');
            }else{
                this._pickImage();
            }
        }else{
            this._pickImage();
        }
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4,4],
          quality: 0.2
        });
    
        if (!result.cancelled) {            
          this.setState({ image: result.uri, newImage: true });
        }
    }    

    _takePhoto = async () => {
        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4,4],
            quality: 0.2
          });
      
          if (!result.cancelled) {
            this.setState({ image: result.uri, newImage: true });
          }
    }

    // _handleInput = (name) => {
    //     return (text) => {
    //         this.setState({ [name]:text })
    //     }
    // }
    
    // Image BG Color #41484e  

    render() {
    
        const transformStyle ={
            transform : [{ 
              translateY : this.state.animation,
            }]
          }
        return (   
            <>
                <Container style={styles.attribution}>
                    <Content showsVerticalScrollIndicator={false} enableOnAndroid style={{ backgroundColor: '#fff'}}>   
                        <StatusBar barStyle="light-content"  />         
                        <Loading loading={this.state.saving}/>                                   
                        <List style={styles.bodyContainer} >
                            <ListItem noIndent style={{ marginLeft: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0, paddingTop: 0}}> 
                                <ImageBackground style={styles.headercontainer} source={(this.state.image)?{uri:this.state.image}: undefined} key={this.state.image}>
                                    <LinearGradient locations={[1.0, 0]}  colors= {['rgba(0,0,0,0.50)', 'rgba(0,0,0,0.50)']} start={[1.0, 0.0]} start={[1.0, 0.0]}
                                                    style={styles.linearGradient}/>
                                    <View style={{ padding: 20}}>
                                        <View style={styles.avatarStyle}>
                                            <Thumbnail large source={(this.state.image)?{uri:this.state.image}: require('../assets/images/paw.png')} style={styles.avatarImg} key={this.state.image}/>
                                        </View>
                                        <AwesomeButton 
                                                raiseLevel={0} 
                                                height={25} 
                                                stretch={false}
                                                textFontFamily="sansmedium"       
                                                textSize={12}          
                                                borderRadius={20}
                                                backgroundColor="rgba(0, 0, 0, 0.5)"
                                                backgroundDarker="rgba(0, 0, 0, 0.5)"
                                                onPress={this.showActionSheet}
                                                style={{ marginTop: 10, alignSelf: 'center'}}
                                            >
                                            <FontAwesome name={'camera-retro'} size={14} style={{color: '#fff', marginLeft: 12, marginRight: 5}}/>
                                            {
                                                (this.state.mode === 'new')?
                                                <Text style={{color: '#fff', fontFamily: 'sans', fontSize: 12, marginRight: 12}}>Add a pet photo</Text>
                                                :<Text style={{color: '#fff', fontFamily: 'sans', fontSize: 12, marginRight: 12}}>Update photo</Text>
                                            }   
                                        </AwesomeButton>
                                    </View>
                                </ImageBackground> 
                            </ListItem>
                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    <FontAwesome name={'paw'} size={26} style={styles.profileIcons}/>
                                    <Text style={styles.blockHeader}>Pet Info</Text>                            
                                </View>
                                <Text style={styles.blockSubHeader}>Basic information about your pet</Text>
                            </Separator>
                            <View style={styles.formBlock}>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Pet Name</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>                                            
                                            <TextInput placeholder="Pet name" style={styles.inputStyle} value={this.state.petName} onChangeText={text => this.setState({ petName: text })} placeholderTextColor="#rgba(158, 160, 164, 0.65)"/>
                                            {
                                                (this.state.petNameError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.petNameError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>                                                                        
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Age</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>
                                            <TextInput placeholder="Pet's age" style={styles.inputStyle} keyboardType='numeric' value={this.state.petAge} onChangeText={text => this.setState({ petAge: text })} placeholderTextColor="#rgba(158, 160, 164, 0.65)"/>
                                            {
                                                (this.state.petAgeError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.petAgeError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid> 
                                </ListItem>                                
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Breed</Text>                                                
                                            </View>
                                            <DogBreeds buttonText="Lookup Breeds" buttonStyle={{alignSelf: 'flex-start'}} onChange={(breed, weight) => this.setState({ petBreed: breed, petWeight: weight })}/>
                                        </Col>
                                        <Col size={2}>
                                            <TextInput placeholder="Pet breed" style={styles.inputStyle} value={this.state.petBreed} onChangeText={text => this.setState({ petBreed: text })} placeholderTextColor="#rgba(158, 160, 164, 0.65)"/>
                                            {
                                                (this.state.petBreedError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.petBreedError}</Animated.Text>
                                            }                                            
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Weight (approx)</Text>
                                            </View>
                                        </Col>
                                        <Col size={1}>
                                            <TextInput placeholder="in Kg" style={styles.inputStyle} keyboardType='numeric' value={this.state.petWeight} onChangeText={text => this.setState({ petWeight: text })} placeholderTextColor="#rgba(158, 160, 164, 0.65)"/>
                                            {
                                                (this.state.petWeightError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.petWeightError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>                         
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Gender</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>                                                 
                                            <ModalPicker 
                                                    placeholder='Select a gender'
                                                    value={this.state.petGender}
                                                    options={[
                                                        { label: 'Male', value: 'M' },
                                                        { label: 'Female', value: 'F' }
                                                    ]}                                                        
                                                    placeholderStyle={{alignSelf: 'flex-end'}}   
                                                    valueStyle={{alignSelf: 'flex-end'}}                                                                                
                                                    onChange={value => this.setState({petGender: value})}/>
                                            {
                                                (this.state.petGenderError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.petGenderError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>                                                
                                                <Text style={styles.formLabel}>House trained?</Text>
                                            </View>
                                        </Col>
                                        <Col size={1}>                                                                                
                                            <ModalPicker placeholder='House trained'
                                                        value={this.state.houseTrained}
                                                        options={[
                                                            { label: 'Yes', value: 'Y' },
                                                            { label: 'No', value: 'N' },
                                                            { label: 'Not sure', value: 'X' }
                                                        ]}                                                        
                                                        placeholderStyle={{alignSelf: 'flex-end'}}   
                                                        valueStyle={{alignSelf: 'flex-end'}}                                                                                
                                                        onChange={value => this.setState({houseTrained: value})}/>
                                            {
                                                (this.state.houseTrainedError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.houseTrainedError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={2}>
                                            <View style={{flexDirection: 'row'}}>                                                
                                                <Text style={styles.formLabel}>Friendly with other dogs?</Text>
                                            </View>
                                        </Col>
                                        <Col size={1}>                                                                                
                                            <ModalPicker placeholder='Friendliness'
                                                        value={this.state.friendlyDog}
                                                        options={[
                                                            { label: 'Yes', value: 'Y' },
                                                            { label: 'No', value: 'N' },
                                                            { label: 'Not sure', value: 'X' }
                                                        ]}                                                        
                                                        placeholderStyle={{alignSelf: 'flex-end'}}   
                                                        valueStyle={{alignSelf: 'flex-end'}}                                                                                
                                                        onChange={value => this.setState({friendlyDog: value})}/>
                                            {
                                                (this.state.houseTrainedError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.houseTrainedError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent noBorder>
                                    <View style={{flex: 1}}>
                                        <Text style={styles.formLabel}>About your pet</Text>
                                        <TextInput 
                                            placeholder="A short description of your pet (optional)" 
                                            placeholderTextColor="#rgba(158, 160, 164, 0.65)"
                                            multiline = {true} 
                                            numberOfLines={5} 
                                            style={{flex: 1, fontSize: 16, fontFamily: 'sans', textAlignVertical: 'top', height: 80 }}
                                            onChangeText={text => this.setState({ aboutPet: text})}
                                            value={this.state.aboutPet}
                                            />          
                                    </View>                                                    
                                </ListItem>
                            </View>
                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    <FontAwesome name={'heart-circle'} size={26} style={styles.profileIcons}/>
                                    <Text style={styles.blockHeader}>Care info</Text>                            
                                </View>
                                <Text style={styles.blockSubHeader}>Provide care information (optional)</Text>
                            </Separator>
                            <View style={styles.formBlock}>
                                <ListItem noIndent noBorder>
                                    <TextInput 
                                        placeholder="Please provide any special care instructions" 
                                        placeholderTextColor="#rgba(158, 160, 164, 0.65)"
                                        multiline = {true} 
                                        numberOfLines={5} 
                                        style={{flex: 1, fontSize: 16, fontFamily: 'sans', textAlignVertical: 'top', height: 80}}
                                        onChangeText={text => this.setState({ careInfo: text})}
                                        value={this.state.careInfo}
                                        />                        
                                </ListItem>
                            </View>
                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    <FontAwesome name={'clipboard-prescription'} size={26} style={styles.profileIcons}/>
                                    <Text style={styles.blockHeader}>Veterinary info</Text>                            
                                </View>
                                <Text style={styles.blockSubHeader}>Vet's info, medications, pre-existing conditions (optional)</Text>
                            </Separator>
                            <View style={styles.formBlock}>
                                <ListItem noIndent noBorder>
                                    <TextInput 
                                            placeholder="Please provide any veterinary information about your pet" 
                                            placeholderTextColor="#rgba(158, 160, 164, 0.65)"
                                            multiline = {true} 
                                            numberOfLines={5} 
                                            style={{flex: 1, fontSize: 16, fontFamily: 'sans', textAlignVertical: 'top', height: 80}}
                                            onChangeText={text => this.setState({ vetInfo: text})}
                                            value={this.state.vetInfo}
                                            />
                                </ListItem >
                            </View>
                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    {
                                        (this.state.customerProfileImage !== undefined)?
                                        <Thumbnail small source={{uri:this.state.customerProfileImage}} style={{right: 5}}/>
                                        :<FontAwesome name={'address-book'} size={26} style={styles.profileIcons}/>
                                    }                                                                        
                                    <Text style={styles.blockHeader}>Contact info</Text>                            
                                </View>
                                <Text style={styles.blockSubHeader}>Your name, address, email, & phone</Text>
                            </Separator>              
                            <View style={styles.formBlock}>  
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>First Name</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>
                                            <TextInput placeholder="First Name" style={styles.inputStyle} value={this.state.customerFirstName} onChangeText={text => this.setState({ customerFirstName: text })}/>
                                            {
                                                (this.state.customerFirstNameError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.customerFirstNameError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Last Name</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>
                                            <TextInput placeholder="Last Name" style={styles.inputStyle} value={this.state.customerLastName} onChangeText={text => this.setState({ customerLastName: text })}/>
                                            {
                                                (this.state.customerLastNameError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.customerLastNameError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Email</Text>
                                            </View>
                                        </Col>
                                        <Col size={3}>
                                            <Text style={[styles.inputStyle, {width: '100%'}]} numberOfLines={1}>{this.state.customerEmail}</Text>
                                            {/* <Text style={[styles.inputStyle, {width: '100%'}]} numberOfLines={1}>anjanavabababa.biswasasasas@gmail.com</Text> */}
                                            {/* <TextInput placeholder="user@email.com" style={[styles.inputStyle, {width: '100%'}]} value={'anjanavabababa.biswasasasas@gmail.com'} placeholderTextColor="#rgba(158, 160, 164, 0.65)"  numberOfLines={1}/> */}
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Phone (+91)</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>
                                            <TextInput placeholder="Phone number" style={styles.inputStyle} keyboardType='number-pad' value={this.state.customerPhone} onChangeText={text => this.setState({ customerPhone: text })} placeholderTextColor="#rgba(158, 160, 164, 0.65)"/>
                                            {
                                                (this.state.customerPhoneError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.customerPhoneError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent>
                                    <View style={{flex: 1}}>
                                        <Text style={styles.formLabel}>Address</Text>
                                        <TextInput 
                                            placeholder="Your home address" 
                                            placeholderTextColor="#rgba(158, 160, 164, 0.65)"
                                            multiline = {true} 
                                            numberOfLines={5} 
                                            style={{flex: 1, fontSize: 16, fontFamily: 'sans', textAlignVertical: 'top', height: 70}}
                                            onChangeText={text => this.setState({ customerAddress: text})}
                                            value={this.state.customerAddress}
                                            />
                                    </View>
                                    {
                                        (this.state.customerAddressError !== '')&&
                                        <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.customerAddressError}</Animated.Text>
                                    }
                                </ListItem >
                                <ListItem noIndent style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>City</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>                                            
                                            <ModalPicker placeholder='Select city'
                                                        scrollable={true}
                                                        value={this.state.customerCity}
                                                        options={[
                                                            { label: 'Mumbai', value: 'Mumbai' },
                                                            { label: 'Bangalore', value: 'Bangalore' },
                                                            { label: 'Chennai', value: 'Chennai' },
                                                            { label: 'Hyderabad', value: 'Hyderabad' },
                                                            { label: 'Ahmedabad', value: 'Ahmedabad' },
                                                            { label: 'Kolkata', value: 'Kolkata' },
                                                            { label: 'Noida', value: 'Noida' },
                                                            { label: 'Delhi', value: 'Delhi' },
                                                            { label: 'Gurgaon', value: 'Gurgaon' }
                                                        ]}                                                        
                                                        placeholderStyle={{alignSelf: 'flex-end'}}   
                                                        valueStyle={{alignSelf: 'flex-end'}}                                                                                
                                                        onChange={value => this.setState({customerCity: value})}/>
                                            {
                                                (this.state.customerCityError !== '')&&
                                                <Animated.Text style={[styles.inputErrorStyle, transformStyle]}>{this.state.customerCityError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem noIndent noBorder style={styles.formLine}>
                                    <Grid>
                                        <Col size={1}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.formLabel}>Pin Code</Text>
                                            </View>
                                        </Col>
                                        <Col size={2}>
                                            <TextInput placeholder="Area Pin Code" style={styles.inputStyle} keyboardType='numeric' value={this.state.customerPinCode} onChangeText={text => this.setState({ customerPinCode: text })} placeholderTextColor="#rgba(158, 160, 164, 0.65)"/>
                                            {
                                                (this.state.customerPinCodeError !== '')&&
                                                <Animated.Text style={[[styles.inputErrorStyle, transformStyle], transformStyle]}>{this.state.customerPinCodeError}</Animated.Text>
                                            }
                                        </Col>
                                    </Grid>
                                </ListItem>
                            </View>  
                            {
                                (this.state.mode === 'new')?
                                <>
                                    <Text style={{ marginLeft: 30, marginRight: 30, marginTop: 20, fontSize: 10, alignSelf: 'center', textAlign: 'center', color: '#B0BEC5'}}>
                                        By signing-up with Collrs you acknowledge that you have read, understood, & agree to it's <Text style={{fontSize: 10, color: '#B0BEC5', textDecorationLine: 'underline'}} onPress={ () => this.openWeb('https://www.collrs.com/terms-of-service') }>Terms of Service</Text>.                                
                                    </Text>
                                    <Text style={{fontSize: 10, color: '#B0BEC5', textDecorationLine: 'underline', alignSelf: 'center', textAlign: 'center'}} onPress={ () => this.openWeb('https://www.collrs.com/privacy-policy') }>Privacy Policy</Text>
                                </>
                                :<Separator style={{ backgroundColor: '#fff', marginBottom: (Platform.OS === 'android')? 0:60}}/> 
                            }
                                                        
                        </List>                        
                        <ActionSheet
                            ref={o => this.ActionSheet = o}
                            options={['Take Photo', 'Choose from Library', 'Cancel']}
                            cancelButtonIndex={2}
                            onPress={(index) => { 
                                if(index === 1){
                                    this.showCameraRoll();
                                }
                                if(index === 0){
                                    this.showCamera();
                                }
                                }}/>                                              
                    </Content>                    
                </Container>
            </>
        );
    }
}

const styles = StyleSheet.create({   
    attribution: {
        position: 'absolute',        
        left: 0,
        right: 0,              
      }, 
    bodyContainer:{
        width: '100%',        
        paddingBottom: 80
    },
    formLine:{
        minHeight: 60
    },
    headercontainer: {        
        width: '100%',
        backgroundColor: '#41484e'
    },
    formLabel:{
        fontFamily: 'sans',
        color: '#37474F',
        // opacity: 0.9,
        fontSize: 16,
        textAlign: 'left',
        alignSelf: 'flex-start'
    },
    inputStyle:{
        flex: 1,
        fontSize: 16,
        fontFamily: 'sans',        
        height: 25,
        textAlign: 'right',
        color: 'black'
    },
    inputErrorStyle:{
        fontFamily: 'sans',
        color: 'red',
        fontSize: 12,
        marginBottom: 0,
        textAlign: 'right',
        alignSelf: 'flex-end'
    },        
    profileIcons:{        
        color: '#37474F',
        paddingRight: 5,
        alignSelf: 'center'
    },
    avatarImg:{
        width: 100,
        height: 100,
        borderRadius: 240/2,
        resizeMode: "contain",
        alignSelf: 'center'
    },
    avatarStyle:{
        width: 110,
        height: 110,
        borderRadius: 260/2,
        backgroundColor: 'white',
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 5,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    cameraBtn:{
        flexDirection: 'row',
        // justifyContent: 'center',
        marginTop: 10,
        width: 140,
        alignSelf: 'center',
        // paddingLeft: 12, 
        // paddingRight: 14, 
        paddingTop: 6,
        paddingBottom: 6,
        borderRadius: 20,   
        backgroundColor: "rgba(0, 0, 0, 0.6)"
    },
    linearGradient:{
        position:'absolute',
        flex: 1,
        left: 0,
        right: 0,
        top: 0,
        width:'100%',
        height:'100%'
    },
    formBlock:{ 
        marginLeft: 15, 
        marginRight: 15, 
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 5,
    },
    blockHeader:{ 
        fontFamily: 'sansmedium', 
        fontSize: 24, 
        color: '#37474F'
    },
    blockSubHeader:{ 
        fontFamily: 'sans', 
        fontSize: 12
    },
    separatorStyle:{
        height: 70, 
        backgroundColor: '#fff'
    }
  });

  function mapStateToProps(state){  
    // console.log("State in header - ", state)
    return{
      userdata: state.userDataReducer.userData,
      loading: state.userDataReducer.loading
    };
  }

  function mapDispatchToProps(dispatch){
    // return bindActionCreators({
    //   fetchPermission: () => getPermission(),
    //   logOut: () => userLogout()
    // }, dispatch);
    return{
        setUserData: (userdata) => dispatch(setUserData(userdata))
    }
  }

  export default connect(mapStateToProps, mapDispatchToProps)(CreateEditPetProfile);
