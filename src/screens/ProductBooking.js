import React, { Component } from 'react';
import { Platform, View, StyleSheet, Dimensions, ImageBackground, TouchableOpacity, TextInput, Alert } from 'react-native';
import { Container, Header, Content, List, ListItem, Button, Text, Separator } from 'native-base';
import { Appearance } from 'react-native-appearance';
import { LinearGradient } from 'expo-linear-gradient';
import { Col, Row, Grid } from "react-native-easy-grid";
import { format } from "date-fns";
import ModalPicker from 'src/components/ModalPicker';
import AlertModal from 'src/components/AlertModal';
// import Modal from "react-native-modal";
// import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Loading from 'src/components/ActivityHud';
import {currencyFormatter} from 'src/utils/CurrencyFormatter';
import FontAwesome from 'src/components/FontAwesome5';
import AwesomeButton from "react-native-really-awesome-button";
import ButtonGradient from 'src/components/ButtonGradient';

const OFFER_PLACEHOLDER = 'Enter details to see offer!';
const longtext = 'If you have used Collrs before and have a "Dog Walker ID" we highly recommend entering the ID while making a new booking. This will ensure that you continue to avail services of the same walker. However, if this is your first time using Collrs then please leave "Walker ID" blank. "Walker ID" is optional.'

/*{
    product_id: '103',
    product_name: 'Major Duo',
    product_desc: 'Two Months Package',
    product_sub_desc: 'Upto 2 Walks per Day',
    duration: '30',
    duration_unit: 'mins | per session',
    currency: 'INR',
    marketing_slug: 'Best Value',
    marketing_image: 'https://collrs-assets.s3.ap-south-1.amazonaws.com/images/products/103.png',
    conditions: 'Walk days excludes Sundays',
    upc:[{
        upc_id: '1031',
        upc_description: 'Two Months',
        type: 2,            //1: daily, 2: monthly
        frequency: 1,
        frequency_units: 'walk per day',
        price: 6000.00,
        offer: 'Get 7 days free when you book this package',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 1800.00,
        savings_slug: 'Total estimated savings'
    },{
        upc_id: '1032',
        upc_description: 'Two Months',
        type: 2,            //1: daily, 2: monthly
        frequency: 2,
        frequency_units: 'walks per day',
        price: 10000.00,
        offer: 'Get 7 days free when you book this package',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 5600.00,
        savings_slug: 'Total estimated savings'
    }]
}*/

const MarketingSlug = ({ title, icon }) => {
    return(
        <View style={styles.marketingSlug}>
            <FontAwesome name={icon} style={{ color: 'white', alignSelf: 'center'}} size={16}/>
            <Text style={{ color: 'white', fontSize: 14, fontFamily: 'sansmedium', alignSelf: 'center', left: 2}}>{title}</Text>
        </View>
    );
}

class ProductBooking extends Component {
    constructor(props){
        super(props);
        this.inputRefs = {
            favSport5: null,
          };

        let date = new Date();
        date.setHours(0,0,0,0);

        this.state={
            product: '',
            product_id: '',        
            unit: '',
            upcs: [],
            offerText: '',
            offerExists: false,
            estimatedSavings: '',
            frequency: null,           //this should be sent to the DB on submission
            bookingDate: '',         //this is only for display purposes SHOULD NOT BE SENT
            initialDate: date,       //this should be sent to the DB on submission     
            product_price: 0.00,     //this should be sent to the DB on submission
            timeSlots: [],           //this should be sent to the DB on submission :: USE format(this.state.timeSlots[index],'h:mm a') before sending to
            timeSlotPosition: 0,
            specialInstr: '',        //this should be sent to the DB on submission
            walkerID: '',            //this should be sent to the DB on submission
            showDatePicker: false,
            showFreqPicker: false,
            showTimePicker: false,
            colorscheme: Appearance.getColorScheme(),
            learnmoreAlert: false
        }
    }

    componentDidMount(){
        const data = this.props.navigation.getParam('data');      
        const color= (Appearance.getColorScheme() === 'light')? 'black':'white';  
        let numOffers = 0;
        const upcs = data.upc.map(upc => {
            if (upc.offer) numOffers = numOffers + 1;
            return {
                label: `${upc.frequency} ${upc.frequency_units}`,
                value: upc.frequency,
                key: upc.price,
                color //(Appearance.getColorScheme() === 'light')?'white':'black'
            }
        })
        
        this._schemeSubscription = Appearance.addChangeListener(({ colorScheme }) => {            
            this.setState({ colorscheme: colorScheme });
        });

        this.setState({ 
            product: this.props.navigation.getParam('data') ,
            upcs: upcs,
            frequency: (upcs.length === 1)? upcs[0].value: '',
            product_price: (upcs.length === 1)? upcs[0].key: '',
            offerText: (numOffers>0)? OFFER_PLACEHOLDER: '',
            offerExists: (numOffers>0)? true: false
        });
    }

    componentWillUnmount(){        
        this._schemeSubscription.remove();
    }

    TimeSlots = () => {
        const timePickers = []
        for (var index = 0; index < this.state.frequency; index++) {
            const idx = index;
            timePickers.push(                
                <TouchableOpacity key={index} style={{paddingBottom: 8}} hitSlop={{ top: 4, right: 4, bottom: 4, left: 4 }} 
                        onPress={(event) => {                            
                            this.setState({ timeSlotPosition: idx });
                            this.datepicker('time', index);
                    }}>
                    {
                        (this.state.timeSlots[index] === undefined)?
                        <Text style={{ alignSelf: 'flex-end', opacity: 0.25, fontFamily: 'sans', fontSize: 14}}>Pick a time 00:00 AM/PM</Text>
                        :<Text style={{ alignSelf: 'flex-end', fontFamily: 'sans', fontSize: 14}}>{format(this.state.timeSlots[index],'h:mm a')}</Text>
                    }                    
                </TouchableOpacity>
                
            )
        }
        return <View style={{ flexDirection: 'column', justifyContent: 'flex-end', alignContent: 'flex-end', flex: 1}}>{timePickers}</View>;
    }

    //***** DatePicker helpers */
    setDate = (date) => {

        const bookingDate = date || this.state.bookingDate;
        if(this.state.showTimePicker){            
            const {timeSlots} = this.state;
            timeSlots[this.state.timeSlotPosition] = date;                     
            this.setState({ 
                // showTimePicker: Platform.OS === 'ios' ? true : false,
                showTimePicker: false,
                timeSlots 
            });
        }

        if(this.state.showDatePicker){
            bookingDate.setHours(0,0,0,0);
            this.setState({
                // showDatePicker: Platform.OS === 'ios' ? true : false,
                showDatePicker: false,
                bookingDate: format(bookingDate, 'LLL d, yyyy') ,
                initialDate: bookingDate
            });
        }        
      }
    
      show = mode => {
        this.setState({
            showDatePicker: (mode === 'date')? true:false,
            showTimePicker: (mode === 'time')? true:false,
            mode,
        });
      }
    
    datepicker = (mode) => {        
        this.show(mode);
    }
    //**** Date Picker helpers */

    //** Frequency Picker helpers */

    showFreq = () => this.setState({ showFreqPicker: true})    
    //** Frequency Picker Helpers */

    render() {
        
        const { upcs, timeSlots, timeSlotPosition } = this.state;
        // const timeStamp = (timeSlots[timeSlotPosition])? timeSlots[timeSlotPosition] : new Date();
        const hasInfo = (this.state.product.conditions !== undefined && this.state.product.conditions !== '')? true:false;

        return (
            <Container showsVerticalScrollIndicator={false}>
            {
                (!this.state.product)?
                <Loading loading={this.state.loading}/>
                :<Content showsVerticalScrollIndicator={false} enableOnAndroid>
                    <List style={styles.bodyContainer} >
                        <ListItem noIndent noBorder style={styles.headerListView}>
                            <ImageBackground source={{uri: this.state.product.marketing_image}} style={styles.headerImage}>
                                <LinearGradient locations={[0, 1.0]} colors= {['rgba(0,0,0,0.00)', 'rgba(0,0,0,0.60)']} style={styles.linearGradient}/>
                                <View  style={{ flex: 1, padding: 0, justifyContent: 'flex-end', padding: 15}}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
                                        <Text style={styles.productName}>Book {this.state.product.product_name}</Text>
                                        {
                                            (this.state.product.marketing_slug !== '')&&
                                            <MarketingSlug title={this.state.product.marketing_slug} icon="star"/>
                                        }
                                    </View>
                                </View>                                                     
                            </ImageBackground>
                        </ListItem>
                        <ListItem noIndent noBorder>
                            <View style={{ flex: 1}}>
                                <Text style={styles.bookingHeaderText}>{this.state.product.product_desc}</Text>
                                <Text style={styles.bookingHeaderText}>{this.state.product.product_sub_desc}</Text>
                                <Text style={styles.bookingHeaderText}>{this.state.product.duration} {this.state.product.duration_unit}</Text>     
                            </View>                           
                        </ListItem>
                        {
                            (this.state.offerExists === true)?
                            <ListItem noIndent noBorder style={{ backgroundColor: '#4CAF50', paddingLeft: 0}} > 
                                <View style={{flexDirection: 'row', marginLeft: 10}}>
                                    <FontAwesome fatype='solid' name={'badge-percent'} size={28} style={styles.offerIcon} />
                                    <Text style={styles.offerHeaderText} numberOfLines={2}>{this.state.offerText}</Text>
                                </View>
                            </ListItem>
                            :<Separator style={{height: 15}}/>
                        }                         
                        <Separator style={styles.separatorStyle}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={styles.blockHeader}>Details</Text>                            
                            </View>
                            <Text style={styles.blockSubHeader}>Enter booking details</Text>
                        </Separator>     
                        <View style={styles.formBlock}>
                        <ListItem noIndent style={styles.bookingListItem}>
                            <Grid>
                                <Col size={1}>
                                    <View style={{flexDirection: 'row'}}>
                                        <FontAwesome name={'repeat'} size={14} style={styles.bookingIcons}/>
                                        <Text style={styles.labelText}>Frequency</Text>
                                    </View> 
                                </Col>
                                <Col size={2}>                                    
                                {
                                    (upcs.length === 1)?
                                        <Text style={{textAlign: 'right', alignSelf: 'flex-end', fontSize: 14}}>{upcs[0].label}</Text>
                                    :
                                    <View style={{flex: 1}}>                                        
                                        <ModalPicker placeholder='Chose a frequency'
                                                        options={upcs}                                                        
                                                        placeholderStyle={{alignSelf: 'flex-end', fontSize: 14}}   
                                                        valueStyle={{alignSelf: 'flex-end', fontSize: 14}}
                                                        onChange={(value, index) => {
                                                            const { offer, estimated_savings, savings_slug } =  this.state.product.upc[index];
                                                                this.setState({ frequency: value, 
                                                                                product_price: (value)?upcs[index].key:0.00, 
                                                                                offerText: (!value && this.state.offerExists)? OFFER_PLACEHOLDER: offer,
                                                                                estimatedSavings: (!value)? '': savings_slug+' '+currencyFormatter(estimated_savings),
                                                                                timeSlots: [] })
                                                            }}/>
                                    </View>
                                }

                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem noIndent style={(hasInfo === true)? undefined:styles.bookingListItem}>
                            <Grid>
                                <Col size={3}>
                                    <View style={{flexDirection: 'row'}}>
                                        <FontAwesome name={'calendar-alt'} size={14} style={styles.bookingIcons}/>
                                        <Text style={styles.labelText}>Start Date</Text>
                                    </View> 
                                    {
                                        (hasInfo === true) &&
                                        <View style={{ flexDirection: 'row', paddingTop: 5}}>
                                            <FontAwesome name={'info-circle'} size={11} style={styles.bookingIcons}/>
                                            <Text style={styles.infoText}>{this.state.product.conditions}</Text>
                                        </View> 
                                    }                                    
                                </Col>
                                <Col size={2}>                                    
                                    <TouchableOpacity style={{ flex: 1, height: '100%' }} onPress={() => this.datepicker('date')}>
                                        {
                                            (this.state.bookingDate === '')?
                                            <Text style={{textAlign: 'right', alignSelf: 'flex-end', opacity: 0.25, fontFamily: 'sans', fontSize: 14}}>Pick a date</Text>
                                            :<Text style={{textAlign: 'right', alignSelf: 'flex-end', fontSize: 14}}>{this.state.bookingDate}</Text>
                                        }                                        
                                    </TouchableOpacity>                                    
                                </Col>
                            </Grid>
                        </ListItem>
                        {
                            (this.state.frequency !== null && this.state.frequency > 0) &&
                            <ListItem noIndent>                                                    
                                <Grid>
                                    <Col size={1}>
                                        <View style={{flexDirection: 'row'}}>
                                            <FontAwesome name={'clock'} size={14} style={styles.bookingIcons}/>
                                            <Text style={styles.labelText}>Time</Text>
                                        </View> 
                                    </Col>                                
                                    <Col size={2}>
                                        {
                                            this.TimeSlots()
                                        }
                                    </Col>
                                </Grid>                        
                            </ListItem>
                        }                        
                        <ListItem noIndent >
                            <Grid>
                                <Row>
                                    <View style={{flexDirection: 'row', paddingBottom: 2}}>
                                        <FontAwesome name={'clipboard'} size={14} style={styles.bookingIcons}/>
                                        <Text style={styles.labelText}>Special Instructions</Text>
                                    </View> 
                                </Row>
                                <Row >                                    
                                    <TextInput 
                                        placeholder="Please provide any special instructions (optional)" 
                                        placeholderTextColor="#rgba(158, 160, 164, 0.65)"
                                        multiline = {true} 
                                        numberOfLines={5} 
                                        style={{flex: 1, fontSize: 14, fontFamily: 'sans', textAlignVertical: 'top', height: 60}}
                                        onChangeText={text => this.setState({ specialInstr: text})}
                                        value={this.state.specialInstr}/>
                                </Row>
                            </Grid>
                        </ListItem>
                        <ListItem noIndent style={{...styles.bookingListItem, height: 70}} noBorder>
                            <Grid>
                                <Col size={1}>
                                    <View style={{flexDirection: 'row'}}>
                                        <FontAwesome name={'walking'} size={14} style={styles.bookingIcons}/>
                                        <Text style={styles.labelText}>Walker ID</Text>
                                    </View> 
                                    <View style={{ flexDirection: 'row', paddingTop: 5}}>
                                        <FontAwesome name={'question-circle'} size={11} style={{...styles.bookingIcons,color: '#6c63ff'}}/>
                                        <TouchableOpacity onPress={() => this.setState({ learnmoreAlert: true })}>
                                            <Text style={{...styles.infoText, color: '#6c63ff'}}>Learn more</Text>
                                            <AlertModal 
                                                visible={this.state.learnmoreAlert}             // show hide modal
                                                // scrollable={true}                            // if the modal content needs to be scrollable (defaults to false)
                                                header={'Walker ID'}                            // header text for the modal popup
                                                // headerStyle = {{...}}                        // header text style override object
                                                content={longtext}                              // modal popup body content text
                                                // contentstyle = {{...}}                       // body content text style object override
                                                headerIcon={'walking'}                          // (optional) Icon to be set on the modal
                                                headerIconStyle={{color: '#6c63ff'}}            // (optional) override Icon Style         
                                                okText={'Got it!'}                           // (optional) OK button text 
                                                // cancelText={'Cancel'}                        // (optional) Cancel Button text
                                                // onOk={() => console.log('Ok Pressed')}       // (optional) call back function when OK button is tapped
                                                // onCancel={() => console.log('Cancel Pressed')}   //(optional) call back function when Cancel button is tapped
                                                // okButtonStyle = {{...}}                      // (optional) OK Button custom text style object
                                                // cancelButtonStyle = {{...}}                  // (optional) Cancel button custom style object
                                                onDismissed={() => this.setState({ learnmoreAlert: false })}  // (optional) Callback function when modal is dismissed
                                                // animationIn={''}                             // (optional) animation in for the modal (default slideInUp)
                                                // animationOut={''}                            // (optional) animation out for the modal (default slideOutDown)
                                                />
                                        </TouchableOpacity>
                                    </View>
                                </Col>
                                <Col size={2}>  
                                    <TextInput 
                                        placeholder="Walker ID (optional)" 
                                        style={styles.inputStyle}
                                        placeholderTextColor="#rgba(158, 160, 164, 0.65)"                                        
                                        onChangeText={text => this.setState({ walkerID: text})}
                                        value={this.state.walkerID}
                                        />
                                </Col>
                            </Grid>
                        </ListItem>
                        </View>
                        <Separator style={styles.separatorStyle}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={styles.blockHeader}>Booking Total</Text>                            
                            </View>
                            {
                                (this.state.estimatedSavings !== '') &&
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', paddingTop: 5}}>
                                    <FontAwesome name={'info-circle'} size={11} style={styles.bookingIcons}/>
                                    <Text style={styles.infoText}>{this.state.estimatedSavings}</Text> 
                                </View>
                            } 
                        </Separator>

                        <ListItem noIndent noBorder style={styles.bookingListItem}>
                            <Grid>
                                <Col size={2}>
                                    <View style={{flexDirection: 'row'}}>
                                        <FontAwesome name={'tag'} size={14} style={styles.bookingIcons}/>
                                        <Text style={styles.labelText}>Price</Text>
                                    </View>
                                                                      
                                </Col>
                                <Col size={1}>
                                    <Text style={{ textAlign: 'right', alignSelf: 'flex-end' , fontFamily: 'sans', fontSize: 16}}>{currencyFormatter(this.state.product_price)}</Text>
                                </Col>
                            </Grid>
                        </ListItem>
                        <View style={{marginLeft: 15, marginRight: 15}}>
                        <AwesomeButton 
                                    raiseLevel={0} 
                                    height={50} 
                                    stretch={true}
                                    textFontFamily="sansmedium"       
                                    textSize={16}          
                                    borderRadius={50}    
                                    ExtraContent={
                                        <ButtonGradient/>
                                      }
                                    // onPress={() => Linking.openURL(`tel:${this.context.walker_contact}`)}
                            >
                                <FontAwesome name={'paper-plane'} size={18} style={{color: '#fff', marginLeft: 5, marginRight: 5}}/>
                                <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 16}}>Complete Booking</Text>
                            </AwesomeButton>
                        </View> 
                        <Separator style={{backgroundColor: '#fff', height: 15}}/>
                    </List>
                    <DateTimePickerModal
                        minimumDate={(this.state.showDatePicker)? new Date(): undefined}
                        isVisible={this.state.showDatePicker || this.state.showTimePicker}
                        mode={this.state.mode}
                        onConfirm={this.setDate}
                        onCancel={() => this.setState({ showDatePicker: false, showTimePicker: false })}
                        isDarkModeEnabled={(this.state.colorscheme === 'light')?false:true}
                        confirmTextIOS={'Done'}
                    />
                </Content>
            }            
            </Container>
        )
    }
}

const styles = StyleSheet.create({  
    bodyContainer:{
        width: '100%',        
        backgroundColor: 'white'
    },
    headerListView:{
        width: '100%', 
        height: 200,
        marginLeft: 0, 
        paddingLeft: 0, 
        paddingRight: 0, 
        marginRight: 0,
    },
    headerImage:{
        width: '100%',
        height: 200,
    },
    productName:{
        fontFamily: 'sansmedium',
        color: '#fff',        
        fontSize: 24,
        alignSelf: 'flex-start'
    },
    linearGradient:{
        position:'absolute',
        flex: 1,
        width:'100%',
        height:'100%'
    },
    bookingIcons:{        
        color: '#616161',
        paddingRight: 5,
        alignSelf: 'center'
    },
    bookingHeaderText:{
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 16,
        textAlign: 'left',
        alignSelf: 'flex-start'
    },
    conditionText:{
        fontFamily: 'sans',
        // color: '#37474F',
        opacity: 0.3,
        fontSize: 12,
        textAlign: 'left',
        alignSelf: 'flex-start'
    },
    infoIcons:{        
        opacity: 0.3,
        paddingRight: 5,
        alignSelf: 'center'
    },
    labelText:{
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 14,
        textAlign: 'left',
        alignSelf: 'flex-start'
    },
    bookingListItem:{
        height: 50
    },
    marketingSlug:{
        flexDirection: 'row',
        // paddingTop: 2, 
        // paddingBottom: 2, 
        paddingLeft: 6, 
        paddingRight: 8, 
        borderRadius: 20,
        // borderTopLeftRadius: 20, 
        // borderBottomLeftRadius: 20, 
        backgroundColor: '#FFA000'
    },
    infoText:{
        fontFamily: 'sans',        
        color: '#37474F',
        opacity: 0.75,
        fontSize: 13,                
        alignSelf: 'center'
    },
    offerIcon:{
        // color: '#FFA000',
        color: '#fff',
        paddingRight: 5,
        alignSelf: 'center'
    },
    offerHeaderText:{
        fontFamily: 'sans',
        // color: '#37474F',        
        color: '#fff',
        fontSize: 15,
        textAlign: 'left',
        marginRight: 10
        // alignSelf: 'center'
    },
    formBlock:{ 
        marginLeft: 15, 
        marginRight: 15, 
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
    },        
    profileIcons:{        
        color: '#37474F',
        paddingRight: 5,
        alignSelf: 'center'
    },
    blockHeader:{ 
        fontFamily: 'sansmedium', 
        fontSize: 24, 
        color: '#37474F'
    },
    blockSubHeader:{ 
        fontFamily: 'sans', 
        fontSize: 12
    },
    separatorStyle:{
        height: 70, 
        backgroundColor: '#fff'
    },
    inputStyle:{
        flex: 1,
        fontSize: 14,
        fontFamily: 'sans',        
        height: 25,
        textAlign: 'right',
        color: 'black'
    }
});

export default ProductBooking;