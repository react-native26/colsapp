import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet,  View, ImageBackground, Platform, StatusBar, Alert, Text, Image, TouchableOpacity } from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import Amplify, { Auth, Hub } from 'aws-amplify';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserData, setWalkerApplicationData } from 'src/redux/reduxReducer';
import { AppLoading, SplashScreen } from 'expo';
import { Asset } from 'expo-asset';
import { GoogleSocialButton, FacebookSocialButton, GitHubSocialButton } from "react-native-social-buttons";
import { withOAuth } from "aws-amplify-react-native";
import awsconfig from '../aws-exports';
import * as Font from 'expo-font';
import FontAwesome from 'src/components/FontAwesome5';

Amplify.configure(awsconfig);

class SplashInitialScreen extends Component {
    _isMounted = false;

    state = { 
        user: null, 
        customState: null, 
        fontloaded: false, 
        checkingAuth: true,
        loggingIn: false,
        isSplashReady: false,
        isAppReady: false,
        authError: ''
       };

    openWeb = async (url) =>{
      await WebBrowser.openBrowserAsync(url, {enableBarCollapsing: true });
    }

    onAuthChange = ({ payload: { event, data } }) => {
      // console.log("EVENT = ", event);
      switch (event) {
        case "signIn":
          this.navigateUser(true);
          break;
        case 'signIn_failure':
          this._showAlert("Error", this.props.oAuthError);
          break;
        case "loginCancelled":
          this.setState({ loggingIn: false });
          break;
        default:
          break;
      }
    }

    _showAlert = (header, error) => {
      Alert.alert(
          header,
          error,
        [
          { text: 'OK', onPress: () => this.setState({loggingIn: false}) },
        ],
        { cancelable: false }
      )
    }
    
    async componentDidMount() {
      this._isMounted = true;
      Hub.listen("auth", this.onAuthChange);
      Hub.listen("authChannel", this.onAuthChange);
      // this.props.setWalkerApplicationData({ walkerapplied: false, status: '' });
    }

    componentWillUnmount() {      
      Hub.remove("auth", this.onAuthChange);
      Hub.remove("authChannel", this.onAuthChange);
      this._isMounted = false;
    }
    
    navigateUser = (signin = false) => {
      Auth.currentAuthenticatedUser()
        .then(async(user) => {

          const isCustomer = user.signInUserSession.idToken.payload['custom:is_customer'];
          const isWalker = user.signInUserSession.idToken.payload['custom:is_walker'];  
          const userName = user.signInUserSession.idToken.payload['cognito:username'];  
          // this.props.navigation.navigate('CustomerContainer');
          if(signin && (isCustomer || isWalker)){
            //load profile data from S3 prefs file (Redux Reducer)
            if(isCustomer){
              this.props.getUserData(userName);
            }            
          }

          if (isCustomer === 'Y'){            
            this.props.navigation.navigate('CustomerContainer');
          }else if (isWalker === 'Y'){
            const status = user.signInUserSession.idToken.payload['custom:walker_status'] || this.props.applicationData.status;
            this.setState({loggingIn: false});

            if( status === 'APPLIED_PENDING_DOC' || status === 'PENDING_REVIEW'){
              this.props.setWalkerApplicationData({ walkerapplied: true, status: status });
              this.props.navigation.navigate('WalkerApplication');
            }else if(status === 'APPROVED'){              
              // this.props.navigation.navigate('WalkerContainer');
            }else{
              this.props.navigation.navigate('CompleteRegister');
            }
            
          }else{ //user needs to complete registration as a Customer or walker
            this.props.navigation.navigate('CompleteRegister');
          }
        })
        .catch((err) => {
          if(this._isMounted){
            this.setState({ checkingAuth: false });
          }          
          console.log("Not signed in",err)
        });
    }

    signOut = () => {
      Auth.signOut()
      .then(data => {
        if(this._isMounted){
          this.setState({ user: null });
        }
      })
      .catch(err => console.log(err));
    }

    _cacheSplashResourcesAsync = async () => {
      const images = [
        require('../assets/images/welcome.png'),
        require('../assets/images/home1.png'),
        require('../assets/images/home2.png'),
        require('../assets/images/home3.png'),
        require('../assets/images/paw.png'),
        require('../assets/images/payu.png'),
        require('../assets/images/started.png'),
        require('../assets/images/walker.png'),
        require('../assets/images/LogoText.png'),
      ];

      const cacheAssets = images.map(image => {
        return Asset.fromModule(image).downloadAsync();
      });
      
      cacheAssets.push(Font.loadAsync({
          'pacifico': require('../assets/fonts/Pacifico-Regular.ttf'),
          'sans': require('../assets/fonts/OpenSans-Regular.ttf'),
          'sanslight': require('../assets/fonts/OpenSans-Light.ttf'),
          'sansmedium': require('../assets/fonts/OpenSans-SemiBold.ttf'),
          'Roboto_medium': require('../assets/fonts/Roboto-Medium.ttf'),
          'fa': require('../assets/fonts/fa-light-300.ttf'),
          'fas': require('../assets/fonts/fa-solid-900.ttf'),
          'far': require('../assets/fonts/fa-regular-400.ttf'),
          'fab': require('../assets/fonts/fa-brands-400.ttf'),
      }));

      return Promise.all(cacheAssets);
    };
  
    render() {
      // const { user } = this.state;

      // console.log("Is APP Ready = ", this.state.isSplashReady);
      
      if (!this.state.isSplashReady) {
        return (
          <>
            <StatusBar translucent barStyle="light-content" />
            <AppLoading
              startAsync={this._cacheSplashResourcesAsync}
              onFinish={() => {
                this.navigateUser();
                if(this._isMounted){
                  this.setState({ isSplashReady: true });
                }              
              }}
              onError={console.warn}
              autoHideSplash={true}
            />
          </>
        );
      }

      return (
        <ImageBackground source={require('../assets/images/welcome.png')} style={{width: '100%', height: '100%'}}>
          {
            (Platform.OS === 'ios')&&
              (this.state.isSplashReady) ? 
              <StatusBar translucent barStyle="dark-content" />
              :<StatusBar translucent barStyle="light-content" />            
          }          
          <View style={styles.container}>
                <Image source={require('../assets/images/LogoText.png')} style={{width: '70%', height: 70, marginBottom: 20}}/>
                <Text style={{fontFamily: 'sans', fontSize: 16}}>India's Largest Network of Dog-Walkers</Text>                                
          </View>
          <View style={styles.buttonContainer}>
          {
              (this.state.checkingAuth || this.state.loggingIn)?
              <ActivityIndicator size="large" style={{ marginBottom: 50 }}/>
              : <>
                {
                  (Platform.OS === 'ios')&&
                  <TouchableOpacity
                      style={{ ...styles.appleButton }}
                      // onPress={this.props.onPress}
                    >              
                      <FontAwesome name={'apple'} size={26} fatype={'brand'} style={styles.appleIcon}/>        
                      <Text style={{...styles.appleText, ...styles.buttonTextStyle}}>
                        Sign in with Apple
                      </Text>
                    </TouchableOpacity>           
                }
                <GoogleSocialButton 
                  onPress={() => {
                        if(this._isMounted){
                          this.setState({ loggingIn: true });
                        }                  
                        this.props.googleSignIn();  //See: https://aws-amplify.github.io/docs/js/authentication#react-components   , this.props.googleSignIn();              
                      }} 
                  buttonText="Sign in with Google"
                  buttonViewStyle={styles.socialButton} 
                  textStyle={styles.buttonTextStyle}/>
                <FacebookSocialButton 
                    onPress={() => {
                      if(this._isMounted){
                        this.setState({ loggingIn: true });
                      }                  
                      this.props.facebookSignIn();  //See: https://aws-amplify.github.io/docs/js/authentication#react-components   , this.props.facebookSignIn();              
                    }} 
                    buttonViewStyle={styles.fbButton} 
                    buttonText="Sign in with Facebook" 
                    textStyle={styles.buttonTextStyle}/>
                </>
          }            
            <Text style={{fontFamily: 'sans', marginBottom: 50, marginTop: 50, fontSize: 12}}>
              Collrs {' '}
              <Text style={{textDecorationLine: 'underline'}} onPress={ () => this.openWeb('https://www.collrs.com/terms-of-use') }>
                Terms of Use
              </Text> and{' '}
              <Text style={{textDecorationLine: 'underline'}} onPress={ () => this.openWeb('https://www.collrs.com/privacy-policy') }>
                Privacy Policy
              </Text>
            </Text>
          </View>
        </ImageBackground>
      );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      // marginBottom: 200    
    },
    // statusBar: {
    //   height: Constants.statusBarHeight,
    //   opacity: 0.2
    // },
    buttonContainer:{   
      position: 'absolute',   
      justifyContent: 'center',
      alignItems: 'center',
      bottom: 0,
      width: '100%'
    },
    buttonTextStyle:{
      fontFamily: 'sans',
      fontSize: 16
    },
    socialButton:{      
      height: 45,
      borderRadius: 45,
      borderWidth: 1,
      borderColor: '#BDBDBD',
      width: '80%',
      marginBottom: 10
    },
    fbButton:{      
      height: 45,
      borderRadius: 45,
      width: '80%',
      marginBottom: 10
    },
    appleButton:{      
      height: 45,
      borderRadius: 45,
      width: '80%',
      marginBottom: 10,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#333",
      borderWidth: 0.5,
      borderColor: "#fff",
      margin: 5
    },
    appleText: {
      color: "#fff",
    },
    appleIcon:{      
      marginRight: 15,
      color: "#fff",
    },
    welcome: {
      fontSize: 60,
      color: '#6c63ff',
      fontFamily: 'pacifico',    
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });
  
const SplashInit = withOAuth(SplashInitialScreen)

function mapStateToProps(state){
  // console.log(state.userWalkerApplicationReducer.applicationData)
  return{
      applicationData: state.userWalkerApplicationReducer.applicationData
    };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getUserData: (username) => getUserData(username),
    setWalkerApplicationData: (applicationData) => setWalkerApplicationData(applicationData)
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SplashInit);