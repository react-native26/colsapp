import React, { Component } from 'react';
import  {StyleSheet, Platform, View, StatusBar } from 'react-native';
import { Container, Header, Content, Left, Body, Right, Title, Tabs, Tab } from 'native-base';
import BookingDeails from './CustomerBookingDetails/BookingDetails';
import PaymentDetails from './CustomerBookingDetails/PaymentDetails';
import Constants from 'expo-constants';
import { CustomerBookingProvider } from '../context/CustomerBookingContext';

class CustomerBookingDetails extends Component {
    
    state = {
        paymentEnabled: false
    }

    componentDidMount(){
        const paymentEnabled = this.props.navigation.getParam('data').booking_confirmed;
        this.setState({ paymentEnabled });        
    }

    render() {
        return (
            <Container>
                    <StatusBar barStyle="light-content"  />
                    <Tabs tabBarUnderlineStyle={{ backgroundColor: '#6c63ff', height: 1}} locked={!this.state.paymentEnabled}>
                        <Tab heading='Details' textStyle={{ fontFamily: 'sans', fontSize: 16}} activeTextStyle={{ fontFamily: 'sans', fontSize: 16, color: '#6c63ff'}} tabStyle={{backgroundColor: '#fff'}} activeTabStyle={{backgroundColor: '#fff'}}>
                            <CustomerBookingProvider value={this.props.navigation.getParam('data')}>
                                <BookingDeails/>
                            </CustomerBookingProvider>
                        </Tab>
                        {
                            (this.state.paymentEnabled === true)?
                            <Tab heading='Payment' textStyle={{ fontFamily: 'sans', fontSize: 16}} activeTextStyle={{ fontFamily: 'sans', fontSize: 16, color: '#6c63ff'}} tabStyle={{backgroundColor: '#fff'}} activeTabStyle={{backgroundColor: '#fff'}}>
                                <CustomerBookingProvider value={this.props.navigation.getParam('data')}>
                                    <PaymentDetails/>  
                                </CustomerBookingProvider>
                            </Tab>
                            :<Tab disabled={true} heading='Payment' textStyle={{ fontFamily: 'sans', fontSize: 16}} activeTextStyle={{ fontFamily: 'sans', fontSize: 16, color: '#6c63ff'}} tabStyle={{backgroundColor: '#fff'}} activeTabStyle={{backgroundColor: '#fff'}}/>                                
                        }                        
                    </Tabs>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    statusBar: {
        backgroundColor: "#6c63ff",
        height: Constants.statusBarHeight,
      }
});

export default CustomerBookingDetails;