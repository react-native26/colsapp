import React, { Component } from 'react'
import { Auth } from 'aws-amplify';
import  {StyleSheet, View, Text, Image, Dimensions, Platform, StatusBar, TouchableOpacity } from 'react-native';
import { Form, Item, Input, Label, Container } from 'native-base';
import AwesomeButton from "react-native-really-awesome-button";
import Constants from 'expo-constants';
import { Header } from 'react-navigation';

class AcceptInviteCustomer extends Component {
    render() {
        return (
            // <KeyboardAwareScrollView enableOnAndroid={true} extraScrollHeight={(Platform.OS === 'ios')? 30:150} >
                <View style={{width: '100%', height: '100%'}}>
                    {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}                
                    <View style={styles.container}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center'}} >
                            <Text style={styles.heaederText}>Register</Text>
                        </View>
                    </View>
                    
                    <View style={styles.logincontainer}>                    
                        <Form>
                            <Item floatingLabel >
                                <Label style={styles.formLabel}>Email</Label>                                    
                                <Input/>                                                                                                           
                            </Item>
                            <Text style={styles.formSubLabel}>Your email address.</Text>

                            <Item floatingLabel>
                                <Label style={styles.formLabel}>Password</Label>
                                <Input secureTextEntry={true}/>
                            </Item>
                            <Text style={styles.formSubLabel}>Enter a new password that you can remember</Text>

                            <Item floatingLabel>
                                <Label style={styles.formLabel}>Invite Code</Label>
                                <Input/>
                            </Item>
                            <Text style={styles.formSubLabel}>Invite code from the invitation email</Text>
                        </Form>
                    </View>

                    <View style={styles.btncontainer}>
                        <AwesomeButton 
                            raiseLevel={0} 
                            height={44} 
                            stretch={true}
                            onPress={next => this.props.navigation.navigate('CreatePetProfile')}
                            textFontFamily="sans" 
                            textSize={16} 
                            style={styles.btn2} 
                            backgroundColor="#6c63ff" 
                            borderColor="#fff" 
                            borderWidth={0.5}>
                                Continue
                        </AwesomeButton>
                        <TouchableOpacity style={{top: 20}}>
                            <Text style={{color: '#6c63ff', fontFamily: 'sansmedium'}}>Why do I need an invite code?</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.footerContent}>
                        <View style={{ bottom: 10 }}>
                            <Text style={{color: '#546E7A', fontSize: 16}}>Already have an account?</Text>                            
                        </View>                        
                        <View style={{alignSelf: 'center'}}>
                            <TouchableOpacity>
                                <Text style={{color: '#6c63ff', fontSize: 16, fontFamily: 'sansmedium'}}>Login</Text>                            
                            </TouchableOpacity>
                        </View>                        
                    </View>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {    
        // flex: 1,
        marginTop: Constants.statusBarHeight + Header.HEIGHT //(Platform.OS === 'ios')? 120 : 100    
    },
    heaederText:{        
        fontSize: 21,   
        fontFamily: 'sansmedium',
        color: '#37474f'
    },
    logincontainer: {
        // flex: 1,
        marginLeft: 20,
        marginRight: 30,
        // justifyContent: '',
        alignContent: 'stretch',
        // alignItems: 'center',
        // marginBottom: (Platform.OS === 'ios')? 100 : 90
        // backgroundColor: 'white',
        // borderRadius: 10,
        // shadowOpacity: 0.25,
        // shadowRadius: 5,
        // shadowColor: '#607D8B',
        // shadowOffset: { height: 0, width: 0 },
        // elevation: 5         
    },
    formLabel:{
        fontSize: 16,
        fontFamily: 'sans',
        color: '#546E7A'
    },
    formSubLabel:{
        fontSize: 14,
        fontFamily: 'sans',
        marginLeft: 14,
        color: '#B0BEC5'
    },
    btncontainer: {
        // flex: 1,
        margin: 30,
        margin: 30,        
        alignItems: 'center',
        justifyContent: 'space-between'       
    },
    footerContent:{
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: (Platform.OS === 'ios')? 100 : 90
    }
  });

export default AcceptInviteCustomer;
