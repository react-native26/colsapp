import React, { Component } from 'react';
import { Platform, View, StyleSheet, Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Button, Text } from 'native-base';
import { withNavigation } from 'react-navigation';
import { Col, Row, Grid } from "react-native-easy-grid";
import { LinearGradient } from 'expo-linear-gradient';
// import {currencyFormatter} from 'src/utils/CurrencyFormatter';
import AwesomeButton from "react-native-really-awesome-button";
import Constants from 'expo-constants';
import FontAwesome from 'src/components/FontAwesome5';
import CornerLabel from 'src/components/CornerLabel';
import Loading from 'src/components/ActivityHud';
import * as WebBrowser from 'expo-web-browser';
import ButtonGradient from 'src/components/ButtonGradient';

const products = [{
    product_id: '101',
    product_name: 'Marshall',
    product_desc: 'Our Daily Starter Package',
    product_sub_desc: 'Single Day, Morning & Evening Walks',
    duration: '30',
    duration_unit: 'mins | per session',
    currency: 'INR',
    marketing_slug: '',
    marketing_url: 'https://www.collrs.com',
    marketing_image: 'https://collrs-assets.s3.ap-south-1.amazonaws.com/images/products/101.png',
    conditions: '',
    upc:[{
        upc_id: '1011',
        upc_description: 'Single Day, Morning & Evening Walks',
        type: 1,            //1: daily, 2: monthly
        frequency: 2,
        frequency_units: 'walks per day',
        price: 300.00,
        offer: '',
        conditions: '',
        estimated_savings: 0.00,
        savings_slug: ''
    }]
},{
    product_id: '102',
    product_name: 'Major',
    product_desc: 'Our Monthly Package',
    product_sub_desc: 'Upto 2 Walks per Day',
    duration: '30',
    duration_unit: 'mins | per session',
    currency: 'INR',
    marketing_slug: 'Most Popular',
    marketing_url: 'https://www.collrs.com',
    marketing_image: 'https://collrs-assets.s3.ap-south-1.amazonaws.com/images/products/102.png',
    conditions: 'Walk days excludes Sundays',
    upc:[{
        upc_id: '1021',
        upc_description: 'One Month',
        type: 2,            //1: daily, 2: monthly
        frequency: 1,
        frequency_units: 'walk per day',
        price: 2999.00,
        offer: '',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 900.00,
        savings_slug: 'Total estimated savings'
    },{
        upc_id: '1022',
        upc_description: 'One Month',
        type: 2,            //1: daily, 2: monthly
        frequency: 2,
        frequency_units: 'walks per day',
        price: 4999.00,
        offer: '',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 3000.00,
        savings_slug: 'Total estimated savings'
    }]
},{
    product_id: '103',
    product_name: 'Major Duo',
    product_desc: 'Two Months Package',
    product_sub_desc: 'Upto 2 Walks per Day',
    duration: '30',
    duration_unit: 'mins | per session',
    currency: 'INR',
    marketing_slug: 'Best Value',
    marketing_url: 'https://www.collrs.com',
    marketing_image: 'https://collrs-assets.s3.ap-south-1.amazonaws.com/images/products/103.png',
    conditions: 'Walk days excludes Sundays',
    upc:[{
        upc_id: '1031',
        upc_description: 'Two Months',
        type: 2,            //1: daily, 2: monthly
        frequency: 1,
        frequency_units: 'walk per day',
        price: 6000.00,
        offer: 'Get 7 days free when you book this package',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 1800.00,
        savings_slug: 'Total estimated savings'
    },{
        upc_id: '1032',
        upc_description: 'Two Months',
        type: 2,            //1: daily, 2: monthly
        frequency: 2,
        frequency_units: 'walks per day',
        price: 10000.00,
        offer: 'Get 7 days free when you book this package',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 5600.00,
        savings_slug: 'Total estimated savings'
    }]
},{
    product_id: '104',
    product_name: 'Titan',
    product_desc: 'Three Months Package',
    product_sub_desc: 'Upto 2 Walks per Day',
    duration: '30',
    duration_unit: 'mins | per session',
    currency: 'INR',
    marketing_slug: 'Most Value',
    marketing_url: 'https://www.collrs.com',
    marketing_image: 'https://collrs-assets.s3.ap-south-1.amazonaws.com/images/products/104.png',
    conditions: 'Walk days excludes Sundays',
    upc:[{
        upc_id: '1041',
        upc_description: 'Three Months',
        type: 2,            //1: daily, 2: monthly
        frequency: 1,
        frequency_units: 'walk per day',
        price: 9000.00,
        offer: 'Get 15 days free when you book this package',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 2700.00,
        savings_slug: 'Total estimated savings'
    },{
        upc_id: '1042',
        upc_description: 'Three Months',
        type: 2,            //1: daily, 2: monthly
        frequency: 2,
        frequency_units: 'walks per day',
        price: 15000.00,
        offer: 'Get 15 days free when you book this package',
        conditions: 'Walk days excludes Sundays',
        estimated_savings: 8400.00,
        savings_slug: 'Total estimated savings'
    }]
}];

const MarketingSlug = ({ title, icon }) => {
    return(
        <View style={styles.marketingSlug}>
            <FontAwesome name={icon} style={{ color: 'white', alignSelf: 'center'}} size={16}/>
            <Text style={{ color: 'white', fontSize: 12, fontFamily: 'sansmedium', alignSelf: 'center', left: 2}}>{title}</Text>
        </View>
    );
}

const ProductCard = ({ product, navigation, checkOffers }) => {
    return (
        <ListItem noBorder noIndent style={styles.listView} onPress={() => navigation.navigate('ProductBooking', {data: product, ProductBookingTitle: product.product_name})} touchableHighlightStyle={{borderRadius: 20}}>
            <View style={styles.productView}>                
                <Grid >
                    <Row size={3} style={styles.marketingHeader}>                     
                        <ImageBackground resizeMode="cover" resizeMethod="auto" source={{uri: product.marketing_image}} style={{ flex: 1, padding: 0, justifyContent: 'flex-end', overflow: 'hidden'}} borderTopLeftRadius={20} borderTopRightRadius={20}>
                        {
                            checkOffers(product.upc)
                        }
                            <LinearGradient locations={[0, 1.0]}  colors= {['rgba(0,0,0,0.00)', 'rgba(0,0,0,0.80)']} 
                                            style={styles.linearGradient}/>
                            <View style={{padding: 10}}>
                                <Text style={styles.productName}>{product.product_name}</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>                                    
                                    <Text style={styles.productDesc}>{product.product_desc}</Text>                                    
                                    {
                                        (product.marketing_slug !== '')&&
                                        <MarketingSlug title={product.marketing_slug} icon="star"/>
                                    }                                    
                                </View>                                
                            </View>                                                                            
                        </ImageBackground>
                    </Row>
                    <Row size={1}>
                        <Col size={2} style={styles.productDetail}>                            
                            <View>
                                <Text style={styles.productBib}>{product.product_sub_desc}</Text>
                                <Text style={styles.productBib}>{product.duration} {product.duration_unit}</Text>
                                <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'flex-start'}}
                                        onPress={() => openWeb(product.marketing_url)}>
                                    <FontAwesome fatype='regular' name={'info-circle'} size={14} style={styles.bookingIcons}/>
                                    <Text style={styles.learnMore}>
                                        Learn More
                                    </Text>
                                </TouchableOpacity>                                                                
                                {/* {
                                    (product.upc.length > 1)?
                                     <Text style={styles.productPrice}>From {currencyFormatter(product.upc[0].price)}</Text>
                                    :<Text style={styles.productPrice}>{currencyFormatter(product.upc[0].price)}</Text>
                                } */}
                            </View>                             
                        </Col>
                        <Col size={1} style={styles.productDetail}>
                            <Row size={5}>
                                
                            </Row>
                            <Row size={1}>
                                <View style={{ flex: 1 }}>
                                    <AwesomeButton 
                                        raiseLevel={0} 
                                        height={30} 
                                        stretch={true}
                                        textFontFamily="sansmedium"       
                                        textSize={16}          
                                        borderRadius={30}  
                                        backgroundColor='#fff'
                                        backgroundActive='rgba(108, 99, 255, 0.4)'
                                        borderColor='#6c63ff'
                                        backgroundShadow='#6c63ff'
                                        borderWidth={1}
                                        paddingHorizontal={14}
                                        // ExtraContent={
                                        //     <ButtonGradient/>
                                        //   }
                                        onPress={() => navigation.navigate('ProductBooking', {data: product, ProductBookingTitle: product.product_name})}
                                    >
                                        <Text style={{color: '#6c63ff', fontFamily: 'sans', fontSize: 14}}>Book Now</Text>
                                        <FontAwesome name={'chevron-right'} size={14} style={{color: '#6c63ff', marginLeft: 5, marginRight: 5}}/>
                                    </AwesomeButton>
                                </View>                                
                            </Row>
                        </Col>    
                    </Row>
                </Grid>
            </View>
        </ListItem>
    )
}

class CustomerBooking extends Component {
    state={
        loading: false,
        products: []
    }

    handleOnScroll = event => {
        console.log(event.nativeEvent.contentOffset.y);
    }

    checkOffers = (upc) => {
        let offCount = 0
        upc.forEach(upc => {
            if(upc.offer){
                offCount = offCount + 1;
            }
        })

        if(offCount > 0){
            return( 
                <CornerLabel
                    cornerRadius={70}
                    alignment={'right'}
                    style={{backgroundColor: '#6c63ff', height: 28}}
                    textStyle={{fontSize: 14, fontFamily: 'sansmedium', color: '#fff', }}>
                    <View style={{flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'stretch'}}>
                        <FontAwesome fatype='solid' name={'stars'} size={16} style={{color: '#fff', right: 2}}/> 
                        <Text style={{fontSize: 14, fontFamily: 'sansmedium', color: '#fff', }}>Offer</Text>
                    </View>                        
                </CornerLabel>)
        }else{
            return <></>;
        }
    }

    openWeb = async (url) =>{
        await WebBrowser.openBrowserAsync(url, {enableBarCollapsing: true });
    }

    componentDidMount(){
        this.setState({ loading: true })
        setTimeout(function() { //Start the timer
            this.setState({ loading: false, products: products }) //After 1 second, set render to true
        }.bind(this), 200)        
    }

     render() {        
        return (        
                <Container showsVerticalScrollIndicator={false}>
                    <Content showsVerticalScrollIndicator={false} >
                    {
                        (this.state.products.length === 0 && this.state.loading)?
                        <Loading loading={this.state.loading}/>
                        :<List style={styles.bodyContainer} >
                        {
                            this.state.products.map((item) => <ProductCard key={item.product_id} product={item} navigation={this.props.navigation} checkOffers={this.checkOffers}/>)
                        }
                        </List>
                    }                    
                    </Content>
                </Container>
        );
    }
}

const styles = StyleSheet.create({      
    statusBar: {
        backgroundColor: "#6c63ff",
        height: Constants.statusBarHeight,
      },
    bodyContainer:{
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10
    },
    listView:{
        width: '100%', 
        marginLeft: 0, 
        paddingLeft: 0, 
        paddingRight: 0, 
        marginRight: 0,
    },
    productView:{
        flex: 1,
        width: Math.round(Dimensions.get('window').width) - 40,
        borderRadius: 20,
        borderColor: '#CFD8DC',        
        backgroundColor: 'white',
        shadowOpacity: 0.55,
        shadowRadius: 3,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 3,
        // overflow: 'hidden'
    },
    marketingHeader:{
        width: '100%',
        height: 150,
        backgroundColor: '#ECEFF1',
        borderRadius: 20
    },
    productDetail:{
        padding: 12,        
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        // backgroundColor: 'yellow'
    },
    productName:{
        fontFamily: 'sansmedium',
        color: '#fff',
        // textAlign: 'left',
        fontSize: 22,
        // flex: 1,
        alignSelf: 'flex-start'
    },
    productDesc:{
        fontFamily: 'sans',
        color: '#fff',
        // textAlign: 'left',
        fontSize: 16,
        // flex: 1,
        alignSelf: 'flex-start'
    },
    marketingSlug:{
        flexDirection: 'row',
        paddingTop: 4, 
        paddingBottom: 4, 
        paddingLeft: 6, 
        paddingRight: 8, 
        borderRadius: 20,
        // borderTopLeftRadius: 20, 
        // borderBottomLeftRadius: 20, 
        backgroundColor: '#FFA000'
    },
    productBib:{
        fontFamily: 'sans',
        color: '#616161',
        textAlign: 'left',
        flex: 1,
        fontSize: 16,
        alignSelf: 'flex-start',
        paddingBottom: 4
    },
    productPrice:{
        fontFamily: 'Roboto_medium',
        textAlign: 'left',
        flex: 1,
        fontSize: 18,
        alignSelf: 'flex-start',
        color: '#37474F',
    },
    linearGradient:{
        position:'absolute',
        flex: 1,
        width:'100%',
        height:'70%'
    },
    bookingIcons:{        
        color: '#6c63ff',
        paddingRight: 3,
        alignSelf: 'center'
    },
    learnMore:{
        color: '#6c63ff',
        fontSize: 14, 
        fontFamily: 'sans' , 
        alignSelf: 'flex-start', 
        // textDecorationLine: 'underline'
    }
});

export default withNavigation(CustomerBooking);