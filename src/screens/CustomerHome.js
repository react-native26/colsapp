import React, { Component } from 'react';
import  {StyleSheet, Platform, Dimensions, View, ImageBackground, TouchableOpacity, TouchableHighlight, Image, RefreshControl, StatusBar } from 'react-native';
import { Container, Content, Button, Right, Thumbnail, Text, List, ListItem, Separator, Badge } from 'native-base';
import FontAwesome from '../components/FontAwesome5';
import { Col, Row, Grid } from "react-native-easy-grid"
import Constants from 'expo-constants';
import * as WebBrowser from 'expo-web-browser';
import { Linking } from 'expo';
import BlinkView from 'src/components/Blinkview';

const bookingData = [{
    booking_id: 'CLLR-112',
    product_id: 'WALK30',
    product_desc: 'Major Duo, Two Months Package',
    start_date: '01-JUN-2020',
    end_date: '',
    walk_times: ['10:00 AM','6:30 PM'],    // array, can contain more than 1 walk times
    completed_walks: 0,
    total_walks: 30,
    pet_name: 'Romeo',
    walk_in_progress: true,
    walker_id: '11234-WLK',     // Walker ID will be a <sequence_number>-WLK eg. 11234-WLK
    walker_name: 'Anurag S.',
    walker_contact: '+919208438890',
    walker_avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0QS0Z1xEs2A_tQn6NLqQY8WNa_uUbqbi7OV7Ty-c5lQ2aXgklSQ&s',
    booking_status: 'A',        //could be (A) active, (C) completed, (B) booked
    booking_date: '15-MAY-2020',
    payment_status: 3,          // (1) - fully paid, (2) - partially paid, (3) - not paid yet
    payment_amount: '15000.00',
    special_instructions: 'Please ensure Romeo is leashed all the time. Also please ensure not to feed him outside food or water.',
    booking_confirmed: true
},
{
    booking_id: 'CLLR-113',
    product_id: 'WALK30',
    product_desc: 'Major Duo, Two Months Package',
    start_date: '01-JUL-2020',
    end_date: '30-AUG-2020',
    walk_times: ['10:00 AM','6:30 PM'],    // array, can contain more than 1 walk times
    completed_walks: 0,
    total_walks: 30,
    pet_name: 'Romeo',
    walk_in_progress: false,
    walker_id: '',     // Walker ID will be a <sequence_number>-WLK eg. 11234-WLK
    walker_name: '',
    walker_contact: '',
    walker_avatar: '',
    booking_status: 'A',        //could be (A) active, (C) completed, (B) booked
    booking_date: '15-MAY-2020',
    payment_status: 3,          // (1) - fully paid, (2) - partially paid, (3) - not paid yet
    payment_amount: '',
    special_instructions: 'Please ensure Romeo is leashed all the time.',
    booking_confirmed: false
},
{
    booking_id: 'CLLR-114',
    product_id: 'WALK30',
    product_desc: 'Major Duo, Two Months Package',
    start_date: '01-SEP-2020',
    end_date: '30-OCT-2020',
    walk_times: ['6:30 PM'],    // array, can contain more than 1 walk times
    completed_walks: 0,
    total_walks: 30,
    pet_name: 'Romeo',
    walk_in_progress: false,
    walker_id: '',     // Walker ID will be a <sequence_number>-WLK eg. 11234-WLK
    walker_name: '',
    walker_contact: '',
    walker_avatar: '',
    booking_status: 'A',        //could be (A) active, (C) completed, (B) booked
    booking_date: '15-MAY-2020',
    payment_status: 3,          // (1) - fully paid, (2) - partially paid, (3) - not paid yet
    payment_amount: '',
    special_instructions: 'Please ensure Romeo is leashed all the time.',
    booking_confirmed: false
}
];

const bookingData1 = [{
    booking_id: 'CLLR-112',
    product_id: 'WALK30',
    product_desc: 'Major Duo, Two Months Package',
    start_date: '01-JUN-2020',
    end_date: '30-JUN-2020',
    walk_times: ['10:00 AM','6:30 PM'],    // array, can contain more than 1 walk times
    completed_walks: 0,
    total_walks: 30,
    pet_name: 'Romeo',
    walk_in_progress: false,
    walker_id: '',     // Walker ID will be a <sequence_number>-WLK eg. 11234-WLK
    walker_name: '',
    walker_contact: '',
    walker_avatar: '',
    booking_status: 'A',        //could be (A) active, (C) completed, (B) booked
    booking_date: '15-MAY-2020',
    payment_status: 3,          // (1) - fully paid, (2) - partially paid, (3) - not paid yet
    payment_amount: '',
    special_instructions: 'Please ensure Romeo is leashed all the time.',
    booking_confirmed: false
},
// {
//     booking_id: 'CLLR-113',
//     product_id: 'WALK30',
//     product_desc: 'Major Duo, Two Months Package',
//     start_date: '01-JUL-2020',
//     end_date: '30-AUG-2020',
//     walk_times: ['6:30 PM'],    // array, can contain more than 1 walk times
//     completed_walks: 0,
//     total_walks: 30,
//     pet_name: 'Romeo',
//     walk_in_progress: false,
//     walker_id: '',     // Walker ID will be a <sequence_number>-WLK eg. 11234-WLK
//     walker_name: '',
//     walker_contact: '',
//     walker_avatar: '',
//     booking_status: 'A',        //could be (A) active, (C) completed, (B) booked
//     booking_date: '15-MAY-2020',
//     payment_status: 3,          // (1) - fully paid, (2) - partially paid, (3) - not paid yet
//     payment_amount: '',
//     special_instructions: 'Please ensure Romeo is leashed all the time.',
//     booking_confirmed: false
// },
// {
//     booking_id: 'CLLR-114',
//     product_id: 'WALK30',
//     product_desc: 'Major Duo, Two Months Package',
//     start_date: '01-SEP-2020',
//     end_date: '30-OCT-2020',
//     walk_times: ['6:30 PM'],    // array, can contain more than 1 walk times
//     completed_walks: 0,
//     total_walks: 30,
//     pet_name: 'Romeo',
//     walk_in_progress: false,
//     walker_id: '',     // Walker ID will be a <sequence_number>-WLK eg. 11234-WLK
//     walker_name: '',
//     walker_contact: '',
//     walker_avatar: '',
//     booking_status: 'A',        //could be (A) active, (C) completed, (B) booked
//     booking_date: '15-MAY-2020',
//     payment_status: 3,          // (1) - fully paid, (2) - partially paid, (3) - not paid yet
//     payment_amount: '',
//     special_instructions: 'Please ensure Romeo is leashed all the time.',
//     booking_confirmed: false
// }
];

const BookingItem = ({ booking, navigation }) =>{
    return(
        <ListItem noIndent noBorder onPress={() => navigation.navigate('CustomerBookingDetails', {data: booking})} touchableHighlightStyle={{borderRadius: 20}}>  
            {/* <TouchableHighlight style={{backgroundColor: 'transparent'}}> */}
                <View style={styles.upcomingItem}>
                    <Text style={styles.upcomingHeader}>{booking.product_desc}</Text>
                    <Text style={styles.upcomingSubHeader}>{booking.start_date}</Text>
                    {
                        (booking.booking_confirmed === true)?
                        <Text style={styles.upcomingSubHeader}>Booking Confirm</Text>
                        :<Text style={styles.upcomingSubHeader}>Pending Confirmation</Text>
                    }
                </View>
                <Right>
                    <FontAwesome name={'chevron-right'} size={18} style={{ color: '#9E9E9E' }}/>
                </Right>
            {/* </TouchableHighlight> */}
        </ListItem>
    )
}



export default class CustomerHome extends Component {
    state = {
        petname: 'Collie',
        petbreed: 'Golden Retriever',
        membersince: '24th March 2019',
        checkingBooking: true,
        currentBooking: true,
        bookingData: '',
        allBookings: []
        // walkerId: '',
        // walkerName: '',
        // walkerContact: '',
        // walkerAvatar: '',

    }

    componentDidMount(){        
        // Hub.listen("auth", ({ payload: { event, data } }) => {
        //     switch (event) {              
        //       case "signOut":              
        //         NavigationService.navigateMainNavigator('Main');
        //         break;
        //     }
        //   });
        this.setState({ checkingBooking: true }, () => this.setState({ bookingData: bookingData1[0], allBookings: bookingData1.slice(1), checkingBooking: false}));  
        // this.setState({ bookingData: bookingData1[0], allBookings: bookingData1})                
    }

    renderContent = () => {
        return (
          <WebView
            source={{ uri: this.state.webURL }}
            // onNavigationStateChange={this.onNavigationStateChange}
            startInLoadingState
            scalesPageToFit
            javaScriptEnabled
            style={{ flex: 1 }}
          />
        );
    }

    openWeb = async (url) =>{
        await WebBrowser.openBrowserAsync(url, {enableBarCollapsing: true });
    }

    refreshBooking = () => {
        this.setState({ bookingData: bookingData[0], allBookings: bookingData.slice(1)})
    }

    render() {        
        return (
                <Container>            
                    <StatusBar barStyle="light-content"/>
                    <Content showsVerticalScrollIndicator={false} 
                        style={{ flex: 1}} 
                        refreshControl={<RefreshControl refreshing={this.state.checkingBooking}
                                        onRefresh={this.refreshBooking}
                                        />}>
                        <List>
                            {
                                (Object.keys(this.state.bookingData).length === 0)&&
                                <ListItem noIndent style={styles.listView} noBorder>
                                    <View style={styles.placeholderContainer}>
                                        <Image source={require('../assets/images/home3.png')} style={{height: 120, width: Dimensions.get('window').width - 190}}/>
                                        <Text style={styles.placeholderText}>You have no active bookings</Text>
                                        <View
                                            style={{
                                                borderBottomColor: '#CFD8DC',
                                                borderBottomWidth: StyleSheet.hairlineWidth,
                                                alignSelf:'stretch'
                                            }}
                                            />
                                        <Button bordered block style={{ borderColor: '#6c63ff', borderWidth: 1, borderColor: 'transparent' }} onPress={() => this.props.navigation.navigate("CustomerBooking")}>
                                            <Text style={{color: '#6c63ff', fontFamily: 'sans', fontSize: 16}}>Book a Dog Walk</Text>
                                        </Button>  
                                    </View>
                                </ListItem>
                            }
                            {
                                (Object.keys(this.state.bookingData).length !== 0)&&
                                <ListItem noIndent style={styles.listView} noBorder>
                                    <View style={styles.placeholderContainer}>
                                        <Grid>
                                            <Col size={3}>
                                                <View style={styles.infocontainer}>
                                                    <View style={{ flexDirection: 'row', paddingBottom: 10}}>
                                                        <Text style={styles.bookingHeaderText}>Dog Walk for </Text>
                                                        <Text style={styles.bookingHeaderText}>{this.state.bookingData.pet_name}</Text>
                                                    </View>     
                                                    <View style={{ flexDirection: 'row', paddingBottom: 10}}>
                                                        <FontAwesome name={'flag-alt'} size={14} style={styles.bookingIcons}/>
                                                        <Text style={styles.bookingText}>{this.state.bookingData.product_desc}</Text>
                                                    </View>                                                                                            
                                                    <View style={{ flexDirection: 'row', paddingBottom: 10}}>
                                                        {/* <Text>Date: </Text> */}
                                                        <FontAwesome name={'calendar-alt'} size={14} style={styles.bookingIcons}/>
                                                        <Text style={styles.bookingText}>Start date: </Text>
                                                        <Text style={styles.bookingText}>{this.state.bookingData.start_date}</Text>                                                                                                                
                                                        {/* {
                                                            (this.state.bookingData.end_date !== '')&&
                                                            <Text style={styles.bookingText}> to {this.state.bookingData.end_date}</Text>
                                                        }                                                         */}
                                                    </View>
                                                    <View style={{ flexDirection: 'row', paddingBottom: 10}}>
                                                        <FontAwesome name={'clock'} size={14} style={styles.bookingIcons}/>
                                                        <Text style={styles.bookingText}>Schedule: </Text>
                                                        {
                                                            this.state.bookingData.walk_times.map((time,idx) => 
                                                                // <Text key={idx} style={styles.bookingText}>
                                                                <Badge key={idx} success style={{marginRight: 5}}>
                                                                    <Text key={idx} style={styles.bookingTextLight}>{time}</Text>
                                                                </Badge>)
                                                                
                                                        }
                                                    </View>                                                
                                                </View>
                                            </Col>
                                            <Col size={1}>
                                                <View style={styles.headercontainer}>
                                                    <View style={styles.avatarStyle}>
                                                        <Thumbnail large source={(this.state.bookingData.walker_id && this.state.bookingData.walker_avatar)? {uri: this.state.bookingData.walker_avatar}:require('../assets/images/walker.png')} style={styles.avatarImg}/>                                
                                                    </View>
                                                    {
                                                        (this.state.bookingData.walker_id)?
                                                        <React.Fragment>
                                                            <Text style={styles.bookingText}>{this.state.bookingData.walker_name}</Text>
                                                            <Text style={styles.subHeaderText}>Walker</Text>
                                                        </React.Fragment>
                                                        :<React.Fragment>
                                                            <Text style={styles.subHeaderText}>Pending walker assignment</Text>
                                                        </React.Fragment>
                                                    }
                                                    
                                                </View>
                                            </Col>
                                        </Grid>
                                        {
                                            (this.state.bookingData.walk_in_progress)&&
                                            <View style={{ flexDirection: 'row', paddingBottom: 10}}>                                                
                                                <BlinkView delay={2000} style={{ alignSelf: 'center'}}>
                                                    <FontAwesome name={'bullseye'} size={16} style={styles.progressIcons}/>
                                                </BlinkView>                                                
                                                <Text style={styles.progressText}>Walk in Progress...</Text>                                                                                                                              
                                            </View>
                                        }
                                        <View
                                            style={{
                                                borderBottomColor: '#CFD8DC',
                                                borderBottomWidth: StyleSheet.hairlineWidth,
                                                alignSelf:'stretch'
                                            }}
                                            />
                                        <Grid>
                                            {
                                                (this.state.bookingData.walker_id !== '') &&
                                                <React.Fragment>
                                                    <Col >
                                                        <TouchableOpacity style={styles.actionButtons} 
                                                                         onPress={() => Linking.openURL(`tel:${this.state.bookingData.walker_contact}`)}>
                                                            <FontAwesome name={'phone-alt'} size={18} style={{color: '#4CAF50', marginRight: 10, alignSelf: 'center'}}/>
                                                            <Text style={{color: '#4CAF50', fontFamily: 'sansmedium', fontSize: 14}}>Contact Walker</Text>                                                            
                                                        </TouchableOpacity>
                                                    </Col>
                                                    <View
                                                        style={{
                                                            borderRightColor: '#CFD8DC',
                                                            borderRightWidth: StyleSheet.hairlineWidth
                                                        }}
                                                    />
                                                </React.Fragment>
                                            }                                        
                                            <Col>
                                                <TouchableOpacity style={styles.actionButtons} 
                                                    onPress={() => this.props.navigation.navigate('CustomerBookingDetails', {data: this.state.bookingData})}>
                                                    <Text style={{color: '#6c63ff', fontFamily: 'sansmedium', fontSize: 14}}>Walk details</Text>
                                                    <FontAwesome name={'chevron-right'} size={18} style={{color: '#6c63ff', marginLeft: 10, alignSelf: 'center'}}/>
                                                </TouchableOpacity>
                                            </Col>
                                        </Grid>                                    
                                        {
                                            (this.state.bookingData.walk_in_progress && this.state.bookingData.walker_id !== '')&&
                                            <React.Fragment>
                                                <View
                                                    style={{
                                                        borderBottomColor: '#CFD8DC',
                                                        borderBottomWidth: StyleSheet.hairlineWidth,
                                                        alignSelf:'stretch'
                                                    }}
                                                    />

                                                <TouchableOpacity style={[styles.actionButtons, {backgroundColor: '#6c63ff', borderBottomRightRadius: 20, borderBottomLeftRadius: 20}]}>
                                                    <FontAwesome name={'flag-checkered'} size={18} style={{color: '#fff', marginRight: 10, alignSelf: 'center'}}/>
                                                    <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 14}}>Walk Complete</Text>                                                    
                                                </TouchableOpacity>
                                            </React.Fragment>
                                        }
                                    </View>
                                </ListItem>
                            }
                            {
                                (this.state.allBookings.length > 1)&&
                                <React.Fragment>
                                    <Separator style={styles.separatorStyle}>
                                        <View style={{flexDirection: 'row'}}>
                                            <FontAwesome name={'sync'} size={20} style={styles.profileIcons}/>
                                            <Text style={styles.blockHeader}>Upcoming</Text>                            
                                        </View>
                                        <Text style={styles.blockSubHeader}>Your upcoming bookings</Text>
                                    </Separator>  
                                    <View style={styles.upcomingBlock}>
                                    {
                                        this.state.allBookings.map(booking => <BookingItem key={booking.booking_id} booking={booking} navigation={this.props.navigation}/>)
                                    }
                                    </View>
                                </React.Fragment>
                            }
                            <ListItem noIndent style={styles.listView} noBorder>
                                <View>                                                
                                    <TouchableOpacity onPress={() => this.openWeb('https://www.collrs.com/pet-parent-faqs')} activeOpacity={0.8}>
                                        <ImageBackground source={require('../assets/images/home1.png')} style={styles.imageContainer} borderRadius={20} >
                                            {/* <View style={{backgroundColor: 'black', opacity: 0.5, flex: 1}} /> */}                                        
                                            <Text style={{color: 'white', fontFamily: 'sans', fontSize: 18}}>Frequently Asked Questions</Text>
                                            <Text style={{color: 'white', fontFamily: 'sans', fontSize: 12}}>Answers to your common questions about Collrs</Text>   
                                        </ImageBackground>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.openWeb('https://www.collrs.com/blog')} activeOpacity={0.8}>
                                        <ImageBackground source={require('../assets/images/home2.png')} style={styles.imageContainer} borderRadius={20}>
                                            <Text style={{color: 'white', fontFamily: 'sans', fontSize: 18}}>Read the Collrs Blog</Text>
                                            <Text style={{color: 'white', fontFamily: 'sans', fontSize: 12}}>Curated blog posts by our in-house experts</Text> 
                                        </ImageBackground>     
                                    </TouchableOpacity>                                                                                                                                                             
                                </View>
                            </ListItem>
                        </List>
                    </Content>
                </Container>
        );
  }
}

const styles = StyleSheet.create({  
    headercontainer: {        
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarStyle:{
        width: 70,
        height: 70,
        borderRadius: 140/2,
        backgroundColor: 'white',
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },        
    avatarImg:{
        width: 60,
        height: 60,
        borderRadius: 100/2,
        resizeMode: "contain"
    },
    listView:{
        width: '100%', 
        marginLeft: 0, 
        paddingLeft: 0, 
        paddingRight: 0, 
        marginRight: 0,
    },
    headerText:{
        fontSize: 22,
        fontFamily: 'sansmedium',
        color: '#616161'
    },
    subHeaderText:{
        fontSize: 14,
        fontFamily: 'sans',
        color: '#616161',
        textAlign: 'center'
    },
    placeholderText:{
        fontFamily: 'sanslight',
        fontSize: 18,
        color: '#B0BEC5',
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 20
    },
    placeholderContainer:{
        marginLeft: 10,
        marginRight: 10,
        width: Math.round(Dimensions.get('window').width) - 20,
        borderRadius: 20,
        borderColor: '#CFD8DC',
        // borderWidth: StyleSheet.hairlineWidth,
        backgroundColor: 'white',
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
        // flex: 0.7,
        paddingBottom:0,
        paddingTop:10,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    statusBar: {
        backgroundColor: "#6c63ff",
        height: Constants.statusBarHeight,
      },
    imageContainer:{
        margin: 10,
        width: Math.round(Dimensions.get('window').width) - 20,
        height: 100,
        borderRadius: 20,
        backgroundColor: 'white',
        shadowOpacity: 0.8,
        shadowRadius: 7,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
        justifyContent: 'center',
        alignItems: 'center',
        // overflow: 'hidden'
    },
    infocontainer:{
        // margin: 10,
        padding: 10,
        flex: 1,        
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    bookingText:{
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 14,
        alignSelf: 'center'
    },
    bookingTextLight:{
        fontFamily: 'sans',
        color: '#fff',
        fontSize: 14,
        alignSelf: 'center'
    },
    bookingHeaderText:{
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 16,
        alignSelf: 'center'
    },
    bookingIcons:{        
        color: '#616161',
        paddingRight: 5,
        alignSelf: 'center'
    },
    progressText:{
        fontFamily: 'sans',
        color: '#EF6C00',
        fontSize: 14,
        alignSelf: 'center'
    },
    progressIcons:{        
        color: '#EF6C00',
        paddingRight: 5,
        alignSelf: 'center'
    },
    upcomingBlock:{
        marginLeft: 10, 
        marginRight: 10, 
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
    },
    blockHeader:{ 
        fontFamily: 'sansmedium', 
        fontSize: 24, 
        color: '#37474F'
    },
    upcomingItem:{
        alignItems: 'flex-start', 
        justifyContent: 'flex-start', 
        flex: 1,
    },
    upcomingHeader: {
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 16,
        alignSelf: 'flex-start'
    },
    upcomingSubHeader: {
        fontFamily: 'sans',
        color: '#9E9E9E',
        fontSize: 12,
        alignSelf: 'flex-start'
    },
    separatorStyle:{
        height: 70, 
        backgroundColor: '#fff'
    },        
    profileIcons:{        
        color: '#37474F',
        paddingRight: 5,
        alignSelf: 'center'
    },
    actionButtons:{
        flex: 1, 
        flexDirection: 'row', 
        alignContent:'center', 
        justifyContent: 'center', 
        width: '100%',
        height: 50
    }
});