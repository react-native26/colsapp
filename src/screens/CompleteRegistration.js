import React, { Component } from 'react'
import { Auth } from 'aws-amplify';
import  {StyleSheet, View, Text, Image, Dimensions, Platform, StatusBar } from 'react-native';
import AwesomeButton from "react-native-really-awesome-button";
import Constants from 'expo-constants';
import { Header } from 'react-navigation';
import ButtonGradient from 'src/components/ButtonGradient';

class CompleteRegistration extends Component {
    // componentDidMount(){
    //     this.signOut();
    // }

    render() {      
        const imgHeight = (Dimensions.get('window').height < 800)? 300:400;
        return (
                // <ImageBackground source={require('../assets/images/startedBG.png')} style={{width: '100%', height: '100%'}}>
                // <View style={{width: '100%', height: '100%'}}>    
                <>
                    <StatusBar barStyle="dark-content" />           
                    <View style={styles.container}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center'}} >
                            <Text style={styles.heaederText}>Lets Get Started</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center'}} >
                            <Text style={styles.subText}>
                                Hi there 👋 thanks for joining Collrs and a network of loving pet parents and passionate dog walkers. 
                                We are excited to have you onboard. Please select an option to continue.
                            </Text>
                        </View>                        
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10}} >
                        <Image source={require('../assets/images/started.png')} style={{height: imgHeight, width: Dimensions.get('window').width}}/>
                    </View>
                        
                    
                    <View style={styles.btncontainer}>                    
                        <AwesomeButton 
                            raiseLevel={0} 
                            height={50} 
                            borderRadius={55}  
                            stretch={true}
                            textFontFamily="sansmedium"       
                            textSize={16}                      
                            ExtraContent={
                                <ButtonGradient/>
                              }
                            onPress={next => this.props.navigation.navigate('CreatePetProfile')}
                            >I am a Customer
                        </AwesomeButton>
                        <AwesomeButton 
                            raiseLevel={0} 
                            height={50} 
                            borderRadius={55}  
                            stretch={true}
                            textFontFamily="sansmedium" 
                            textSize={16} 
                            style={styles.btn2} 
                            ExtraContent={
                                <ButtonGradient/>
                              }
                             //onPress={next => this.props.navigation.navigate('WalkerApply')}
                             onPress={next => this.props.navigation.navigate('WalkerContainer')}

                              >I am a Walker
                        </AwesomeButton>
                    </View>
                </>
        )
    }
}

const styles = StyleSheet.create({
    container: {    
        marginTop: Constants.statusBarHeight + Header.HEIGHT
    },
    heaederText:{        
        fontSize: 21,   
        fontFamily: 'sansmedium',
        color: '#37474f'
    },
    subText:{
        fontSize: 16,
        marginLeft: 60, 
        marginRight: 60,   
        textAlign: 'center',            
        // top: 20,
        fontFamily: 'sans',
        color: '#37474f'
    },
    btn:{
        top:20
    },
    btncontainer: {
        flex: 1,
        marginLeft: 30,
        marginRight: 30,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: (Platform.OS === 'ios')? 80 : 90                
    },
    btn2:{
        top: 20
    }
  });

export default CompleteRegistration;