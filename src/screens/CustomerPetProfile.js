import React, { Component } from 'react';
import  {StyleSheet, StatusBar, View, Keyboard, TextInput, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Content, Text, List, ListItem, Separator, Thumbnail } from 'native-base';
import { Col, Grid, Row } from "react-native-easy-grid";
import { LinearGradient } from 'expo-linear-gradient';
import { format } from "date-fns";
import FontAwesome from 'src/components/FontAwesome5';
import Placeholder from 'src/components/PlaceHolder';

const userData = {
    pet_name: "Rollie",
    breed: "Rottweiler",
    age: 3.5,
    weight: 9.2,
    gender: 'M',
    house_trained: 'Y',
    friendly_dog: 'N',
    about_pet: 'Rollie is a good dog but he can get crranky some times. He has brown fur and black eyes and is usually playful.',
    care_info: 'Please do not feed Rollie outside water or food. He is allergic to gluten as well.',
    vet_info: '',
    profile_image: '',
    member_since: '13-DEC-2019',
    contact_info:{
        name: 'John Doe',
        email: 'john@email.com',
        phone: '+919208438890',
        address: '442 Blg K, Sector 24, Near Ashok Vihar',
        city: 'Noida',
        pin: '100101'
    }

};


const Label = ({ title }) => {
    return (<Row size={1} style={{ left: 4}}>
                <Text style={styles.labelText}>
                    {title}
                </Text>
            </Row>);
};

const Value = ({ title }) => {
    return (<Row size={2}>
                <Text style={styles.valueText}>
                    {title}
                </Text>
            </Row>);
};

const ItemWithVal = (props) => {
    const { label, value, icon } = props;
    return (<ListItem {...props} style={styles.formLine}>
                <Grid>
                    <Col size={2} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        {
                            (icon)&& <FontAwesome name={icon} size={14}/> 
                        }                                                       
                        <Label title={label}/>                         
                    </Col>
                    <Col size={1} style={{paddingRight: 5}}>                                                                
                        <Value title={value} />
                    </Col>
                </Grid>
            </ListItem>);
}

const NoInfo = () => {
    return (
        <View style={{ marginTop: 20, marginBottom: 20}}>
            <FontAwesome name={'question-square'} size={26} style={[styles.profileIcons, {color: '#E0E0E0'}]}/>
            <Text style={[styles.valueText, {color: '#E0E0E0'}]}>
                No information provided
            </Text>
        </View>
    );
}

class CustomerPetProfile extends Component {
    constructor(props){
        super(props);
        this.inputRefs = {
            favSport5: null
          };

        // console.log("Date is *******",format(Date.parse(userData.member_since), 'LLL d, yyyy'))
        
        this.state = {
                image: null,
                userData: null,
                loading: true
            };
    }

    componentDidMount(){
        this.setState({image: userData.profile_image, userData, loading: false });
    }

    render() {
        return (
            <Container showsVerticalScrollIndicator={false}>
                <StatusBar barStyle="light-content"  />   
                <Content showsVerticalScrollIndicator={false}>
                    <List style={styles.bodyContainer} >
                        <ListItem noIndent style={{ marginLeft: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0, paddingTop: 0}}> 
                            <ImageBackground style={styles.headercontainer} source={(this.state.image)?{uri:this.state.image}: undefined}>
                                {/* <BlurView tint="light" intensity={90} style={styles.blurred}/>   */}
                                <LinearGradient locations={[1.0, 0]}  colors= {['rgba(0,0,0,0.50)', 'rgba(0,0,0,0.50)']} start={[1.0, 0.0]} start={[1.0, 0.0]}
                                                    style={styles.linearGradient}/>
                                <View style={{ alignContent: 'center', justifyContent: 'center', marginTop: 40, marginBottom: 40}}>
                                    <View style={styles.avatarStyle}>
                                        <Thumbnail large source={(this.state.image)?{uri:this.state.image}: require('../assets/images/paw.png')} style={styles.avatarImg}/>                              
                                    </View>
                                </View>
                            </ImageBackground> 
                        </ListItem>
                        {
                            (this.state.loading === true)?
                             <Placeholder/>
                            :<>                            
                            <ListItem noIndent noBorder>
                                <Grid>
                                    <Col>
                                        <Text style={styles.petName}>{this.state.userData.pet_name}</Text>
                                        <Text style={styles.petSpecifics}>{this.state.userData.breed}</Text>
                                        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
                                            <Text style={styles.petSpecifics}>{(this.state.userData.gender === 'M')? 'Male, ':'Female, '}</Text>
                                            <Text style={styles.petSpecifics}>{this.state.userData.age} yrs</Text>
                                        </View>
                                        {
                                            (this.state.userData.member_since !== '' || this.state.userData.member_since !== null)&&
                                            <Text style={styles.petSpecifics}>Member since {format(Date.parse(userData.member_since), 'LLL d, yyyy')}</Text>
                                        }
                                    </Col>
                                </Grid>
                                
                            </ListItem>
                            <Separator style={{height: 20}}/>
                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    <FontAwesome name={'paw'} size={26} style={styles.profileIcons}/>
                                    <Text style={styles.blockHeader}>Pet Info</Text>                            
                                </View>
                                {/* <Text style={styles.blockSubHeader}>Basic information about your pet</Text> */}
                            </Separator>
                            <View style={styles.formBlock}>
                                <ItemWithVal noIndent label={"Weight"} value={`${this.state.userData.weight} kg`}/>
                                <ItemWithVal noIndent label="House trained?" value={(this.state.userData.house_trained === 'Y')? 'Yes': 'No'}/>
                                <ItemWithVal noIndent label="Friendly with other dogs?" value={(this.state.userData.friendly_dog === 'Y')? 'Yes':'No'}/>
                                <ListItem noIndent noBorder>
                                    <Grid>
                                        <Col>
                                            <Text style={[styles.labelText, {textAlign: 'left', alignSelf: 'flex-start'}]}>
                                                About {this.state.userData.pet_name}
                                            </Text>
                                            {
                                                (this.state.userData.about_pet !== '')?
                                                <Text style={[styles.valueText, {textAlign: 'left', alignSelf: 'flex-start', paddingBottom: 20}]}>
                                                    {this.state.userData.about_pet}
                                                </Text>
                                                :<NoInfo/>
                                            }
                                            {/* <Text style={[styles.valueText, {textAlign: 'left', alignSelf: 'flex-start'}]}>
                                                {this.state.userData.about_pet}
                                            </Text> */}
                                        </Col>
                                    </Grid>                                    
                                </ListItem>                                
                            </View>
                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    <FontAwesome name={'heart-circle'} size={26} style={styles.profileIcons}/>
                                    <Text style={styles.blockHeader}>Care info</Text>                            
                                </View>
                                {/* <Text style={styles.blockSubHeader}>Provide care information</Text> */}
                            </Separator>
                            <View style={styles.formBlock}>
                                <ListItem noBorder noIndent>
                                    {
                                        (this.state.userData.care_info !== '')?
                                        <Text style={[styles.valueText, {textAlign: 'left', alignSelf: 'flex-start', paddingBottom: 40}]}>
                                            {this.state.userData.care_info}
                                        </Text>
                                        :<Grid>
                                            <Col>
                                                <NoInfo/>
                                            </Col>
                                        </Grid>
                                    }
                                </ListItem>                                
                            </View>

                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    <FontAwesome name={'clipboard-prescription'} size={26} style={styles.profileIcons}/>
                                    <Text style={styles.blockHeader}>Veterinary info</Text>                            
                                </View>
                                {/* <Text style={styles.blockSubHeader}>Vet's info, medications, pre-existing conditions</Text> */}
                            </Separator>
                            <View style={styles.formBlock}>
                                <ListItem noBorder noIndent>
                                    {
                                        (this.state.userData.vet_info !== '')?
                                        <Text style={[styles.valueText, {textAlign: 'left', alignSelf: 'flex-start', paddingBottom: 40}]}>
                                            {this.state.userData.vet_info}
                                        </Text>
                                        :<Grid>
                                            <Col>
                                                <NoInfo/>
                                            </Col>
                                        </Grid>
                                    }
                                </ListItem>                                
                            </View>

                            <Separator style={styles.separatorStyle}>
                                <View style={{flexDirection: 'row'}}>
                                    <FontAwesome name={'address-book'} size={26} style={styles.profileIcons}/>
                                    <Text style={styles.blockHeader}>Contact info</Text>                            
                                </View>
                                {/* <Text style={styles.blockSubHeader}>Your name, address, email, & phone</Text> */}
                            </Separator>  
                            <View style={styles.formBlock}>
                                <ItemWithVal noIndent label={"Name"} value={this.state.userData.contact_info.name}/>
                                <ItemWithVal noIndent label="Email" value={this.state.userData.contact_info.name}/>
                                <ItemWithVal noIndent label="Phone (+91)" value={this.state.userData.contact_info.phone}/>
                                <ListItem noIndent>
                                    <Grid>
                                        <Col>
                                            <Text style={[styles.labelText, {textAlign: 'left', alignSelf: 'flex-start'}]}>
                                                Address
                                            </Text>
                                            <Text style={[styles.valueText, {textAlign: 'left', alignSelf: 'flex-start', paddingBottom: 20}]}>
                                                {this.state.userData.contact_info.address}
                                            </Text>
                                        </Col>
                                    </Grid>                                    
                                </ListItem> 
                                <ItemWithVal noIndent label="City" value={this.state.userData.contact_info.city}/>
                                <ItemWithVal noIndent noBorder label="Pin" value={this.state.userData.contact_info.pin}/>
                            </View>
                            </>
                        }
                        
                    </List>          
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({ 
    bodyContainer:{
        width: '100%',        
        backgroundColor: 'white'
    },
    headercontainer: {        
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center' , 
        backgroundColor: '#41484e'
    },
    avatarImg:{
        width: 100,
        height: 100,
        borderRadius: 240/2,
        resizeMode: "contain",
        alignSelf: 'center'
    },
    avatarStyle:{
        width: 110,
        height: 110,
        borderRadius: 260/2,
        backgroundColor: 'white',
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 5,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    linearGradient:{
        position:'absolute',
        flex: 1,
        width:'100%',
        height:'100%'
    },        
    profileIcons:{        
        color: '#37474F',
        paddingRight: 5,
        alignSelf: 'center'
    },
    formLine:{
        height: 60
    },
    largeFormLine:{
        paddingBottom: 40
    },
    labelText:{
        fontFamily: 'sans',
        color: '#616161',
        fontSize: 14,
        textAlign: 'left',
        paddingTop: 5,
        paddingBottom: 5
    },
    valueText:{
        fontFamily: 'sans',        
        fontSize: 14,
        textAlign: 'right',
        flex: 1
    },
    formBlock:{ 
        marginLeft: 15, 
        marginRight: 15, 
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 5,
    },
    blockHeader:{ 
        fontFamily: 'sansmedium', 
        fontSize: 24, 
        color: '#37474F'
    },
    blockSubHeader:{ 
        fontFamily: 'sans', 
        fontSize: 12
    },
    separatorStyle:{
        height: 70, 
        backgroundColor: '#fff'
    },
    petName:{
        fontFamily: 'sansmedium',
        fontSize: 22,
        color: '#37474F',
    },
    petSpecifics:{
        fontFamily: 'sans',
        fontSize: 16,
        color: '#616161'
    }
});

export default CustomerPetProfile;