import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Dimensions, TouchableOpacity, ActivityIndicator, Alert } from 'react-native';
import { Container, Content, List, ListItem, Text } from 'native-base';
import { Col, Grid } from "react-native-easy-grid";
import { WebView } from 'react-native-webview';
import { Auth } from 'aws-amplify';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setWalkerApplicationData } from 'src/redux/reduxReducer';
import * as WebBrowser from 'expo-web-browser';
import Loading from 'src/components/ActivityHud';
import Modal from 'react-native-modal';
import Constants from 'expo-constants';
import AwesomeButton from "react-native-really-awesome-button";
import ButtonGradient from 'src/components/ButtonGradient';
import FontAwesome from 'src/components/FontAwesome5';
import API from 'src/services/ServiceProvider';

//RESPONSE : https://docs.google.com/forms/u/0/d/e/1FAIpQLSes08rgE97qxvNEERAxOIb6zmlZ5HMBmhRFxdfApLnjhV2XLw/formResponse

class WalkerApplication extends Component {

    constructor(props){        
        super(props);
        this.webview = null;
        
        this.state={
            applicationComplete: this.props.applicationData.walkerapplied || false,
            showGoogleForm: false,
            checkingApplication: false,            
            email: '',
            fName: '',
            lName: '',
            userName: '',
            formURL: '',
            formResponsePattern: '',
            applicationStatus: this.props.navigation.getParam('status') || ''
        }
    }

    componentDidMount(){
        this.getUser();
    }

    openWeb = async (url) =>{
        await WebBrowser.openBrowserAsync(url, {enableBarCollapsing: true });
    }

    signOut = () => {        
        Auth.signOut()
        .then(data => this.props.navigation.navigate('Splash'))
        .catch(err => this._showAlert('Error', 'Unable to logout'));
    }

    formShow = () => this.setState({ showGoogleForm: true });
    closeForm = () => {
        this.setState({ showGoogleForm: false })
        setTimeout(() => this.setState({checkingApplication: false }), 2000);
    };    

    _showAlert = (header, error) => {
        Alert.alert(
            header,
            error,
          [
            { text: 'OK', onPress: () => this.setState({ checkingApplication: false }) },
          ],
          { cancelable: false }
        )
    }

    getUser = async() => {
        try{
            const user = await Auth.currentAuthenticatedUser();            
            const email = user.signInUserSession.idToken.payload['email'];
            const fName = user.signInUserSession.idToken.payload['custom:first_name'];  
            const lName = user.signInUserSession.idToken.payload['custom:last_name'];  
            const userName = user.signInUserSession.idToken.payload['cognito:username']; 
            this.setState({ email, fName, lName, userName });
        }catch(err){
            console.log(err);            
        }
    }

    getFormURL = async(next) => {        
        if(this.state.formURL) {
            this.setState({showGoogleForm: true}, () => {
                next();
            });
            return;
        }

        try{
            const payloadData = {
                email: this.state.email,
                fName: this.state.fName,
                lName: this.state.lName,
                action: 'GET_FORM_URL'
            };
            const apiResponse = await API({
                                            lambdaName: 'collrs-walker-application', 
                                            data: payloadData
                                        });

            const { url, responsePattern } = apiResponse.body;
            this.setState({ formURL: url, formResponsePattern: responsePattern, showGoogleForm: true }, () => {
                next();
            });
        }catch(err){            
            next();
            this._showAlert('Error', "Unable to process request at this time. Please try again later");
        }
    }

    handleWebViewNavigationStateChange = async(newNavState) => {
        // newNavState looks something like this:
        // {
        //   url?: string;
        //   title?: string;
        //   loading?: boolean;
        //   canGoBack?: boolean;
        //   canGoForward?: boolean;
        // }

        const { url } = newNavState;
        if (!url) return;
    
        // handle certain doctypes
        if (url.includes(this.state.formResponsePattern)) {
          this.webview.stopLoading();          
          this.setState({ showGoogleForm: false })
          //call API to check application complete and set applicationComplete to true
          try{
            const payloadData = {
                email: this.state.email,
                action: 'CHECK_APPLICATION',
                cognitoUserName: this.state.userName
            };
            const apiResponse = await API({
                                            lambdaName: 'collrs-walker-application', 
                                            data: payloadData
                                        });
            const { applied } = apiResponse.body;
            this.setState({ applicationComplete: applied, checkingApplication: false });
            if(applied) {
                this.props.navigation.setParams({ walkerapplied: true })
                this.props.setWalkerApplicationData({ walkerapplied: true, status: status });
            }; // removes back button since application is in progress
          }catch(err){
            this.setState({ checkingApplication: false });
          }
        }
        // else{
        //     this.webview.stopLoading();
        //     this.setState({ showGoogleForm: false });
        // }
      }

    render() {
        //The JS below will prevent zoom out of the google form in Webview in ios devices
        const fitscreen = `const meta = document.createElement('meta'); 
                    meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'); 
                    meta.setAttribute('name', 'viewport'); 
                    document.getElementsByTagName('head')[0].appendChild(meta); 
                    true;
                    `;

        if(this.state.applicationStatus === 'PENDING_REVIEW'){
            return <View/>;
        }

        return (
            <>
                <Container style={styles.attribution}>
                    <StatusBar barStyle="light-content"  />  
                    <Loading loading={this.state.checkingApplication}/> 
                    <Content showsVerticalScrollIndicator={false} style={{ backgroundColor: '#fff'}} contentContainerStyle={{paddingBottom: 80}} alwaysBounceVertical={false}>
                        <List style={styles.bodyContainer} >
                            <ListItem noIndent style={styles.listView}>                            
                                <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>  
                                    <FontAwesome name={'rocket-launch'} size={36} style={{color: '#37474F', alignSelf: 'center', marginBottom: 20, marginTop: 20 }}/>                                                                      
                                    <Text style={styles.stepMainText}>Lets get started with the application</Text>
                                    <Text style={styles.stepSubText}>Filling out the application questionnaire shouldn't take more than 3-5 mins of your time. We'll make it quick, we promise!</Text>
                                    {
                                        (this.state.applicationComplete === true)?
                                        <View style={{ justifyContent: 'center', alignContent: 'center', marginTop: 20, marginBottom: 20}}>
                                            <FontAwesome name={'check-circle'} fatype='solid' size={30} style={{color: '#00E676', marginRight: 5, alignSelf: 'center', marginBottom: 5}}/>
                                            <Text style={{fontFamily: 'sans'}}>Questionnaire Complete</Text>
                                        </View>
                                        :<AwesomeButton 
                                            raiseLevel={0} 
                                            height={50} 
                                            progress
                                            stretch={true}
                                            textFontFamily="sansmedium"       
                                            textSize={16}          
                                            borderRadius={50}                                                        
                                            ExtraContent={
                                                <ButtonGradient/>
                                                }
                                            style={{marginTop: 20, marginBottom: 20}}
                                            onPress={this.getFormURL}
                                        >
                                                <FontAwesome name={'file-alt'} size={18} style={{color: '#fff', marginLeft: 5, marginRight: 5}}/>
                                                <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 16}}>Start Application</Text>
                                        </AwesomeButton> 
                                    }
                                    
                                </View>
                            </ListItem>
                            <ListItem noIndent style={styles.listView}>                            
                                <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>  
                                    <FontAwesome name={'fingerprint'} size={36} style={{color: '#37474F', alignSelf: 'center', marginBottom: 20, marginTop: 20 }}/>                                                                      
                                    <Text style={styles.stepMainText}>Add an ID Proof</Text>
                                    {
                                        (this.state.applicationComplete)?
                                        <Text style={styles.stepSubText}>Tap the button below to add a valid government issued identification document. Acceptable documents include PAN card, valid driver's license, valid passport, Aadhar card, or Voter ID card </Text>
                                        :<Text style={styles.stepSubText}>Once you've completed the application questionnaire, you will be able to add an ID proof.</Text>
                                    }
                                    {
                                        (this.state.applicationComplete)?
                                        <>
                                            <AwesomeButton 
                                                raiseLevel={0} 
                                                height={50} 
                                                stretch={true}
                                                textFontFamily="sansmedium"       
                                                textSize={16}          
                                                borderRadius={50}                                                        
                                                ExtraContent={
                                                    <ButtonGradient/>
                                                    }
                                                style={{marginTop: 20, marginBottom: 20}}
                                                //onPress={this.formShow}
                                                onPress={() =>  this.props.navigation.navigate('WalkerContainer')}
                                            >
                                                <FontAwesome name={'id-card-alt'} size={18} style={{color: '#fff', marginLeft: 5, marginRight: 5}}/>
                                                <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 16}}>Upload ID document</Text>
                                            </AwesomeButton> 
                                            <Text style={styles.stepSubText}>NOTE: applications without ID Proof will not be considered</Text>
                                        </>
                                        :<AwesomeButton 
                                            raiseLevel={0} 
                                            height={50} 
                                            stretch={true}
                                            textFontFamily="sansmedium"       
                                            textSize={16}          
                                            borderRadius={50} 
                                            disabled={true}
                                            style={{marginTop: 20, marginBottom: 20}}                                            
                                        >
                                            <FontAwesome name={'id-card-alt'} size={18} style={{color: '#fff', marginLeft: 5, marginRight: 5}}/>
                                            <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 16}}>Upload ID document</Text>
                                        </AwesomeButton> 
                                    }
                                    
                                </View>
                            </ListItem>
                            <ListItem noBorder noIndent>
                            <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>
                                <Text style={{  marginTop: 20, fontSize: 12, alignSelf: 'center', textAlign: 'center', color: '#B0BEC5'}}>
                                    By applying to Collrs you confirm that you are 18 years of age or older and have read, understood, & agree to Collrs <Text style={{fontSize: 12, color: '#B0BEC5', textDecorationLine: 'underline'}} onPress={ () => this.openWeb('https://www.collrs.com/walker-tos') }>Terms of Service</Text> &
                                    <Text style={{fontSize: 12, color: '#B0BEC5', textDecorationLine: 'underline', alignSelf: 'center', textAlign: 'center'}} onPress={ () => this.openWeb('https://www.collrs.com/privacy-policy') }> Privacy Policy</Text>
                                </Text>
                                

                                <Text style={{fontSize: 10, color: '#B0BEC5', alignSelf: 'center', textAlign: 'center', fontFamily: 'sans', marginTop: 20}}>
                                    * Approval depends on our review of your application and identity verification. Applications may be reviewed on a case-by-case basis.
                                </Text>
                                {
                                    (this.state.applicationComplete === true)&&
                                    <Text style={{fontSize: 12, color: '#B0BEC5', alignSelf: 'center', textAlign: 'center', fontFamily: 'sans', marginTop: 20, textDecorationLine: 'underline'}}
                                        onPress={this.signOut}
                                    >
                                        Logout
                                    </Text>
                                }                                
                            </View>
                        </ListItem> 
                        </List>
                    </Content>
                    
                    <Modal              
                        isVisible={this.state.showGoogleForm}
                        onBackdropPress={this.closeForm}
                        animationIn='slideInUp'
                        animationOut='slideOutDown'
                        hardwareAccelerated={true}
                        propagateSwipe={true}
                        style={{justifyContent: 'flex-end', margin: 0}}
                        onDismiss={() => this.setState({checkingApplication: true })}
                        > 
                        <View style={styles.modalHeader}>
                            <StatusBar barStyle="light-content"  />  
                            <ListItem noBorder noBorder style={styles.headerOptionStyle}>
                                <Grid style={{width: '100%'}}>
                                    <Col size={1}>
                                        <Text></Text>
                                    </Col>
                                    <Col size={1}>                                        
                                        <Text style={styles.headerStyle}>Application</Text>
                                    </Col>
                                    <Col size={1}>
                                        <TouchableOpacity hitSlop={{ top: 15, bottom: 15, left: 15, right: 15}} onPress={this.closeForm}>
                                            <FontAwesome name={'times'} size={24} style={{ color: '#fff', alignSelf: 'flex-end'}}/>
                                        </TouchableOpacity>
                                    </Col>
                                </Grid>
                            </ListItem>
                            <View style={{flex: 1}}>
                                <WebView 
                                    ref={ref => (this.webview = ref)}
                                    source={{ uri: this.state.formURL }} 
                                    onNavigationStateChange={this.handleWebViewNavigationStateChange}
                                    textZoom={100}
                                    style={{backgroundColor: '#fff', position: 'absolute', height: '100%',width: '100%'}}   
                                    injectedJavaScript={fitscreen}                                                                                                         
                                    javaScriptEnabled={true}
                                    startInLoadingState={true}
                                    renderLoading={() => <ActivityIndicator size="large" color="#6c63ff" style={{marginTop: 100}}/>}/>
                            </View>
                        </View>
                    </Modal>
                </Container>
            </>
        )
    }
}

const styles = StyleSheet.create({
    attribution: {
        position: 'absolute',        
        left: 0,
        right: 0                           
      },
    bodyContainer:{
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10
    },
    listView:{
        width: '100%', 
        marginLeft: 0, 
        paddingLeft: 0, 
        paddingRight: 0, 
        marginRight: 0,
    },
    stepMainText:{
        fontSize: 18,        
        fontFamily: 'sansmedium',
        color: '#37474F',
        alignSelf: 'center',
        textAlign: 'center'
    },
    stepSubText:{
        fontSize: 14,
        marginTop: 10,
        fontFamily: 'sans',
        color: '#616161',
        alignSelf: 'center',
        textAlign: 'center'
    },
    modalBackground: {
        flex: 1,
        // alignItems: 'center',
        // flexDirection: 'column',
        // justifyContent: 'space-around',
        backgroundColor: '#fff'
      },
    modalHeader: {
        backgroundColor: 'white',        
        margin: 0,        
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',    
        height: Math.round(Dimensions.get('window').height) - Constants.statusBarHeight
    },
    headerStyle:{
        color: '#fff',
        fontFamily: 'sans',
        fontSize: 18,
        textAlign:'right',
        alignSelf: 'center',        
        width: '100%',
    },
    headerOptionStyle:{
        height: 60, 
        paddingLeft: 0,
        marginLeft: 0,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: '#6c63ff',
    },
});

function mapStateToProps(state){
    // console.log(state.userWalkerApplicationReducer.applicationData)
    return{
        applicationData: state.userWalkerApplicationReducer.applicationData
      };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({        
        setWalkerApplicationData: (applicationData) => setWalkerApplicationData(applicationData)
      }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(WalkerApplication);