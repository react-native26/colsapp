import React, { Component } from 'react';
import  {StyleSheet, View, Image } from 'react-native';
import { Content,  Text, List, ListItem, Separator, Button } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import CustomerBookingContext from '../../context/CustomerBookingContext';
import AwesomeButton from "react-native-really-awesome-button";
import ButtonGradient from 'src/components/ButtonGradient';
import {currencyFormatter} from 'src/utils/CurrencyFormatter';
import FontAwesome from 'src/components/FontAwesome5';

const Label = ({ title }) => {
    return (<Row size={1}>
                <Text style={styles.labelText}>
                    {title}
                </Text>
            </Row>);
};

const Value = ({ title }) => {
    return (<Row size={2}>
                <Text style={styles.valueText}>
                    {title}
                </Text>
            </Row>);
};

const ItemWithVal = (props) => {
    const { label, value } = props;
    return (<ListItem {...props} >
                <Grid>
                    <Col size={1}>                                
                        <Label title={label} />                         
                    </Col>
                    <Col size={2}>                                                                
                        <Value title={value} />
                    </Col>
                </Grid>
            </ListItem>);
}

export default class PaymentDetails extends Component {
    static contextType = CustomerBookingContext;

    state={
        payment_amt: currencyFormatter(this.context.payment_amount),
        payslug: '-'
    }

    componentDidMount(){
        switch (this.context.payment_status){
            case 1:
                this.setState({ payslug: 'Paid In Full'});
                break;
            case 2:
                this.setState({ payslug: 'Partially paid'});
                break;
            case 3:
                this.setState({ payslug: 'Payment Required'});
                break;
            default:
                this.setState({ payslug: '-'});
                break;

        }
    }

    render() {        
        return (
            <Content showsVerticalScrollIndicator={false} >
                <List style={{ backgroundColor: 'white'}}>
                    <Separator style={{ height: 15, backgroundColor: '#fff'}}/>
                    <View style={styles.formBlock}>
                        <ItemWithVal noIndent label="Status" value={this.state.payslug}/>
                        <ItemWithVal noIndent noBorder label="Price" value={this.state.payment_amt}/>
                    </View>
                    {/* <Separator/> */}
                    {/* <ListItem noIndent noBorder style={{justifyContent: 'center', alignItems:'center', margin: 0}}>
                        <Button transparent block style={{flex: 1, margin: 0}} iconLeft>
                            <FontAwesome name={'credit-card'} size={16} style={{color: '#4CAF50'}} />
                            <Text style={{color: '#4CAF50', fontFamily: 'sans', fontSize: 16}}>Make a Payment</Text>                                                     
                        </Button>                        
                    </ListItem>  */}
                    <View style={{marginLeft: 15, marginRight: 15}}>
                        <AwesomeButton 
                                    raiseLevel={0} 
                                    height={50} 
                                    stretch={true}
                                    textFontFamily="sansmedium"       
                                    textSize={16}          
                                    borderRadius={50}            
                                    ExtraContent={
                                        <ButtonGradient/>
                                      }
                                    // onPress={() => Linking.openURL(`tel:${this.context.walker_contact}`)}
                            >
                            <FontAwesome name={'credit-card'} size={18} style={{color: '#fff', marginLeft: 5, marginRight: 5}}/>
                            <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 16}}>Make a Payment</Text>
                        </AwesomeButton>
                    </View>                   
                </List>
                <View style={{justifyContent: 'flex-end', alignItems:'center', paddingTop: 30}}>                        
                    <Text style={{color: '#616161' , fontSize: 14}}>Pay securely with</Text>
                    <Image source={require('../../assets/images/payu.png')} style={{ width: 40, height: 20}}/>   
                </View>                
            </Content>
        )
    }
}

const styles = StyleSheet.create({
    avatarStyle:{
        width: 70,
        height: 70,
        borderRadius: 140/2,
        backgroundColor: 'white',
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },            
    avatarImg:{
        width: 60,
        height: 60,
        borderRadius: 100/2,
        resizeMode: "contain"
    },
    labelText:{
        fontFamily: 'sans',
        color: '#616161',
        fontSize: 14,
        textAlign: 'left',
        paddingTop: 5,
        paddingBottom: 5
    },
    valueText:{
        fontFamily: 'sans',        
        fontSize: 14,
        textAlign: 'right',
        flex: 1
    },
    formBlock:{ 
        marginLeft: 15, 
        marginRight: 15, 
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
    },  
    separatorStyle:{
        height: 70, 
        backgroundColor: '#fff'
    }
});