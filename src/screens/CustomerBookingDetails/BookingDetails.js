import React, { Component } from 'react';
import  {StyleSheet, View } from 'react-native';
import { Content, Thumbnail, Text, List, ListItem, Separator, Button } from 'native-base';
import ProgressCircle from 'react-native-progress-circle';
import { Col, Row, Grid } from "react-native-easy-grid";
import FontAwesome from '../../components/FontAwesome5';
import StepIndicator from 'react-native-step-indicator';
import AwesomeButton from "react-native-really-awesome-button";
import ButtonGradient from 'src/components/ButtonGradient';
import { Linking } from 'expo';
import CustomerBookingContext from '../../context/CustomerBookingContext';

const steplabels = ["Booking Complete","Pending Confirmation"];
const steplabels2 = ["Booking Complete","Booking Confirmed"];

const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize:30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#aaaaaa',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#4CAF50',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#4CAF50',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#4CAF50',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#aaaaaa',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#4CAF50',
    labelSize: 13,
    currentStepLabelColor: '#aaaaaa'
  }

const Label = ({ title }) => {
    return (<Row size={1} style={{ left: 4}}>
                <Text style={styles.labelText}>
                    {title}
                </Text>
            </Row>);
};

const Value = ({ title }) => {
    return (<Row size={2}>
                <Text style={styles.valueText}>
                    {title}
                </Text>
            </Row>);
};

const ItemWithVal = (props) => {
    const { label, value, icon } = props;
    return (<ListItem {...props} >
                <Grid>
                    <Col size={1} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        {
                            (icon)&& <FontAwesome name={icon} size={14}/> 
                        }                                                       
                        <Label title={label}/>                         
                    </Col>
                    <Col size={2} style={{paddingRight: 5}}>                                                                
                        <Value title={value} />
                    </Col>
                </Grid>
            </ListItem>);
}

export default class BookingDetails extends Component {
    static contextType = CustomerBookingContext;

    componentDidMount(){
        // console.log("BOOKING DETAILS ************",this.context);
    }

    render() {
        return (
            <Content showsVerticalScrollIndicator={false}>
                <List style={{ backgroundColor: 'white'}}>
                    {
                        (this.context.completed_walks === 0)&&
                        <ListItem noIndent noBorder style={{justifyContent: 'center', alignItems:'center'}}>
                            <View style={{ flex: 1 }}>
                                <StepIndicator
                                    customStyles={customStyles}
                                    currentPosition={(!this.context.booking_confirmed)? 1 : 2}
                                    labels={(!this.context.booking_confirmed)? steplabels : steplabels2}
                                    stepCount={2}
                                    direction="horizontal"
                                    renderStepIndicator={({position, stepStatus}) => {                                        
                                        if(stepStatus === 'finished')
                                        {                                            
                                            return <FontAwesome name={'check'} size={14} style={{color: 'white'}}/>;
                                        }else{
                                            return <Text style={{ color: '#aaaaaa'}}>{position + 1}</Text>;
                                        }
                                    }}
                                />
                            </View>
                        </ListItem>
                    }                    
                    <Separator style={styles.separatorStyle}>
                        <View style={{flexDirection: 'row'}}>
                            <FontAwesome name={'walking'} size={26} style={styles.profileIcons}/>
                            <Text style={styles.blockHeader}>Walker</Text>                            
                        </View>
                        <Text style={styles.blockSubHeader}>Your pet's walker</Text>
                    </Separator>
                    {/* <View style={styles.formBlock}> */}
                        <ListItem noIndent noBorder>
                            <View style={{flex: 1 }}>
                                    <View style={styles.avatarStyle}>
                                        <Thumbnail large source={(this.context.walker_id && this.context.walker_avatar)? { uri: this.context.walker_avatar}:require('../../assets/images/walker.png')} style={styles.avatarImg}/>                                
                                    </View>                                     
                                    {
                                        (this.context.walker_id)?
                                            <Text style={styles.valueText}>                                                
                                                {this.context.walker_name}   
                                            </Text>
                                        :<Text style={styles.valueText}>                                                
                                            Walker assignment pending...   
                                        </Text>
                                    } 
                            </View>
                            {/* <Grid>
                                <Col size={1}>                                
                                    <View style={styles.avatarStyle}>
                                        <Thumbnail large source={(this.context.walker_id && this.context.walker_avatar)? { uri: this.context.walker_avatar}:require('../../assets/images/walker.png')} style={styles.avatarImg}/>                                
                                    </View>                        
                                </Col>
                                <Col size={2}>                                                                
                                    <Row size={2}>
                                        {
                                            (this.context.walker_id)?
                                                <Text style={styles.valueText}>                                                
                                                    {this.context.walker_name}   
                                                </Text>
                                            :<Text style={styles.valueText}>                                                
                                                Walker assignment pending...   
                                            </Text>
                                        }
                                    </Row> 
                                </Col>
                            </Grid>                                                                    */}
                        </ListItem>
                    {/* </View> */}
                    <Separator style={styles.separatorStyle}>
                        <View style={{flexDirection: 'row'}}>
                            <FontAwesome name={'clipboard-list-check'} size={26} style={styles.profileIcons}/>
                            <Text style={styles.blockHeader}>Details</Text>                            
                        </View>
                        <Text style={styles.blockSubHeader}>Booking details</Text>
                    </Separator>    
                    <View style={styles.formBlock}>
                        <ItemWithVal noIndent label={"Pet Name"} value={this.context.pet_name}/>
                        <ItemWithVal noIndent label="Booking ID" value={this.context.booking_id}/>
                        <ItemWithVal noIndent label="Booking Date" value={this.context.booking_date}/>
                        <ItemWithVal noIndent label="Description" value={this.context.product_desc}/>
                        <ItemWithVal noIndent label="Walk Dates" value={`${this.context.start_date} to ${this.context.end_date}`}/>
                        <ItemWithVal noIndent noBorder label="Walk Times" value={this.context.walk_times.join(', ')}/>
                    </View>
                    <Separator style={styles.separatorStyle}>
                        <View style={{flexDirection: 'row'}}>
                            <FontAwesome name={'chart-pie-alt'} size={26} style={styles.profileIcons}/>
                            <Text style={styles.blockHeader}>Progress</Text>                            
                        </View>
                        <Text style={styles.blockSubHeader}>Walks completed and remaining in this booking</Text>                  
                    </Separator>
                    <View style={styles.formBlock}>
                        <ListItem noIndent noBorder style={{justifyContent: 'center', alignItems:'center'}}>
                            <ProgressCircle
                                percent={(this.context.completed_walks/this.context.total_walks * 100)}
                                radius={80}
                                borderWidth={15}
                                color="#00C853"
                                shadowColor="#ECEFF1"
                                bgColor="#fff"
                            >
                                <Text style={{ fontSize: 28, fontFamily: 'sans' , color: '#616161'}}>
                                    {this.context.completed_walks}/{this.context.total_walks}
                                </Text>
                                <Text style={{ fontSize: 12, fontFamily: 'sans' , color: '#616161'}}>Walks completed</Text>
                            </ProgressCircle>
                        </ListItem>
                    </View>
                    <Separator style={styles.separatorStyle}>
                        <View style={{flexDirection: 'row'}}>
                            <FontAwesome name={'clipboard'} size={26} style={styles.profileIcons}/>
                            <Text style={styles.blockHeader}>Instructions</Text>                            
                        </View>
                        <Text style={styles.blockSubHeader}>Instructions specified by you</Text>
                    </Separator>
                    <View style={styles.formBlock}>
                        <ListItem noIndent noBorder>
                            <Text style={styles.labelText}>
                                {(this.context.special_instructions)? this.context.special_instructions : 'n/a'}
                            </Text>                     
                        </ListItem>
                    </View>
                    <Separator style={{height: 15, backgroundColor: '#fff'}}/>                        
                    {
                        (this.context.walker_contact !== '')&&
                        <React.Fragment>
                            {/* <ListItem noIndent noBorder style={{justifyContent: 'center', alignItems:'center', margin: 0}}>
                                <Button transparent block iconLeft onPress={() => Linking.openURL(`tel:${this.context.walker_contact}`)} style={{flex: 1, margin: 0}}>
                                    <FontAwesome name={'phone-alt'} size={18} style={{color: '#4CAF50', marginLeft: 5, marginRight: 0}}/>
                                    <Text style={{color: '#4CAF50', fontFamily: 'sans', fontSize: 16}}>Contact Walker</Text>
                                </Button> 
                            </ListItem> */}
                            <View style={{marginLeft: 15, marginRight: 15}}>
                                <AwesomeButton 
                                            raiseLevel={0} 
                                            height={50} 
                                            stretch={true}
                                            textFontFamily="sansmedium"       
                                            textSize={16}          
                                            borderRadius={50}            
                                            ExtraContent={
                                                <ButtonGradient/>
                                              }
                                            onPress={() => Linking.openURL(`tel:${this.context.walker_contact}`)}
                                    >
                                    <FontAwesome name={'phone-alt'} size={18} style={{color: '#fff', marginLeft: 5, marginRight: 5}}/>
                                    <Text style={{color: '#fff', fontFamily: 'sansmedium', fontSize: 16}}>Contact Walker</Text>
                                </AwesomeButton>
                            </View>
                            <Separator style={{backgroundColor: '#fff'}}/> 
                        </React.Fragment>
                    }                                        
                </List>                
            </Content>
        )
    }
}

const styles = StyleSheet.create({
    avatarStyle:{
        width: 90,
        height: 90,
        borderRadius: 180/2,
        backgroundColor: 'white',
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },            
    avatarImg:{
        width: 80,
        height: 80,
        borderRadius: 160/2,
        resizeMode: "contain"
    },
    labelText:{
        fontFamily: 'sans',
        color: '#616161',
        fontSize: 14,
        textAlign: 'left',
        paddingTop: 5,
        paddingBottom: 5
    },
    valueText:{
        fontFamily: 'sans',        
        fontSize: 14,
        textAlign: 'right',
        flex: 1
    },
    formBlock:{ 
        marginLeft: 15, 
        marginRight: 15, 
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowOpacity: 0.25,
        shadowRadius: 5,
        shadowColor: '#607D8B',
        shadowOffset: { height: 0, width: 0 },
        elevation: 2,
    },        
    profileIcons:{        
        color: '#37474F',
        paddingRight: 5,
        alignSelf: 'center'
    },
    blockHeader:{ 
        fontFamily: 'sansmedium', 
        fontSize: 24, 
        color: '#37474F'
    },
    blockSubHeader:{ 
        fontFamily: 'sans', 
        fontSize: 12
    },
    separatorStyle:{
        height: 70, 
        backgroundColor: '#fff'
    }
});