import { applyMiddleware, createStore , combineReducers} from 'redux';
import { AsyncStorage } from 'react-native';
import { persistStore, persistReducer } from 'redux-persist';
import thunkMiddleware from 'redux-thunk';
import { userDataReducer, userWalkerApplicationReducer } from './reduxReducer';
import loggerMiddleware from './middleware/logger'

const appReducer = combineReducers({
  userDataReducer: userDataReducer,
  userWalkerApplicationReducer: userWalkerApplicationReducer
});

const persistConfig = {
    // Root
    key: 'root',
    // Storage Method (React Native)
    storage: AsyncStorage,    
    whitelist: [
      'userDataReducer',
      'userWalkerApplicationReducer'
    ],
    // blacklist: [
    //   'counterReducer',
    // ],
  };

  const middlewares = [thunkMiddleware, loggerMiddleware]
const persistedReducer = persistReducer(persistConfig, appReducer);
const store = createStore(persistedReducer, applyMiddleware(...middlewares));
let persistor = persistStore(store);
export {
    store,
    persistor,
  };