import { S3Get } from 'src/services/FileServiceProvider';
import ImageServices from 'src/services/ImageServices';
import API from 'src/services/ServiceProvider';
/*--------------------------------------------------------------------------------------------------
                                    All Reducers Goes Below
--------------------------------------------------------------------------------------------------*/

/**
 * Get User from LocalStorage Async
 */

// const GET_USER = 'getUser';
const SET_USER = 'setUser';
const GET_USER_ERR = 'getUserErr';

// const initialUserState = {};

export const userDataReducer = (state = { loading: true, 
                                          error: false,
                                          userData: {} }, action) =>{
    switch(action.type){
        case GET_USER_ERR:
            return {
                userData: {},
                loading: false,
                error: true
            }
        case SET_USER:
            return {
                userData: action.payload,
                loading: false,
                error:  false
            }
        default:
            return state;
    }
}

// This function will take User data either entered by the user manually or returned by the API
export const setUserData = (userdata) =>({
                    type: SET_USER,
                    payload: userdata
            });

export const getUserData = (username) => {
    return async dispatch => {
        try{                        
            // const result = await S3Get({
            //     filename: `prof/${username}.json`,
            //     config: { level: 'protected', download: true},
            //     filetype: 'json'
            // });                
            const result = await API({lambdaName: 'collrs-user-profile', 
                                      data: {
                                        cognitoUserName: username,
                                        operation: 'READ'
                                      }});
            //download and cache image
            // console.log("REDUX received data: ", result);
            if(result.responseCode === 200){
                let img;
                const { image, ...remaining } = result.data;
                if(image){
                    img = await ImageServices.DownloadCacheImage(image);
                    remaining['image'] = img;       // file system local URL
                    remaining['webImage'] = image; //https web image URL     
                }      
                dispatch({
                    type: SET_USER,
                    payload: {
                        user: remaining
                    }
                })
            }else{
                dispatch({
                    type: GET_USER_ERR
                })
            }            
        }catch(err){
            console.warn("Prefs File read errorr: ",err);
            dispatch({
                type: GET_USER_ERR
            })
        }
    }
}            
/*--------------------------------------------------------------------------------------------------
                                    End Reducer Section
--------------------------------------------------------------------------------------------------*/

/**
 * Get Walker User from LocalStorage Async
 */

const SET_WALKER_APPLICATION = 'setWalkerApplication';
const GET_WALKER_APPLICATION_ERR = 'getWalkerApplicationErr';

export const userWalkerApplicationReducer = (state = { loading: true, 
                                                error: false,
                                                applicationData: {
                                                    walkerapplied: true, 
                                                    status: 'REVIEW_PENDING'} }, action) =>{
    switch(action.type){
        case GET_WALKER_APPLICATION_ERR:
            return {
                applicationData: {},
                loading: false,
                error: true
            }
        case SET_WALKER_APPLICATION:
            return {
                applicationData: action.payload,
                loading: false,
                error:  false
            }
        default:
            return state;
    }
}

// This function will take User data either entered by the user manually or returned by the API
export const setWalkerApplicationData = (applicationData) =>({
                    type: SET_WALKER_APPLICATION,
                    payload: applicationData
            });