import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import {
    Placeholder,
    PlaceholderMedia,
    PlaceholderLine,
    ShineOverlay,
  } from 'rn-placeholder';

export default class PlaceHolder extends Component {
    render() {
        return (
        <View style={styles.container}>
            <Placeholder
                Left={PlaceholderMedia}
                // Right={PlaceholderMedia}
                Animation={ShineOverlay}>
                    <PlaceholderLine width={80} />
                    <PlaceholderLine />
                    <PlaceholderLine width={30} />
            </Placeholder>
            <View style={styles.paddingSpace}/>
            <Placeholder
                // Left={PlaceholderMedia}
                Right={PlaceholderMedia}
                Animation={ShineOverlay}>
                    <PlaceholderLine width={80} />
                    <PlaceholderLine />
                    <PlaceholderLine width={30} />
            </Placeholder>
            <View style={styles.paddingSpace}/>
            <Placeholder
                Left={PlaceholderMedia}
                // Right={PlaceholderMedia}
                Animation={ShineOverlay}>
                    <PlaceholderLine width={80} />
                    <PlaceholderLine />
                    <PlaceholderLine width={30} />
            </Placeholder>
            <View style={styles.paddingSpace}/>
            <Placeholder
                // Left={PlaceholderMedia}
                Right={PlaceholderMedia}
                Animation={ShineOverlay}>
                    <PlaceholderLine width={80} />
                    <PlaceholderLine />
                    <PlaceholderLine width={30} />
            </Placeholder>
          </View>
        )
    }
}

const styles = StyleSheet.create({ 
    container:{
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20
    },
    paddingSpace:{
        height: 50
    }
});