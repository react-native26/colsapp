import React, { Component } from 'react';
import { LinearGradient } from 'expo-linear-gradient';

export default class ButtonGradient extends Component {
    render() {
        return (
            <LinearGradient 
                colors={['#4b44d9','#6c63ff']}
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    height: '100%'
                  }}/>
        )
    }
}
