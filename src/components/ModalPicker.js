import React, { Component } from 'react';
import {ScrollView, StyleSheet, Text, View, TouchableOpacity, Platform, Dimensions, Keyboard} from 'react-native';
import {List, ListItem } from 'native-base';
import Modal from 'react-native-modal';
import FontAwesome from 'src/components/FontAwesome5';


export default class ModalPicker extends Component {

    constructor(props){
        super(props);
        const optionVal = this.props.options.find(elem => elem.value === this.props.value);
        this.state={
            visible: false,
            placeholder: this.props.placeholder,
            options: this.props.options,
            selectedValue: (optionVal)?optionVal.label: '',
            scrollable: this.props.scrollable || false,        
        }
    }
    

    onChange = (value, index, label) =>{
        this.props.onChange(value,index);
        this.setState({ visible: false, selectedValue: label });
    }

    close = () => this.setState({ visible: false });

    render() {
        let scrollableView = {};
        if(this.state.scrollable){
            scrollableView={
                height: Math.round(Dimensions.get('window').height) - 433
            }
        }
        return (
            <>
            <TouchableOpacity hitSlop={{ top: 5, bottom: 5, left: 5, right: 5}} onPress={() => {
                                            Keyboard.dismiss();
                                            this.setState({ visible: !this.state.visible})
                                        }} >
                {
                    (this.state.selectedValue === '')?
                    <Text style={[styles.placeholderStyle, this.props.placeholderStyle]}>{this.state.placeholder}</Text>
                    :<Text style={[styles.valueStyle, this.props.valueStyle]}>{this.state.selectedValue}</Text>
                }
                
            </TouchableOpacity>
            {
                (!this.state.scrollable)?
                <Modal              
                    isVisible={this.state.visible}
                    onBackdropPress={this.close}
                    animationIn='fadeIn'
                    animationOut='slideOutDown'
                    hardwareAccelerated={true}
                    >
                    <View style={styles.content}>                      
                        <List>
                            <ListItem noIndent style={styles.optionStyle}>
                                <Text style={styles.headerStyle}>{this.state.placeholder}</Text>
                                <TouchableOpacity hitSlop={{ top: 5, bottom: 5, left: 5, right: 5}} onPress={this.close}>
                                    <FontAwesome name={'times'} size={18} style={{ color: '#6c63ff'}}/>
                                </TouchableOpacity>
                            </ListItem>
                            {
                                this.state.options.map((option, index) => {
                                    return (
                                    <ListItem noIndent noBorder onPress={() => this.onChange(option.value, index, option.label)} key={option.value} style={styles.optionStyle}>
                                        <Text style={styles.itemStyle}>{option.label}</Text>
                                    </ListItem>)
                                })
                            }
                        </List>                    
                    </View>
                </Modal>
                :
                <Modal              
                    isVisible={this.state.visible}
                    onBackdropPress={this.close}
                    animationIn='fadeIn'
                    animationOut='slideOutDown'
                    hardwareAccelerated={true}
                    propagateSwipe={true}
                    >                    
                        <View style={[styles.content, scrollableView]}>  
                            <ListItem noIndent style={styles.headerOptionStyle}>
                                <Text style={styles.headerStyle}>{this.state.placeholder}</Text>
                                <TouchableOpacity hitSlop={{ top: 5, bottom: 5, left: 5, right: 5}} onPress={this.close}>
                                    <FontAwesome name={'times'} size={18} style={{ color: '#6c63ff'}}/>
                                </TouchableOpacity>
                            </ListItem>
                            <ScrollView showsVerticalScrollIndicator={true}>                                    
                                    {
                                        this.state.options.map((option, index) => {
                                            return (
                                            <ListItem noIndent noBorder onPress={() => this.onChange(option.value, index, option.label)} key={option.value} style={styles.optionStyle}>
                                                <Text style={styles.itemStyle}>{option.label}</Text>
                                            </ListItem>)
                                        })
                                    }
                            </ScrollView>                        
                        </View>
                        
                    </Modal>
            }
            
            </>
        )
    }
}

ModalPicker.defaultProps = {
    vlaue: '',
    placeholderStyle:{
        fontFamily: 'sans',
        fontSize: 14,
        color: '#9EA0A4'
    },
    itemStyle:{
        fontFamily: 'sans',
        fontSize: 24
    }
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',
        paddingLeft: (Platform.OS === 'android')? 14:2,
        borderRadius: 15,
        borderColor: 'rgba(0, 0, 0, 0.1)',    
    },
    placeholderStyle:{
        fontFamily: 'sans', 
        fontSize: 16, 
        color: '#9EA0A4', 
        opacity: 0.65
    },
    valueStyle:{
        fontFamily: 'sans', 
        fontSize: 16, 
    },
    optionStyle:{
        height: 50, 
        paddingLeft: 10,
        alignSelf: 'center'
    },
    itemStyle:{
        fontFamily: 'sans',
        fontSize: 16
    },
    headerStyle:{
        fontFamily: 'sansmedium', 
        fontSize: 16, 
        color: '#6c63ff', 
        alignSelf: 'center',
        flex: 1
        // opacity: 0.8
    },
    headerOptionStyle:{
        height: 50, 
        paddingLeft: 10,
        justifyContent: 'center',
        alignContent: 'center',
    }
  });

  //#6c63ff