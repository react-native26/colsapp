import React from 'react';
import { createIconSet } from '@expo/vector-icons';

const glyphMap = { 
    'dog-leashed': '\uf6d4', 
    'dog': '\uf6d3',
    'home-alt': '\uf80a', 
    'paw': '\uf1b0',
    'calendar-alt': '\uf073',
    'clock': '\uf017',
    'flag-alt': '\uf74c',
    'phone-alt': '\uf879',
    'camera-alt': '\uf332',
    'walking': '\uf554',
    'chevron-right': '\uf054',
    'chevron-left': '\uf053',
    'flag-checkered': '\uf11e',
    'thumbs-up': '\uf164',
    'check': '\uf00c',
    'sync': '\uf021',
    'clipboard': '\uf328',
    'shield-check': '\uf2f7',
    'stars': '\uf762',
    'star': '\uf005',
    'star-shooting': '\uf936',
    'tag': '\uf02b',
    'bars': '\uf0c9',
    'repeat': '\uf363',
    'credit-card': '\uf09d',
    'paper-plane': '\uf1d8',
    'info-circle': '\uf05a',
    'award': '\uf559',
    'badge-percent': '\uf646',
    'piggy-bank': '\uf4d3',
    'rupee-sign': '\uf156',
    'pen-nib': '\uf5ad',
    'envelope': '\uf0e0',
    'shield-check': '\uf2f7',
    'balance-scale': '\uf24e',
    'sign-out': '\uf08b',
    'weight': '\uf496',
    'baby-carriage': '\uf77d',
    'venus-mars': '\uf228',
    'male': '\uf183',
    'camera-retro': '\uf083',
    'user-edit': '\uf4ff',
    'heart-circle': '\uf4c7',
    'clipboard-prescription': '\uf5e8',
    'id-card-alt': '\uf47f',
    'address-book': '\uf2b9',
    'clipboard-list-check': '\uf737',
    'chart-pie-alt': '\uf64e',
    'bullseye': '\uf140',
    'question-square': '\uf2fd',
    'question-circle': '\uf059',
    'times': '\uf00d',
    'unlink': '\uf127',
    'times-circle': '\uf057',
    'rocket-launch': '\uf927',
    'file-alt': '\uf15c',
    'id-card-alt': '\uf47f',
    'fingerprint': '\uf577',
    'check-circle': '\uf058',
    'apple': '\uf179',
    'exlamation-triangle':'\uf071',
    'lightbulb-exclamation': '\uf671',
    'tasks': '\uf0ae',
    'handshake': '\uf2b5',
    'calendar-check': '\uf274'
};

//fontawesome defaults to Light font type if fatype is not provided
//FontAwesome Light, Regular, and Solid supported

const FontAwesome5 = (props) => {
    let expoAssetId, type = 'fa'; 

    switch (props.fatype){
        case 'light':
            expoAssetId = require("../assets/fonts/fa-light-300.ttf");
            type = 'fa';
            break;
        case 'solid':
            type = 'fas';
            expoAssetId = require("../assets/fonts/fa-solid-900.ttf");
            break;
        case 'regular':
            type = 'far';
            expoAssetId = require("../assets/fonts/fa-regular-400.ttf");
            break;
        case 'brand':
            type = 'fab';
            expoAssetId = require("../assets/fonts/fa-brands-400.ttf");
            break;
        default:
            type = 'fa';
            expoAssetId = require("../assets/fonts/fa-light-300.ttf");
            break;
    }
    const Icon = createIconSet(glyphMap, type, expoAssetId);
    return <Icon {...props}/>
};

export default FontAwesome5;    