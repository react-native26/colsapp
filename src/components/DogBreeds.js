import React, { Component } from 'react';
import {ScrollView, StyleSheet, Text, View, TouchableOpacity, Platform, ActivityIndicator, Keyboard, Dimensions, TextInput} from 'react-native';
import {ListItem, Separator, Row} from 'native-base';
import { Col, Grid } from "react-native-easy-grid";
import { Hub } from '@aws-amplify/core';
import debounce from 'lodash/debounce';
import Constants from 'expo-constants';
import FontAwesome from 'src/components/FontAwesome5';
import Modal from 'react-native-modal';
import API from 'src/services/ServiceProvider';
import Fuse from 'fuse.js';

export default class DogBreeds extends Component {
    constructor(props){
        super(props);
        
        this.state={
            visible: false,     
            buttonText: this.props.buttonText,
            loading: true,
            options: [],
            filteredOptions: [],
            error: '',
            filterText: '',
            filterError: ''
        }
    }

    getData = async() => {
        this.setState({ loading: true });
        try{
            const response = await API({ lambdaName: 'collrs-dog-api' });

            this.setState({ loading: false, options: response.body.data, filteredOptions:  response.body.data});
        }catch(err){
            this.setState({ loading: false, error: "Dog breeds aren't loading right now. Please try again later." });
        }
    }

    onChange = (breed, weight) =>{
        let minWeight, maxWeight, avWeight='';
        const arr = weight.split("-");
        if(arr.length === 2){
            minWeight = parseInt(arr[0].trim(), 10); 
            maxWeight = parseInt(arr[0].trim(), 10);
            avWeight = (minWeight+maxWeight)/2;
        }        
        this.props.onChange(breed, avWeight+'');
        this.setState({ visible: false });
    }

    close = () => {
        this.setState({ visible: false});
    }

    filterData = (value) => {
        // this.setState({ filterText: value });

        if(value){
        const options = {
            shouldSort: true,            
            threshold: 0.8,
            location: 0,
            distance: 100,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            keys: [
              "name",
              "description"
            ]
          };
          const fuse = new Fuse(this.state.options, options); // "list" is the item array
          const result = fuse.search(value);
          this.setState({ filteredOptions: result , filterError: (result.length === 0)? `No results found for "${value}"` :''});
        }else{            
            this.setState({ filteredOptions: this.state.options, filterError: '' });
        }
    }

    render() {
        const bounced = debounce(() => this.filterData(this.state.filterText), 1000);
        return (
            <>
                <TouchableOpacity hitSlop={{ top: 5, bottom: 5, left: 5, right: 5}} onPress={() => {
                                            Keyboard.dismiss();
                                            this.setState({visible: !this.state.visible });
                                            if(this.state.options.length === 0) this.getData();
                                        }} >
                    <View style={{flexDirection: 'row'}}>
                        <FontAwesome name={'info-circle'} size={12} style={styles.icons}/>
                        <Text style={[styles.buttonStyle, this.props.buttonStyle]}>{this.state.buttonText}</Text>
                    </View>                    
                </TouchableOpacity>
                                
                <Modal              
                    isVisible={this.state.visible}
                    onBackdropPress={this.close}
                    animationIn='slideInUp'
                    animationOut='slideOutDown'
                    hardwareAccelerated={true}
                    propagateSwipe={true}
                    style={{justifyContent: 'flex-end', margin: 0}}
                    >                    
                        <View style={styles.content}>  
                            <ListItem noBorder noBorder style={styles.headerOptionStyle}>
                                <Grid style={{width: '100%'}}>
                                    <Col size={1}>
                                        <Text></Text>
                                    </Col>
                                    <Col size={1}>                                        
                                        <Text style={styles.headerStyle}>Dog Breeds</Text>
                                    </Col>
                                    <Col size={1}>
                                        <TouchableOpacity hitSlop={{ top: 15, bottom: 15, left: 15, right: 15}} onPress={this.close}>
                                            <FontAwesome name={'times'} size={24} style={{ color: '#fff', alignSelf: 'flex-end'}}/>
                                        </TouchableOpacity>
                                    </Col>
                                </Grid>
                            </ListItem>
                            {
                                (this.state.loading === true)?
                                <View style={{flex: 1,
                                            alignItems: 'center',
                                            flexDirection: 'column',
                                            justifyContent: 'space-around', bottom: 100}}>
                                    <ActivityIndicator animating={this.state.loading} size='large'/>
                                </View>
                                :(this.state.error !== '')?
                                <View style={{flex: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center', paddingBottom: 100}}>
                                    <FontAwesome name={'unlink'} size={60} style={{ color: 'black',alignSelf: 'center' }}/>
                                    <Text style={[styles.itemStyle, {alignSelf: 'center', margin: 40, textAlign: 'center'}]}>{this.state.error}</Text>
                                </View>
                                :
                                <>
                                <View style={{backgroundColor: "#rgba(158, 160, 164, 0.3)", flexDirection: 'row', justifyContent: 'center'}}>
                                    <TextInput 
                                        placeholder="Search Breed" 
                                        placeholderTextColor="#rgba(158, 160, 164, 0.65)" 
                                        value={this.state.filterText}                                                                                                                    
                                        style={{height: 35, borderRadius: 40, margin: 5, backgroundColor: 'white', color: 'black', paddingLeft: 10, fontSize: 14, fontFamily: 'sans', flex: 1}}
                                        onChangeText={text => {                                            
                                            this.setState({ filterText: text}, () => bounced());
                                        }}
                                        />
                                    {
                                        (this.state.filterText !== '')&&
                                        <TouchableOpacity style={{alignSelf: 'center', marginRight: 5}} 
                                                          onPress={() => {                                                                
                                                                this.setState({ filterText: ''});
                                                                this.filterData('');
                                                              }} hitSlop={{left: 10, top: 10, bottom: 10}}>
                                            <FontAwesome name={'times-circle'} fatype='solid' size={16} style={{ color: 'black', alignSelf: 'center' }}/>
                                        </TouchableOpacity>
                                    }
                                </View>
                                <ScrollView showsVerticalScrollIndicator={true}>                                      
                                    {
                                        this.state.filteredOptions.map((option, index) => {
                                            return (
                                            <ListItem noIndent onPress={() => this.onChange(option.name, option.weight.metric)} key={option.id} style={styles.optionStyle}>
                                                <View style={{width: '100%'}}>
                                                    <Text style={styles.itemStyle}>{option.name}</Text>
                                                    <Text style={styles.subItemStyle}>Weight: {option.weight.metric} Kg (avg.)</Text>
                                                </View>                                                    
                                            </ListItem>)
                                        })
                                    }
                                    {
                                        (this.state.filterError !== '')&&
                                        <View style={{flex: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center'}}>                                            
                                            <Text style={[styles.itemStyle, {alignSelf: 'center', margin: 40, textAlign: 'center'}]}>{this.state.filterError}</Text>
                                        </View>
                                    }
                                    <Separator style={{height: 30, backgroundColor: 'white'}}/>                                                                                                                    
                                </ScrollView> 
                                </>
                            }
                                                   
                        </View>
                        
                    </Modal>              
            </>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',        
        margin: 0,        
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',    
        height: Math.round(Dimensions.get('window').height) - Constants.statusBarHeight
    },
    buttonStyle:{
        fontFamily: 'sans', 
        fontSize: 12, 
        color: '#6c63ff'
    },
    optionStyle:{
        height: 60, 
        paddingLeft: 10,
    },
    itemStyle:{
        fontFamily: 'sans',
        fontSize: 16
    },
    subItemStyle:{
        top: 4,
        fontFamily: 'sans',
        fontSize: 12,
        opacity: 0.8,
        textAlign: 'right',
        width: '100%'
    },
    headerStyle:{
        color: '#fff',
        fontFamily: 'sans',
        fontSize: 18,
        textAlign:'right',
        alignSelf: 'center',        
        width: '100%',
    },
    headerOptionStyle:{
        height: 60, 
        paddingLeft: 0,
        marginLeft: 0,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: '#6c63ff',
    },
    icons:{        
        color: '#6c63ff',
        paddingRight: 2,
        alignSelf: 'center'
    }
  });