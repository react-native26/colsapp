import React from 'react';
import { Dimensions, StyleSheet, Platform, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator, createAppContainer, createStackNavigator, createDrawerNavigator } from 'react-navigation';
import { Footer, FooterTab, Button, Text, StyleProvider, Title} from 'native-base';
import { AppearanceProvider } from 'react-native-appearance';
import CustomerDawer from './CustomerDrawer';
import getTheme from '../../native-base-theme/components';
import platform from '../../native-base-theme/variables/platform';
import CustomerHome from 'src/screens/CustomerHome';
import Products from 'src/screens/Products';
import ProductBooking from 'src/screens/ProductBooking';
import CustomerBookingDetails from 'src/screens/CustomerBookingDetails';
import CustomerPetProfile from 'src/screens/CustomerPetProfile';
import CreatePetProfile from 'src/screens/CreatePetProfile';
import FontAwesome from '../components/FontAwesome5';

const CustomerHomeStack = createStackNavigator({
    CustomerHome: { 
        screen: CustomerHome,
        navigationOptions: {
            title: 'Home',
            headerTitle: () => <Title style={styles.headerTitle}>Home</Title>
          }
    },
    CustomerBookingDetails: { 
        screen: CustomerBookingDetails,
        navigationOptions: {
            title: 'Walk Details',
            headerTitle: () => <Title style={styles.headerTitle}>Walk Details</Title>
          }
    },    
  }, {
    initialRoute: 'CustomerHome',
    defaultNavigationOptions: ({ navigation }) => ({
        headerTintColor: '#fff',
        headerStyle: {
            backgroundColor: '#6c63ff',     
        },
        headerForceInset: {  },
        headerRight: <TouchableOpacity onPress={() => navigation.openDrawer()} hitSlop={{left: 20, right: 10, top: 10, bottom: 10}}>
                        <FontAwesome
                            fatype='regular'
                            name={'bars'}
                            style={{ color: 'white', paddingRight: 15, paddingLeft: 15 }}                                            
                            size={20}
                            />
                    </TouchableOpacity>
      })
    });

const CustomerProductStack = createStackNavigator({
        Products: { 
            screen: Products,
            navigationOptions: {
                title: '',
                headerTitle: () => <Title style={styles.headerTitle}>Booking</Title>
            }
        },
        ProductBooking:{
            screen: ProductBooking,
            navigationOptions: ({ navigation }) => { return({
                title: null,
                headerTitle: () => <Title style={styles.headerTitle}>{navigation.getParam('ProductBookingTitle')}</Title>,              
            }) }
        }
    },{
        initialRoute: 'Products',
        defaultNavigationOptions: ({ navigation }) => ({
            headerTintColor: '#fff',
            headerStyle: {
                backgroundColor: '#6c63ff',            
            },
            headerForceInset: {  },
            headerRight: <TouchableOpacity onPress={() => navigation.openDrawer()} hitSlop={{left: 20, right: 10, top: 10, bottom: 10}}>
                            <FontAwesome
                                name={'bars'}
                                fatype='regular'
                                style={{ color: 'white', paddingRight: 15, paddingLeft: 15  }}                                               
                                size={20}
                                />
                        </TouchableOpacity>
      })
    });

const CustomerPetProfileStack = createStackNavigator({
        CustomerPetProfile:{
            screen: CustomerPetProfile,
            navigationOptions: ({ navigation }) => ({
                title: '',
                headerTitle: () => <Title style={styles.headerTitle}>Profile</Title>,
                headerTintColor: '#fff',
                headerStyle: {
                    backgroundColor: '#6c63ff',            
                },
                headerRight: <TouchableOpacity onPress={() => navigation.navigate('EditPetProfile', {mode: 'edit'})} hitSlop={{left: 20, right: 10, top: 10, bottom: 10}}>
                                <FontAwesome
                                    name={'user-edit'}
                                    style={{ color: 'white', paddingRight: 15, paddingLeft: 15 }}                                                
                                    size={20}
                                    />
                            </TouchableOpacity>
                            
            })
        },
        EditPetProfile: {
          screen: CreatePetProfile,
          navigationOptions: ({ navigation }) =>  ({
            headerTintColor: '#fff',            
            title: 'Edit Profile',
            headerTitle: () => <Title style={styles.headerTitle}>Edit Profile</Title>,
            headerStyle:{                    
              backgroundColor: '#6c63ff', 
            },
            headerRight: <TouchableOpacity style={{marginRight: 15}}
                                onPress={() =>{
                                    const saveProfile = navigation.getParam('createSaveProfile');
                                    saveProfile();
                                }}>
                            <Text style={{ fontSize: 18 , color: '#fff'}}>Save</Text>
                        </TouchableOpacity>
          })
        }
},{
    initialRoute: 'CustomerPetProfile',
    defaultNavigationOptions: ({ navigation }) => ({
        headerTintColor: '#fff',
        headerStyle: {
            backgroundColor: '#6c63ff',            
        },
        headerForceInset: {  }
  })
});

const AppTabsNavigator = createBottomTabNavigator({
        CustomerHomeStack: { 
            screen: CustomerHomeStack,
        },
        CustomerProductStack: {                      //needs to be converted to a stack navigator
            screen: CustomerProductStack,
        },
        CustomerPetProfileStack: {
            screen: CustomerPetProfileStack
        }
    },
    {
        navigationOptions: ({navigation}) => {
            return {
                header: null
            }
        },
        tabBarPosition: 'bottom',
        swipeEnabled: false,
        tabBarComponent: props => {
            return (                
                    <Footer>
                        <FooterTab>
                            <Button
                                vertical
                                active={props.navigation.state.index === 0}
                                onPress={() => props.navigation.navigate("CustomerHomeStack")}>
                                <FontAwesome fatype={(props.navigation.state.index === 0)?'solid':undefined} name={'home-alt'}  size={24} style={{color: (props.navigation.state.index === 0)? '#6c63ff':'#90A4AE'}}/>
                                <Text>Home</Text>
                            </Button>
                            <Button
                                vertical
                                active={props.navigation.state.index === 1}
                                onPress={() => props.navigation.navigate("CustomerProductStack")}>
                                <FontAwesome fatype={(props.navigation.state.index === 1)?'solid':undefined} name={'dog-leashed'} size={24} style={{color: (props.navigation.state.index === 1)? '#6c63ff':'#90A4AE'}}/>
                                <Text>Booking</Text>
                            </Button>
                            <Button
                                vertical
                                active={props.navigation.state.index === 2}
                                onPress={() => props.navigation.navigate("CustomerPetProfileStack")}>
                                <FontAwesome fatype={(props.navigation.state.index === 2)?'solid':undefined} name={'paw'} size={24} style={{color: (props.navigation.state.index === 2)? '#6c63ff':'#90A4AE'}}/>
                                <Text>Profile</Text>
                            </Button>
                        </FooterTab>
                    </Footer>                
            );
        }
});

// const AppTabsStackNavigator = createStackNavigator({
//     AppTabsNavigator: AppTabsNavigator
// });

const AppDrawerNavigator = createDrawerNavigator({
    AppTabsNavigator: {
      screen: AppTabsNavigator
    }
  },{
    drawerPosition: 'right',
    drawerWidth: Dimensions.get('window').width / 1.8,
    overlayColor: "rgba(0,0,0,0.40)",
    contentComponent: CustomerDawer
    // drawerBackgroundColor: 'rgba(0,0,0,0.60)'
  });

  const styles = StyleSheet.create({
      headerTitle:{
        color: 'white', 
        fontFamily: 'sans', 
        fontSize: 18, 
        paddingLeft: (Platform.OS === 'android')?15:0
      }
  });

// const AppTabNavigator = createAppContainer(AppNavigator);
const AppTabNavigator = createAppContainer(AppDrawerNavigator);

export default () => <AppearanceProvider>
    <StyleProvider style={getTheme(platform)}>
        <AppTabNavigator/>
    </StyleProvider>
</AppearanceProvider>

// export default () => <AppTabNavigator/>