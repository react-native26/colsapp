import React, { Component } from 'react';
import { Container, Footer, FooterTab, Button, Icon, Text} from 'native-base';
import { 
    // Ionicons,
    // MaterialIcons,
    // Foundation,
    MaterialCommunityIcons,
    // Octicons,
    AntDesign
  } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

class BottomTabBar extends Component {

  state = {
    active : this.props.position,
    button1Color: (this.props.position === 1)?'#6c63ff':'#607D8B',
    button2Color: (this.props.position === 2)?'#6c63ff':'#607D8B',
    button3Color: (this.props.position === 3)?'#6c63ff':'#607D8B'
  }

  onTabChange = (position) => {
    switch (position){
      case 1: 
        this.props.navigation.navigate('CustomerHome');
        break;
      case 2: 
        this.props.navigation.navigate('CustomerBooking');
        break;
      case 3:
    };
  }  

  render() {
    const iconColor = (this.state.active === 1)? true:false;
    return (
            <Footer>
                <FooterTab>
                    <Button active={(this.state.active === 1)? true:false} onPress={() => this.onTabChange(1)}>
                        {/* <Icon active name="home" /> */}
                        <AntDesign name={'home'}  size={32} style={{color: this.state.button1Color}}/>
                        <Text>Home</Text>
                    </Button>
                    <Button active={(this.state.active === 2)? true:false} onPress={() => this.onTabChange(2)}>
                      <MaterialCommunityIcons name={'dog-service'} size={32} style={{color: this.state.button2Color}}/>
                      <Text>Booking</Text>
                    </Button>
                    <Button active={(this.state.active === 3)? true:false} onPress={() => this.onTabChange(3)}>
                      <MaterialCommunityIcons name={'paw'} size={32} style={{color: this.state.button3Color}}/>
                      <Text>Profile</Text>
                    </Button>
                </FooterTab>
            </Footer>
    );
  }
}

export default withNavigation(BottomTabBar);