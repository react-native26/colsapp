import React, { Component } from 'react';
import {  ScrollView, View, StyleSheet, Platform } from 'react-native';
import { List, ListItem, Button, Text, Header } from 'native-base';
import NavigationService from 'src/components/NavigationService';
import { Hub } from 'aws-amplify';
import * as WebBrowser from 'expo-web-browser';
import FontAwesome from 'src/components/FontAwesome5';
import { Auth } from 'aws-amplify';
import Constants from 'expo-constants';
import Loading from 'src/components/ActivityHud';

class CustomerDrawer extends Component{
    state={
        loggingOut: false
    }

    componentDidMount(){
        Hub.listen("auth", this._logOut);
    }

    _logOut = ({ payload: { event, data } }) => {
        switch (event) {              
            case "signOut":              
              NavigationService.navigateMainNavigator('Main');
              break;
          }
    }


    openWeb = async (url) =>{
        await WebBrowser.openBrowserAsync(url, {enableBarCollapsing: true });
    }

    signOut = () => {
        this.setState({ loggingOut: true });
        Auth.signOut()
        .then(data => this.setState({ loggingOut: false }))
        .catch(err => this._showAlert('Error', 'Unable to logout'));
      }

      _showAlert = (header, error) => {
        Alert.alert(
            header,
            error,
          [
            { text: 'OK', onPress: () => this.setState({ loggingOut: false }) },
          ],
          { cancelable: false }
        )
      }

    componentWillUnmount(){
        Hub.remove("auth", this._logOut);
    }
    // componentDidMount(){
    //     console.log("Status bar height= ", Constants.statusBarHeight);
    // }

    render(){
    return (
        <View style={{height: '100%', flex: 1}}>
            <Header transparent={false} style={styles.headerStyle}/>
            <Loading loading={this.state.loggingOut}/>
            <ScrollView >
                <List>
                    <ListItem noIndent style={styles.listitem} onPress={() => this.openWeb('https://www.collrs.com/about')}>
                        <View style={{ flexDirection: 'row' }}>
                            <FontAwesome name={'pen-nib'} size={16} style={styles.icons}/>
                            <Text style={styles.labelText}>About Us</Text>
                        </View>                            
                    </ListItem>     
                    <ListItem noIndent style={styles.listitem} onPress={() => this.openWeb('https://www.collrs.com/contact-us')}>
                        <View style={{ flexDirection: 'row' }}>
                            <FontAwesome name={'envelope'} size={16} style={styles.icons} />
                            <Text style={styles.labelText}>Contact Us</Text>
                        </View>
                    </ListItem>                                     
                    
                    <ListItem noIndent style={styles.listitem} onPress={() => this.openWeb('https://www.collrs.com/privacy-policy')}>
                        <View style={{ flexDirection: 'row' }}>
                            <FontAwesome name={'shield-check'} size={16} style={styles.icons} />
                            <Text style={styles.labelText}>Privacy Policy</Text>
                        </View>
                    </ListItem>
                    <ListItem noIndent style={styles.listitem} onPress={() => this.openWeb('https://www.collrs.com/privacy-policy')}>
                        <View style={{ flexDirection: 'row' }}>
                            <FontAwesome name={'balance-scale'} size={16} style={styles.icons} />
                            <Text style={styles.labelText}>Legal</Text>
                        </View>
                    </ListItem>
                    <ListItem noIndent style={styles.listitem} onPress={() => this.openWeb('https://www.collrs.com/about')} button={true}>
                        <View style={{ flexDirection: 'row' }}>
                            <FontAwesome name={'info-circle'} size={16} style={styles.icons}/>
                            <Text style={styles.labelText}>Cancellation & Refunds</Text>
                        </View>                            
                    </ListItem> 
                </List>
            </ScrollView>
            <View style={{ justifyContent: 'flex-end', alignContent: 'flex-end', flex: 1, marginBottom: 20}}>
                <Button block style={{backgroundColor: '#EF5350', borderRadius: 0}} iconLeft onPress={this.signOut}>
                    <FontAwesome name={'sign-out'} size={16} style={{ color: '#fff'}}/>
                    <Text>Logout</Text>
                </Button>
                <Text style={styles.footerVersion}>v1.0.1</Text>          
            </View>
        </View>
    );
    }
}

const styles = StyleSheet.create({     
    listitem:{
        height: 60,
        // backgroundColor: 'red'
    },
    labelText:{
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 14
    },
    footerVersion:{
        textAlign: 'center',
        fontFamily: 'sans',
        color: '#37474F',
        fontSize: 12,
        marginTop: 10
    },
    icons:{        
        color: '#616161',
        paddingRight: 5,
        alignSelf: 'center'
    },
    headerStyle:{
        // paddingTop: (Platform.OS === 'android')? Constants.statusBarHeight  : 0,
        backgroundColor: '#6c63ff'
    }
});

export default CustomerDrawer;