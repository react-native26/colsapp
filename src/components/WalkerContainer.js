import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


import { Dimensions, StyleSheet, Platform, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator, createAppContainer, createStackNavigator, createDrawerNavigator } from 'react-navigation';
import { Footer, FooterTab, Button, Text, StyleProvider, Title } from 'native-base';
import { AppearanceProvider } from 'react-native-appearance';
import CustomerDawer from './CustomerDrawer';
import getTheme from '../../native-base-theme/components';
import platform from '../../native-base-theme/variables/platform';
import FontAwesome from '../components/FontAwesome5';

import WalkerHomeScreen from '../screens/WalkerScreens/HomeStack/WalkerHome'
import AvailableTasks from '../screens/WalkerScreens/HomeStack/AvailableTasks'
import AvailableTasksDetails from '../screens/WalkerScreens/HomeStack/AvailableTaskDetails'
import MyAvailabelTasks from '../screens/WalkerScreens/MyTasksStack/MyAvailableTasks'
import MyTaskDetails from '../screens/WalkerScreens/MyTasksStack/MyTaskDetails'
import CreateWalkerPetProfile from '../screens/WalkerScreens/CreateWalkerPetProfile'
import WalkerPetProfile from '../screens/WalkerScreens/WalkerPetProfile'




const WalkerContainer = (props) => {
    const [status, setStatus] = useState('REVIEW_PENDING')

    useEffect(() => {
        setStatus(props.applicationData.status)
    }, [props.applicationData])

    const getStatus = () => {
        return status
    }

    const WalkerHomeStack = createStackNavigator(
        {
            WalkerHome: {
                screen: WalkerHomeScreen,
                navigationOptions: {
                    title: 'Home',
                    headerTitle: () => <Title style={styles.headerTitle}>Home</Title>
                }
            },
            AvailableNearbyTasks: {
                screen: AvailableTasks,
                navigationOptions: {
                    title: 'Tasks',
                    headerTitle: () => <Title style={styles.headerTitle}>Tasks</Title>
                }
            },
            TaskDetails: {
                screen: AvailableTasksDetails,
                navigationOptions: {
                    title: 'Task',
                    headerTitle: () => <Title style={styles.headerTitle}>Task</Title>
                }
            }
        },
        {
            initialRouteName: getStatus() === 'APPROVED' ? 'AvailableNearbyTasks' : 'WalkerHome',
            defaultNavigationOptions: ({ navigation }) => ({
                headerTintColor: '#fff',
                headerStyle: {
                    backgroundColor: '#6c63ff',
                },
                headerForceInset: {},
                headerRight: <TouchableOpacity onPress={() => navigation.openDrawer()} hitSlop={{ left: 20, right: 10, top: 10, bottom: 10 }}>
                    <FontAwesome
                        fatype='regular'
                        name={'bars'}
                        style={{ color: 'white', paddingRight: 15, paddingLeft: 15 }}
                        size={20}
                    />
                </TouchableOpacity>
            })
        });

    const WalkerTasksStack = createStackNavigator({
        AvailableNearbyMyTasks: {
            screen: MyAvailabelTasks,
            navigationOptions: {
                title: 'My Tasks',
                headerTitle: () => <Title style={styles.headerTitle}>My Tasks</Title>
            }
        },
        MyTaskDetails: {
            screen: MyTaskDetails,
            navigationOptions: {
                title: 'My Task',
                headerTitle: () => <Title style={styles.headerTitle}>My Task</Title>
            }
        }
    },
        {
            initialRoute: 'AvailableNearbyTasks',
            defaultNavigationOptions: ({ navigation }) => ({
                headerTintColor: '#fff',
                headerStyle: {
                    backgroundColor: '#6c63ff',
                },
                headerForceInset: {},
                headerRight: <TouchableOpacity onPress={() => navigation.openDrawer()} hitSlop={{ left: 20, right: 10, top: 10, bottom: 10 }}>
                    <FontAwesome
                        fatype='regular'
                        name={'bars'}
                        style={{ color: 'white', paddingRight: 15, paddingLeft: 15 }}
                        size={20}
                    />
                </TouchableOpacity>
            })
        })



    const WalkerPetProfileStack = createStackNavigator({
        WalkerPetProfile: {
            screen: WalkerPetProfile,
            navigationOptions: ({ navigation }) => ({
                title: '',
                headerTitle: () => <Title style={styles.headerTitle}>Profile</Title>,
                headerTintColor: '#fff',
                headerStyle: {
                    backgroundColor: '#6c63ff',
                },
                headerForceInset: {},
                headerRight: <TouchableOpacity onPress={() => navigation.navigate('EditPetProfile', { mode: 'edit' })} hitSlop={{ left: 20, right: 10, top: 10, bottom: 10 }}>
                    <FontAwesome
                        name={'user-edit'}
                        style={{ color: 'white', paddingRight: 15, paddingLeft: 15 }}
                        size={20}
                    />
                </TouchableOpacity>

            })
        },
        EditPetProfile: {
            screen: CreateWalkerPetProfile,
            navigationOptions: ({ navigation }) => ({
                headerTintColor: '#fff',
                title: 'Edit Profile',
                headerTitle: () => <Title style={styles.headerTitle}>Edit Profile</Title>,
                headerStyle: {
                    backgroundColor: '#6c63ff',
                },
                headerForceInset: {},
                headerRight: <TouchableOpacity style={{ marginRight: 15 }}
                    onPress={() => {
                        const saveProfile = navigation.getParam('createSaveProfile');
                        saveProfile();
                    }}>
                    <Text style={{ fontSize: 18, color: '#fff' }}>Save</Text>
                </TouchableOpacity>
            })
        }
    }, {
        initialRoute: 'WalkerPetProfile'
    });

    const isShowMyTasksTab = (props) => {
        if (status === 'APPROVED' || status === 'REVIEW_PENDING') {
            return true
        }
        return false
    }

    const AppTabsNavigator = createBottomTabNavigator({
        WalkerHomeStack: {
            screen: WalkerHomeStack,
        },
        MyTasksStack: {
            screen: WalkerTasksStack
        },
        WalkerPetProfileStack: {
            screen: WalkerPetProfileStack
        }
    },
        {

            navigationOptions: ({ navigation }) => {
                return {
                    header: null,
                }
            },
            tabBarPosition: 'bottom',
            swipeEnabled: false,
            tabBarComponent: props => {
                return (
                    <Footer>
                        <FooterTab>
                            <Button
                                vertical
                                active={props.navigation.state.index === 0}
                                onPress={() => props.navigation.navigate("WalkerHomeStack")}>
                                <FontAwesome fatype={(props.navigation.state.index === 0) ? 'solid' : undefined} name={'home-alt'} size={24} style={{ color: (props.navigation.state.index === 0) ? '#6c63ff' : '#90A4AE' }} />
                                <Text>Home</Text>
                            </Button>
                            {


                                isShowMyTasksTab(props) && <Button
                                    vertical
                                    active={props.navigation.state.index === 1}
                                    onPress={() => props.navigation.navigate("MyTasksStack")}>
                                    <FontAwesome fatype={(props.navigation.state.index === 1) ? 'solid' : undefined} name={'tasks'} size={24} style={{ color: (props.navigation.state.index === 1) ? '#6c63ff' : '#90A4AE' }} />
                                    <Text>My Tasks</Text>
                                </Button>
                            }
                            <Button
                                vertical
                                active={props.navigation.state.index === 2}
                                onPress={() => props.navigation.navigate("WalkerPetProfileStack")}>
                                <FontAwesome fatype={(props.navigation.state.index === 2) ? 'solid' : undefined} name={'paw'} size={24} style={{ color: (props.navigation.state.index === 2) ? '#6c63ff' : '#90A4AE' }} />
                                <Text>Profile</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                );
            }
        });

    // const AppTabsStackNavigator = createStackNavigator({
    //     AppTabsNavigator: AppTabsNavigator
    // });

    const AppDrawerNavigator = createDrawerNavigator({
        AppTabsNavigator: {
            screen: AppTabsNavigator
        }
    }, {
        drawerPosition: 'right',
        drawerWidth: Dimensions.get('window').width / 1.8,
        overlayColor: "rgba(0,0,0,0.40)",
        contentComponent: CustomerDawer
        // drawerBackgroundColor: 'rgba(0,0,0,0.60)'
    });



    // const AppTabNavigator = createAppContainer(AppNavigator);
    const AppTabNavigator = createAppContainer(AppDrawerNavigator);


    return (
        <AppearanceProvider>
            <StyleProvider style={getTheme(platform)}>
                <AppTabNavigator />
            </StyleProvider>
        </AppearanceProvider>
    )
}


function mapStateToProps(state) {
    // console.log(state.userWalkerApplicationReducer.applicationData)
    return {
        applicationData: state.userWalkerApplicationReducer.applicationData
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WalkerContainer);

const styles = StyleSheet.create({
    headerTitle: {
        color: 'white',
        fontFamily: 'sans',
        fontSize: 18,
        paddingLeft: (Platform.OS === 'android') ? 15 : 0
    }
});
