import React, { Component } from 'react';
import {ScrollView, StyleSheet, Text, View, TouchableOpacity, Platform, Dimensions, Keyboard} from 'react-native';
import {List, ListItem } from 'native-base';
import Modal from 'react-native-modal';
import FontAwesome from 'src/components/FontAwesome5';


export default class AlertModal extends Component {

    constructor(props){
        super(props);
        
        this.state={
            visible: false,
            header: this.props.header,            
            content: this.props.content,
            headerIcon: this.props.headerIcon,
            scrollable: this.props.scrollable || false,     
            okText: this.props.okText || 'OK',
            cancelText: this.props.cancelText   
        }
    }

    componentDidUpdate(prevProp){
        if(prevProp.visible !== this.props.visible){
           
            this.setState({ visible: this.props.visible })
        }
    }


    close = () => {
        this.setState({ visible: false });
        this.props.onDismissed();
    }

    render() {
        let scrollableView = {};
        if(this.state.scrollable){
            scrollableView={
                height: Math.round(Dimensions.get('window').height) - 350
            }
        }
        return (
            <>
            {
                (!this.state.scrollable)?
                <Modal              
                    isVisible={this.state.visible}
                    onBackdropPress={this.close}
                    animationIn= {this.props.animationIn || 'slideInUp'}
                    animationOut= {this.props.animationOut || 'slideOutDown'}
                    hardwareAccelerated={true}
                    >
                    <View style={styles.content}>                      
                        <View style={styles.contentContainer}>
                            {
                                (this.state.headerIcon)&&
                                <FontAwesome name={this.state.headerIcon} size={70} style={{ ...styles.headerIconStyle, ...this.props.headerIconStyle }}/>
                            }   
                            <List>
                                <ListItem noIndent noBorder>
                                    <Text style={{...styles.headerStyle, ...this.props.headerStyle}}>{this.state.header}</Text>
                                </ListItem>
                            </List>                       
                            <Text style={{...styles.contentStyle, ...this.props.contentstyle}}>{this.state.content}</Text>
                           
                        </View>  
                        <View
                            style={{
                                borderBottomColor: '#CFD8DC',
                                borderBottomWidth: StyleSheet.hairlineWidth,
                                alignSelf:'stretch'
                            }}
                            />     
                        <View style={{flexDirection: 'row', width: '100%'}}>
                            <TouchableOpacity style={{...styles.actionButtons, flex: 1}} 
                                        onPress={() => {
                                                this.close();
                                                (this.props.onOk) && this.props.onOk();
                                             }
                                            }>
                                <Text style={{...styles.buttonStyle, ...this.props.okButtonStyle}}>{this.state.okText}</Text>                                    
                            </TouchableOpacity>    
                            {
                                (this.state.cancelText)&&
                                <>
                                    <View
                                        style={{
                                            borderRightColor: '#CFD8DC',
                                            borderRightWidth: StyleSheet.hairlineWidth
                                        }}
                                    />     
                                    <TouchableOpacity style={{...styles.actionButtons, flex: 1}} 
                                                onPress={() => {
                                                    this.close();
                                                    (this.props.onCancel) && this.props.onCancel();
                                                 }
                                                }>
                                        <Text style={{...styles.buttonStyle, ...this.props.cancelButtonStyle}}>{this.state.cancelText}</Text>                                    
                                    </TouchableOpacity>  
                                </>
                            }                                                     
                        </View>        
                    </View>
                </Modal>
                :
                <Modal              
                    isVisible={this.state.visible}
                    onBackdropPress={this.close}
                    animationIn={this.props.animationIn || 'slideInUp'}
                    animationOut={this.props.animationOut || 'slideOutDown'}
                    hardwareAccelerated={true}
                    propagateSwipe={true}
                    >                    
                        <View style={[styles.content, scrollableView]}>  
                            <ListItem noIndent>
                                <Text style={styles.headerStyle}>{this.state.header}</Text>
                            </ListItem>
                            <ScrollView showsVerticalScrollIndicator={true}>                                       
                                
                                <View style={styles.contentContainer}>
                                    {
                                        (this.state.headerIcon)&&
                                        <FontAwesome name={this.state.headerIcon} size={70} style={{ ...styles.headerIconStyle, ...this.props.headerIconStyle }}/>
                                    }                            
                                    <Text style={{...styles.contentStyle, ...this.props.contentstyle}}>{this.state.content}</Text>
                                
                                </View>  
                                <View
                                    style={{
                                        borderBottomColor: '#CFD8DC',
                                        borderBottomWidth: StyleSheet.hairlineWidth,
                                        alignSelf:'stretch'
                                    }}
                                    />     
                                <View style={{flexDirection: 'row', width: '100%'}}>
                                    <TouchableOpacity style={{...styles.actionButtons, flex: 1}} 
                                                onPress={() => {
                                                        this.close();
                                                        (this.props.onOk) && this.props.onOk();
                                                    }
                                                    }>
                                        <Text style={{...styles.buttonStyle, ...this.props.okButtonStyle}}>{this.state.okText}</Text>                                    
                                    </TouchableOpacity>    
                                    {
                                        (this.state.cancelText)&&
                                        <>
                                            <View
                                                style={{
                                                    borderRightColor: '#CFD8DC',
                                                    borderRightWidth: StyleSheet.hairlineWidth
                                                }}
                                            />     
                                            <TouchableOpacity style={{...styles.actionButtons, flex: 1}} 
                                                        onPress={() => {
                                                            this.close();
                                                            (this.props.onCancel) && this.props.onCancel();
                                                        }
                                                        }>
                                                <Text style={{...styles.buttonStyle, ...this.props.cancelButtonStyle}}>{this.state.cancelText}</Text>                                    
                                            </TouchableOpacity>  
                                        </>
                                    }                                                     
                                </View>
                            </ScrollView>                        
                        </View>
                        
                    </Modal>
            }
            
            </>
        )
    }
}

AlertModal.defaultProps = {
    okText: 'OK',
    contentStyle:{
        fontFamily: 'sans', 
        color: '#37474F',
        textAlign: 'center',
        fontSize: 14, 
    },
    headerStyle:{
        fontFamily: 'sansmedium', 
        textAlign: 'center',
        fontSize: 18, 
        color: '#6c63ff', 
        flex: 1
    },
    headerIconStyle:{
        color: '#37474F', 
        textAlign: 'center', 
        paddingBottom: 10
    },
    buttonStyle:{
        color: '#6c63ff', 
        fontFamily: 'sans', 
        fontSize: 16
    }
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',        
        borderRadius: 15,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    headerStyle:{
        fontFamily: 'sansmedium', 
        textAlign: 'center',
        fontSize: 18, 
        color: '#6c63ff', 
        flex: 1
    },
    contentContainer:{     
        paddingLeft: 15,
        paddingBottom: 10,
        paddingRight: 15,
        paddingTop: 10
    },
    contentStyle:{
        fontFamily: 'sans', 
        color: '#37474F',
        textAlign: 'center',
        fontSize: 14, 
    },
    headerIconStyle:{
        color: '#6c63ff', 
        textAlign: 'center', 
        paddingBottom: 10
    },
    actionButtons:{
        // flex: 1, 
        flexDirection: 'row', 
        alignContent:'center', 
        justifyContent: 'center',         
        padding: 10,
        height: 40
    },
    buttonStyle:{
        color: '#6c63ff', 
        fontFamily: 'sans', 
        fontSize: 16
    }
  });

  //#6c63ff