import { Auth } from 'aws-amplify';
import { Signer } from '@aws-amplify/core';
import axios from 'axios';

const ServiceProvider = async({lambdaName, data}) => {
    const cred = await Auth.currentCredentials();
    const {accessKeyId, secretAccessKey, sessionToken} = Auth.essentialCredentials(cred);
    const accessInfo = {
      access_key: accessKeyId,
      secret_key: secretAccessKey,
      session_token: sessionToken
    };
    const service_info = {
      region: 'ap-south-1',
      service: 'lambda',
    };

    const request = {
      method: "POST",
      url: `https://lambda.ap-south-1.amazonaws.com/2015-03-31/functions/${lambdaName}/invocations`,
      data: JSON.stringify(data) || undefined
    };

    const signedRequest = Signer.sign(request, accessInfo, service_info);  
           
    try{
      const result = await axios({              
          ...signedRequest
      });
      return result.data;
    }catch(err){
      console.log(err);
      throw err;
    }   
}

export default ServiceProvider;