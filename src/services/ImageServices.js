import {SetS3Config, S3ImagePut} from 'src/services/FileServiceProvider';
import * as FileSystem from 'expo-file-system';

/**
 * Images will be uploaded into Publicly readable S3 bucket and a static URL will be served
 * because of issues related to caching of expiring signed S3 URL
 * https://collrs-assets.s3.ap-south-1.amazonaws.com/images/users/profile/img/xxxxxxx.jpg
 */

const host = `https://collrs-assets.s3.ap-south-1.amazonaws.com/public/`;


const UploadImage = async({imageURI, filename, bucket, level}) => {
    try{
        const extension = imageURI.split('.').pop();
        const result = await S3ImagePut({
            filename: `${filename}.${extension}`,
            url: imageURI,
            config: {
                level: level,
                bucket: bucket
            }
        }); 
        const imgURL = `${host}${result.key}`;
        const localURI = await DownloadCacheImage(imageURI); //imageURI is the local image URL in this case
        return {
            webURL: imgURL,
            localURL: localURI
        };
    }catch(err){
        console.log("Error: ", err);
        throw err;
    }
}

/**
 * Can download web images to local document cache or copy local images from
 * Camera Roll or Camera to local document cache
 * @param {*} imageuri 
 */
const DownloadCacheImage = async(imageuri) => {
    if(!imageuri) {        
        throw new Error('File URI is required');
    }else{
        const filename = imageuri.split('/').pop();
        const pattern = imageuri.split('/')[0];

        if (pattern === 'http:' || pattern === 'https:'){
            //Download Asynchronously            
            try{
                //check of Directory exists
                const dirStatus = await FileSystem.getInfoAsync(`${FileSystem.documentDirectory}collrsdir/img/`);
                if (!dirStatus.exists){
                    //create directory if doesn't exist
                    await FileSystem.makeDirectoryAsync(`${FileSystem.documentDirectory}collrsdir/img/`, {intermediates: true});
                }
                const result = await FileSystem.downloadAsync(imageuri,`${FileSystem.documentDirectory}collrsdir/img/${filename}`);
                const { uri } = result;
                return uri;
            }catch(err){
                console.log("Error: ", err);
                throw err;
            }   
        }else if (pattern === 'file:'){
            //copy to document local document directory
            try{
                //check of Directory exists
                const dirStatus = await FileSystem.getInfoAsync(`${FileSystem.documentDirectory}collrsdir/img/`);
                if (!dirStatus.exists){
                    //create directory if doesn't exist
                    await FileSystem.makeDirectoryAsync(`${FileSystem.documentDirectory}collrsdir/img/`, {intermediates: true});
                }

                const url = `${FileSystem.documentDirectory}collrsdir/img/${filename}`;
                await FileSystem.copyAsync({ from : imageuri, to: url});

                return url;
            }catch(err){
                console.log("Error: ", err);
                throw err;
            }            
        }        
    }
}

export default {
    UploadImage,
    DownloadCacheImage
}