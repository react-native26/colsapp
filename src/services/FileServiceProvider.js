// import Amplify from '@aws-amplify/core';
// import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage';


// Storage.get('profile/img/Google_102582034870295750321_profile.jpg')
//     .then(result => console.log("public URL",result))
//     .catch(err => console.log(err));

const imgtypes ={
    jpg: 'image/jpeg',
    jpeg: 'image/jpeg',
    png: 'image/png'
}
/**
 * @param {*} bucket : S3 Bucket name
 * @param {*} level  : Protection Level - public, private, protected
 */
export const  SetS3Config = (bucket, level) => {
    Storage.configure({ 
        AWSS3:{
            bucket: bucket, 
            level: level, 
            region: 'ap-south-1'
        }
    });
}

/**
 * filename : Name of the file (this is the S3 Object Key including any prefix)
 * data     : text, JSON, ImageBlob or anything else
 * config   : an object such as { contentType: 'application/json', level: 'protected' }
 * @param {*} param0 
 */
export const S3Put = async({ filename, data, config }) => {       
    try{
        const result = await Storage.put(filename, data, config);
        return result;
    }catch(err){
        throw err;
    }
}

export const S3ImagePut = async ({filename, url, config}) => {
    try {
        const segments = url.split('.');
        const extension = segments[segments.length - 1];

        const response = await fetch(url)        
        const blob = await response.blob()
        return Storage.put(filename, blob, {
            ...config,
            contentType: imgtypes[extension],   
            progressCallback(progress) {
                console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
            }        
        });
    } catch (err) {
        throw err;
    }
  }

/**
 * filename : Name of the file (this is the S3 Object Key including any prefix)
 * config   : an object such as { expires: 120, level: 'protected', download: true|false }
 * filetype : txt|json|img
 * if filetype is txt|json and download is set to true then returned data would be file content
 * if filetype is img then returned data will be the URL of the Image
 * @param {*} param0 
 */
export const S3Get = async({ filename, config, filetype }) => {
    try{        
        const result = await Storage.get(filename, config);
        
        if (filetype === 'txt'){
            return result.Body.toString();
        }
        if (filetype === 'json'){
            return JSON.parse(result.Body.toString());
        }

        return result;
    }catch(err){
        throw err;
    }
}

